-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: systemcontrol
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `webpara`
--

DROP TABLE IF EXISTS `webpara`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `webpara` (
  `id` int NOT NULL AUTO_INCREMENT,
  `OrgName` varchar(15) DEFAULT NULL,
  `DSN` varchar(15) DEFAULT NULL,
  `LogoPath` varchar(145) DEFAULT NULL,
  `CompanyName` varchar(45) DEFAULT NULL,
  `BackgroundColour` varchar(10) DEFAULT NULL,
  `FontColour` varchar(10) DEFAULT NULL,
  `UploadPath` varchar(145) DEFAULT NULL,
  `Active` varchar(1) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `Email` varchar(145) DEFAULT NULL,
  `Mobile` int DEFAULT NULL,
  `Name` varchar(245) DEFAULT NULL,
  `AdminID` int DEFAULT NULL,
  `Address1` text,
  `Address2` text,
  `Address3` text,
  `City` varchar(145) DEFAULT NULL,
  `State` varchar(145) DEFAULT NULL,
  `Postcode` varchar(10) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `BranchName` varchar(145) DEFAULT NULL,
  `BranchCode` varchar(45) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `GroupID` varchar(45) DEFAULT NULL,
  `PassExpiryDuration` int DEFAULT '12',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webpara`
--

LOCK TABLES `webpara` WRITE;
/*!40000 ALTER TABLE `webpara` DISABLE KEYS */;
INSERT INTO `webpara` VALUES 
(20,'spa1','spa1',NULL,'spa396',NULL,NULL,NULL,'A','2020-06-22 19:48:28',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,12),
(21,'spa2','spa2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','',NULL,'Selangor','Selangor','47100','Malaysia','Skudai, Johor','spa2','spa','mvkl',12),
(22,'spainvestor','spainvestor',NULL,'spa396',NULL,NULL,NULL,'A','2020-06-22 19:48:28',NULL,NULL,NULL,NULL,'','',NULL,'Puchong Jaya','Selangor',NULL,NULL,NULL,NULL,NULL,NULL,12),
/*!40000 ALTER TABLE `webpara` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18  9:44:18
