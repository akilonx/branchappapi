const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')
const moment = require('moment-timezone')

const typeDefs = gql`
  type PayrollBooking {
    id: String
    BookingNo: String
    BranchID: ID
    BranchName: String
    BranchCode: String
    BookFrom: Date
    BookTo: Date
    TotalHours: Float
    Status: String
    CreatedBy: String
    CreatedAt: Date
    UpdatedBy: String
    UpdatedAt: Date
    Token: String
    ReceivedBy: String
    ReceivedOn: Date
    Client: String
    FirstName: String
    LastName: String
    Error: String
    TotalPerson: Int
    ServiceTimes: Int
    Rating: Int
    RatingOn: Date
    FullName: String
    BookedBy: String
  }
  type PayrollHeader {
    id: ID
    UserID: ID
    FromDate: Date
    ToDate: Date
    Status: String
    CreatedOn: Date
    CreatedBy: String
    UpdatedOn: Date
    UpdatedBy: String
    Error: String
  }

  type PayrollDetail {
    id: ID
    PayrollID: ID
    Title: String
    Amount: Float
    Error: String
  }

  type PayrollStaff {
    id: ID
    InvoiceAmount: Float
    StaffName: String
    StaffID: String
  }

  extend type Query {
    payrollstaffs(ToDate: Date, FromDate: Date): [PayrollStaff]
    payrollheaders(UserID: ID): [PayrollHeader]
    payrolldetails(PayrollID: ID): [PayrollDetail]
    payrollorders(UserID: ID, FromDate: Date, ToDate: Date): [OrderDetail]
    payrollbookings(UserID: ID, FromDate: Date, ToDate: Date): [PayrollBooking]
    payrollratings(UserID: ID, FromDate: Date, ToDate: Date): [PayrollBooking]
    payrollcollections(UserID: ID, FromDate: Date, ToDate: Date): [Order]
  }

  extend type Mutation {
    updatepayrollheader(id: ID, UserID: ID, From: Date, To: Date): PayrollHeader
    insertpayrollheader(From: Date, UserID: ID, To: Date): PayrollHeader
    removepayrollheader(id: ID): Int

    updatepayrolldetail(
      id: ID
      PayrollID: ID
      Title: String
      Amount: Float
    ): PayrollDetail
    insertpayrolldetail(
      PayrollID: ID
      Title: String
      Amount: Float
    ): PayrollDetail
    removepayrolldetail(id: ID): Int
  }
`

const resolvers = {
  Query: {
    payrollstaffs: (
      root,
      { FromDate, ToDate },
      { username, role, client, sessionid, iat }
    ) => {
      const fromsplit = FromDate.split('/')
      const frdate = `${fromsplit[2]}-${fromsplit[0]}-${fromsplit[1]}`

      const tosplit = ToDate.split('/')
      const todate = `${tosplit[2]}-${tosplit[0]}-${tosplit[1]}`

      return db.sequelize.query(
        `SELECT 
          a.StaffID,
          a.id,
          a.Department,
          b.InvoiceAmount
        FROM ${client}.ao_stafflist a
        LEFT JOIN (SELECT
          SalesPersonSingle as SalesPerson,
          sum(TransactionAmount) as InvoiceAmount
        FROM spa1.ao_transactionlist
        WHERE Client=? AND (DATE(TransactionDate) BETWEEN ? and ? OR DATE(TransactionDate) = ? OR DATE(TransactionDate) = ?)
        GROUP BY SalesPersonSingle ) b ON b.SalesPerson = a.StaffID
        `,
        {
          replacements: [client, FromDate, ToDate, FromDate, ToDate],
          type: QueryTypes.SELECT,
        }
      )
    },
    payrollcollections: async (
      root,
      { UserID, FromDate, ToDate },
      { username, role, client, sessionid, iat }
    ) => {
      if (UserID) {
        const checkuser = await db.sequelize.query(
          `SELECT 
            Department 
          FROM ${client}.ao_stafflist
          WHERE StaffID=?
          `,
          {
            replacements: [username],
            type: QueryTypes.SELECT,
          }
        )

        console.log('here', checkuser[0])

        if (checkuser[0] && checkuser[0].Department != 'Management')
          return { Error: 'Not Allowed' }

        const user = await db.sequelize.query(
          `SELECT 
              StaffID 
            FROM ${client}.ao_stafflist
            WHERE id=?
            `,
          {
            replacements: [UserID],
            type: QueryTypes.SELECT,
          }
        )

        /* const fromSplit = `${FromDate.split('/')[2]}-${
          FromDate.split('/')[0]
        }-${FromDate.split('/')[1]}`
        const toSplit = `${ToDate.split('/')[2]}-${ToDate.split('/')[0]}-${
          ToDate.split('/')[1]
        }` */

        const fromSplit = moment(FromDate).tz('Asia/Kuala_Lumpur').format()
        const toSplit = moment(ToDate).tz('Asia/Kuala_Lumpur').format()
        console.log(fromSplit, toSplit)

        const query = await db.sequelize.query(
          `SELECT 
            a.*
          FROM spa1.ao_orderheader a
          WHERE a.ReceivedBy=? AND a.Client=? AND (DATE(a.ReceivedOn) BETWEEN ? and ? OR DATE(a.ReceivedOn) = ? OR DATE(a.ReceivedOn) = ?)
          ORDER BY a.id asc`,
          {
            replacements: [
              user[0].StaffID,
              client,
              fromSplit.split('T')[0],
              toSplit.split('T')[0],
              fromSplit.split('T')[0],
              toSplit.split('T')[0],
            ],
            type: QueryTypes.SELECT,
          }
        )
        return query || []
      }
    },

    payrollorders: async (
      root,
      { UserID, FromDate, ToDate },
      { username, role, client, sessionid, iat }
    ) => {
      if (UserID) {
        const checkuser = await db.sequelize.query(
          `SELECT 
            Department 
          FROM ${client}.ao_stafflist
          WHERE StaffID=?
          `,
          {
            replacements: [username],
            type: QueryTypes.SELECT,
          }
        )

        console.log('here', checkuser[0])

        if (checkuser[0] && checkuser[0].Department != 'Management')
          return { Error: 'Not Allowed' }

        const user = await db.sequelize.query(
          `SELECT 
              StaffID 
            FROM ${client}.ao_stafflist
            WHERE id=?
            `,
          {
            replacements: [UserID],
            type: QueryTypes.SELECT,
          }
        )

        /* const fromSplit = `${FromDate.split('/')[2]}-${
          FromDate.split('/')[0]
        }-${FromDate.split('/')[1]}`
        const toSplit = `${ToDate.split('/')[2]}-${ToDate.split('/')[0]}-${
          ToDate.split('/')[1]
        }` */

        const fromSplit = moment(FromDate).tz('Asia/Kuala_Lumpur').format()
        const toSplit = moment(ToDate).tz('Asia/Kuala_Lumpur').format()
        console.log(fromSplit, toSplit)

        const query = await db.sequelize.query(
          `SELECT 
            a.*,
            b.ProductName,
            b.Category,
            (select FileName from spa1.ao_uploadlist where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
          FROM spa1.ao_orderdetail a 
          LEFT JOIN spa1.ao_productlist b ON a.ProductID = b.id
            WHERE a.SalesPerson=? AND a.SalesClient=? AND (DATE(a.CreatedDate) BETWEEN ? and ? OR DATE(a.CreatedDate) = ? OR DATE(a.CreatedDate) = ?)
            ORDER BY a.id DESC
            `,
          {
            replacements: [
              user[0].StaffID,
              client,
              fromSplit.split('T')[0],
              toSplit.split('T')[0],
              fromSplit.split('T')[0],
              toSplit.split('T')[0],
            ],
            type: QueryTypes.SELECT,
          }
        )

        return query || []
      }
    },
    payrollbookings: async (
      root,
      { UserID, FromDate, ToDate },
      { username, role, client, sessionid, iat }
    ) => {
      if (UserID) {
        const checkuser = await db.sequelize.query(
          `SELECT 
            Department 
          FROM ${client}.ao_stafflist
          WHERE StaffID=?
          `,
          {
            replacements: [username],
            type: QueryTypes.SELECT,
          }
        )

        console.log('here', checkuser[0])

        if (checkuser[0] && checkuser[0].Department != 'Management')
          return { Error: 'Not Allowed' }

        const user = await db.sequelize.query(
          `SELECT 
              StaffID 
            FROM ${client}.ao_stafflist
            WHERE id=?
            `,
          {
            replacements: [UserID],
            type: QueryTypes.SELECT,
          }
        )

        /* const fromSplit = `${FromDate.split('/')[2]}-${
          FromDate.split('/')[0]
        }-${FromDate.split('/')[1]}`
        const toSplit = `${ToDate.split('/')[2]}-${ToDate.split('/')[0]}-${
          ToDate.split('/')[1]
        }` */

        const fromSplit = moment(FromDate).tz('Asia/Kuala_Lumpur').format()
        const toSplit = moment(ToDate).tz('Asia/Kuala_Lumpur').format()
        console.log(fromSplit, toSplit)

        const query = await db.sequelize.query(
          `SELECT 
              a.*, 
              c.FirstName,
              c.LastName,
              (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
            FROM spa1.ao_bookinglist a
            LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
            LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
            WHERE a.ReceivedBy=? AND a.Client=? AND (DATE(a.ReceivedOn) BETWEEN ? and ? OR DATE(a.ReceivedOn) = ? OR DATE(a.ReceivedOn) = ?)
            ORDER BY a.id DESC
            `,
          {
            replacements: [
              user[0].StaffID,
              client,
              fromSplit.split('T')[0],
              toSplit.split('T')[0],
              fromSplit.split('T')[0],
              toSplit.split('T')[0],
            ],
            type: QueryTypes.SELECT,
          }
        )

        return query || []
      }
    },
    payrollratings: async (
      root,
      { UserID, FromDate, ToDate },
      { username, role, client, sessionid, iat }
    ) => {
      if (UserID) {
        const checkuser = await db.sequelize.query(
          `SELECT 
            Department 
          FROM ${client}.ao_stafflist
          WHERE StaffID=?
          `,
          {
            replacements: [username],
            type: QueryTypes.SELECT,
          }
        )

        console.log('here', checkuser[0])

        if (checkuser[0] && checkuser[0].Department != 'Management')
          return { Error: 'Not Allowed' }

        const user = await db.sequelize.query(
          `SELECT 
              StaffID 
            FROM ${client}.ao_stafflist
            WHERE id=?
            `,
          {
            replacements: [UserID],
            type: QueryTypes.SELECT,
          }
        )

        /* const fromSplit = `${FromDate.split('/')[2]}-${
          FromDate.split('/')[0]
        }-${FromDate.split('/')[1]}`
        const toSplit = `${ToDate.split('/')[2]}-${ToDate.split('/')[0]}-${
          ToDate.split('/')[1]
        }` */

        const fromSplit = moment(FromDate).tz('Asia/Kuala_Lumpur').format()
        const toSplit = moment(ToDate).tz('Asia/Kuala_Lumpur').format()
        console.log(fromSplit, toSplit)

        const query = await db.sequelize.query(
          `SELECT 
              a.*, 
              c.FirstName,
              c.LastName,
              (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
            FROM spa1.ao_bookinglist a
            LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
            LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
            WHERE a.ReceivedBy=? AND a.Client=? AND (DATE(a.RatingOn) BETWEEN ? and ? OR DATE(a.RatingOn) = ? OR DATE(a.RatingOn) = ?)
            ORDER BY a.id DESC
            `,
          {
            replacements: [
              user[0].StaffID,
              client,
              fromSplit.split('T')[0],
              toSplit.split('T')[0],
              fromSplit.split('T')[0],
              toSplit.split('T')[0],
            ],
            type: QueryTypes.SELECT,
          }
        )

        return query || []
      }
    },
    payrollheaders: async (
      root,
      { UserID },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
          a.*
        FROM ${client}.ao_payrollheader a
        WHERE a.UserID=?
        ORDER BY a.id DESC
        `,
        {
          replacements: [UserID],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
    payrolldetails: async (
      root,
      { PayrollID },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
          a.*
        FROM ${client}.ao_payrolldetail a
        WHERE a.PayrollID=?
        ORDER BY a.id ASC
        `,
        {
          replacements: [PayrollID],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
  },

  Mutation: {
    removepayrolldetail: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)
      const query = await db.sequelize.query(
        `DELETE FROM
          ${client}.ao_payrolldetail
        WHERE id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      )

      return id
    },

    removepayrollheader: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)
      const query = await db.sequelize.query(
        `DELETE FROM
          ${client}.ao_payrollheader 
        WHERE id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      )

      return id
    },

    insertpayrollheader: async (
      root,
      { From, To, Status, UserID },
      { username, role, client, sessionid, iat }
    ) => {
      const fromSplit = `${From.split('/')[2]}-${From.split('/')[0]}-${
        From.split('/')[1]
      }`
      const toSplit = `${To.split('/')[2]}-${To.split('/')[0]}-${
        To.split('/')[1]
      }`

      const checkuser = await db.sequelize.query(
        `SELECT 
          Department 
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      console.log('here', checkuser[0])

      if (checkuser[0].Department != 'Management')
        return { Error: 'Not Allowed' }

      const insert = await db.sequelize.query(
        `INSERT INTO 
          ${client}.ao_payrollheader 
        SET UserID=?, FromDate=?, ToDate=?, Status='New', CreatedBy=?, CreatedOn=NOW()
        `,
        {
          replacements: [UserID, fromSplit, toSplit, username],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          a.*
        FROM ${client}.ao_payrollheader a
        WHERE a.id=? LIMIT 1
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    updatepayrollheader: async (
      root,
      { id, From, To, Status, UserID },
      { username, role, client, sessionid, iat }
    ) => {
      const checkuser = await db.sequelize.query(
        `SELECT 
          Department 
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      console.log('here', checkuser[0])

      if (checkuser[0].Department != 'Management')
        return { Error: 'Not Allowed' }

      const fromSplit = `${From.split('/')[2]}-${From.split('/')[0]}-${
        From.split('/')[1]
      }`
      const toSplit = `${To.split('/')[2]}-${To.split('/')[0]}-${
        To.split('/')[1]
      }`

      const update = await db.sequelize.query(
        `UPDATE
          ${client}.ao_payrollheader
        SET 
          UserID=?, FromDate=?, ToDate=?, ModifiedBy=?, ModifiedOn=NOW()
         WHERE id=?
         `,
        {
          replacements: [UserID, fromSplit, toSplit, username, id],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          a.*
        FROM ${client}.ao_payrollheader a
        WHERE a.id=? LIMIT 1
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    insertpayrolldetail: async (
      root,
      { PayrollID, Title, Amount },
      { username, role, client, sessionid, iat }
    ) => {
      const checkuser = await db.sequelize.query(
        `SELECT 
          Department 
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      console.log('here', checkuser[0])

      if (checkuser[0].Department != 'Management')
        return { Error: 'Not Allowed' }

      const insert = await db.sequelize.query(
        `INSERT INTO 
          ${client}.ao_payrolldetail
        SET PayrollID=?, Title=?, Amount=?, CreatedBy=?, CreatedOn=NOW()
        `,
        {
          replacements: [PayrollID, Title, Amount, username],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          a.*
        FROM ${client}.ao_payrolldetail a
        WHERE a.id=? LIMIT 1
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    updatepayrolldetail: async (
      root,
      { id, PayrollID, Title, Amount },
      { username, role, client, sessionid, iat }
    ) => {
      const checkuser = await db.sequelize.query(
        `SELECT 
          Department 
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      console.log('here', checkuser[0])

      if (checkuser[0].Department != 'Management')
        return { Error: 'Not Allowed' }

      const update = await db.sequelize.query(
        `UPDATE
          ${client}.ao_payrolldetail
        SET 
          PayrollID=?, Title=?, Amount=?, ModifiedBy=?, ModifiedOn=NOW()
         WHERE id=?
         `,
        {
          replacements: [PayrollID, Title, Amount, username, id],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          a.*
        FROM ${client}.ao_payrolldetail a
        WHERE a.id=? LIMIT 1
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
