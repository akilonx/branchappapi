const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

//id, ProductID, OrderID, Title, Movement, Qty, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn

const typeDefs = gql`
  type Stock {
    id: ID
    ProductID: ID
    ProductName: String
    StockTakeID: ID
    Title: String
    Qty: Float
    Bags: Float
    CreatedBy: String
    CreatedOn: Date
    ModifiedBy: String
    ModifiedOn: Date
    LocationID: String
  }

  type RemainingStock {
    id: ID
    BranchCode: String
    ProductName: String
    Title: String
    TotalIn: Int
    TotalOut: Int
    TotalReduce: Int
    TotalReserve: Int
  }

  enum TRANSACTION_TYPE {
    ADD
    REDUCE
  }

  type StockTake {
    id: ID
    StockTakeID: ID
    StockTakeDate: Date
    LocationName: String
    TransactionType: String
    LocationID: ID
    FarmCode: ID
    Qty: Float
    Bags: Float
    CreatedBy: String
    CreatedOn: Date
    ModifiedBy: String
    ModifiedOn: Date
  }

  type StockLocation {
    BranchName: ID
    BranchCode: String
  }

  extend type Query {
    stocks(StockTakeID: ID, LocationID: String): [Stock]
    stocktakes: [StockTake]
    stocklocations: [StockLocation]
    remainingstocks: [RemainingStock]
  }

  input CreateStockInput {
    id: ID
    Title: String
    Qty: String
  }

  extend type Mutation {
    createstocktake(
      TransactionType: TRANSACTION_TYPE
      LocationID: ID
      Stocks: [CreateStockInput]
    ): StockTake

    createstock(
      StockTakeID: ID
      ProductID: ID
      OrderID: ID
      Title: String
      Qty: Int
    ): Stock

    updatestock(
      id: ID
      ProductID: ID
      OrderID: ID
      Title: String
      Qty: Int
    ): Stock
  }
`

const resolvers = {
  Query: {
    stocklocations: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const webpara = await db.sequelize.query(
        `SELECT 
            a.*
          FROM systemcontrol.webpara a 
          WHERE a.BranchCode=? 
          Limit 1`,
        {
          replacements: [client],
          type: QueryTypes.SELECT,
        }
      )

      if (!webpara[0]) return []

      if (!webpara[0].GroupID) return webpara

      const webparagroup = await db.sequelize.query(
        `SELECT 
            a.*
          FROM systemcontrol.webpara a 
          WHERE a.GroupID=?
          ORDER BY a.BranchName ASC`,
        {
          replacements: [webpara[0].GroupID],
          type: QueryTypes.SELECT,
        }
      )

      return webparagroup
    },
    remainingstocks: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const output = []

      const groupid = await db.sequelize.query(
        `SELECT 
          * 
        FROM systemcontrol.webpara
        WHERE OrgName=?
        `,
        {
          replacements: [client],
          type: QueryTypes.SELECT,
        }
      )

      if (!groupid[0]) return { Error: 'Invalid data request' }

      let webpara = await db.sequelize.query(
        `SELECT 
          * 
        FROM systemcontrol.webpara
        WHERE GroupID=?
        `,
        {
          replacements: [groupid[0].GroupID],
          type: QueryTypes.SELECT,
        }
      )

      if (!webpara[0]) webpara = [...groupid]

      let stockssql = ``
      webpara.map(async (para, i) => {
        stockssql += `SELECT 
              a.*,
            id + ${i * 1000} as id,
              (SELECT 
                      b.ProductName
                  FROM
                      spa1.ao_productlist b
                  WHERE
                      b.id = a.ProductID) AS ProductName,
              g.TotalOut,
              b.TotalReduce,
              c.TotalIn,
              d.TotalReserve,
              '${para.DSN}' AS BranchCode
          FROM
              ${para.DSN}.ao_stocklist a
              LEFT JOIN
            (SELECT 
                ProductID, SUM(qty) AS TotalOut
            FROM
                ${para.DSN}.ao_stocklist
            WHERE
                Movement = 'Out'
            GROUP BY productid) g ON g.ProductID = a.ProductID
                  LEFT JOIN
              (SELECT 
                  z.ProductID, SUM(z.qty) AS TotalReduce
              FROM
                  ${para.DSN}.ao_stocktakelist y
            LEFT JOIN ${para.DSN}.ao_stocklist z ON z.StockTakeID = y.id
              WHERE
                  y.TransactionType = 'REDUCE'
              GROUP BY z.ProductID) b ON b.ProductID = a.ProductID
                  LEFT JOIN
              (SELECT 
                  z.ProductID, SUM(z.qty) AS TotalIn
              FROM
                  ${para.DSN}.ao_stocktakelist y
            LEFT JOIN ${para.DSN}.ao_stocklist z ON z.StockTakeID = y.id
              WHERE
                  y.TransactionType = 'ADD'
              GROUP BY z.ProductID) c ON c.ProductID = a.ProductID
                  LEFT JOIN
              (SELECT 
                  s.ProductID, s.OrderID, SUM(s.qty) AS TotalReserve, t.Client
              FROM
                  spa1.ao_orderdetail s
              LEFT JOIN spa1.ao_orderheader t ON t.id = s.OrderID
              WHERE
                s.Status = 'Not Collected'
              GROUP BY s.ProductID, t.Client) d ON d.ProductID = a.ProductID AND d.Client = '${
                para.DSN
              }'
          GROUP BY BranchCode , a.productid
        `
        if (i < webpara.length - 1 && webpara.length > 1) stockssql += ` UNION `
        if (i == webpara.length - 1) stockssql += ` order by ProductName asc`
      })

      const stocks = await db.sequelize.query(stockssql, {
        replacements: [],
        type: QueryTypes.SELECT,
      })
      //console.log('output', output)
      return stocks
    },
    stocks: async (
      root,
      { StockTakeID, LocationID },
      { username, role, client, sessionid, iat }
    ) =>
      db.sequelize.query(
        `SELECT 
          b.* ,
          c.ProductName as ProductName
        FROM ${LocationID}.ao_stocklist b
        LEFT JOIN spa1.ao_productlist c ON c.id = b.ProductID
        WHERE b.StockTakeID = ?
      `,
        {
          replacements: [StockTakeID],
          type: QueryTypes.SELECT,
        }
      ),
    stocktakes: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const output = []

      let webpara = await db.sequelize.query(
        `SELECT 
            a.*
          FROM systemcontrol.webpara a 
          WHERE a.BranchCode=? 
          Limit 1`,
        {
          replacements: [client],
          type: QueryTypes.SELECT,
        }
      )

      if (!webpara[0]) return []

      if (webpara[0].GroupID) {
        webpara = await db.sequelize.query(
          `SELECT 
              a.*
            FROM systemcontrol.webpara a 
            WHERE a.GroupID=?
            ORDER BY a.BranchName ASC`,
          {
            replacements: [webpara[0].GroupID],
            type: QueryTypes.SELECT,
          }
        )
      }

      let stockssql = ``
      webpara.map(async (para, i) => {
        stockssql += `SELECT 
            *,
            '${para.BranchCode}' AS LocationID,
           id + ${Math.random() * i} as id,
            id as StockTakeID
          FROM ${para.DSN}.ao_stocktakelist 
        `
        if (i < webpara.length - 1) stockssql += ` UNION `
        console.log('output', output, i, webpara.length)

        if (i == webpara.length - 1) stockssql += ` order by CreatedOn desc`
      })

      const stocks = await db.sequelize.query(stockssql, {
        replacements: [],
        type: QueryTypes.SELECT,
      })
      //console.log('output', output)
      return stocks
    },
  },
  Mutation: {
    createstocktake: async (
      root,
      { TransactionType, LocationID, Stocks },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const insert = await db.sequelize.query(
        `INSERT INTO 
          ${LocationID}.ao_stocktakelist
        SET
          TransactionType=?,
          LocationID=?,
          StockTakeDate=NOW(),
          CreatedOn=NOW(),
          CreatedBy=?`,
        {
          replacements: [TransactionType, LocationID, username],
          type: QueryTypes.INSERT,
        }
      )

      let saveItems = []
      let totalQty = 0
      Stocks.map((a) => {
        let output = [insert[0], a.id, a.Qty, username]
        saveItems = [...saveItems, [insert[0], a.id, a.Qty, username]]
        if (a.Qty) totalQty = totalQty + parseFloat(a.Qty)
      })

      const insertStock = await db.sequelize.query(
        `INSERT INTO 
          ${LocationID}.ao_stocklist
          (StockTakeID, ProductID, Qty, CreatedBy) VALUES ?`,
        {
          replacements: [saveItems],
          type: QueryTypes.INSERT,
        }
      )

      const updateStockTake = await db.sequelize.query(
        `UPDATE
          ${LocationID}.ao_stocktakelist
        SET 
          Qty=?
          WHERE id=?
          `,
        {
          replacements: [totalQty, insert[0]],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          b.* 
        FROM ${LocationID}.ao_stocktakelist b
        WHERE b.id=?
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    createstock: async (
      root,
      { id, OrderID, Title, Movement, Qty },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const insert = await db.sequelize.query(
        `INSERT INTO 
          ${client}.ao_stocklist
        SET 
          ProductID=?,
          Title='Stock In',
          Movement='In',
          Qty=?,
          CreatedOn=NOW(),
          CreatedBy=?`,
        {
          replacements: [id, Qty, username],
          type: QueryTypes.INSERT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          b.* , 
          (SELECT a.ProductName FROM spa1.ao_productlist a WHERE a.id = b.ProductID) as ProductName
        FROM ${client}.ao_stocklist b
        WHERE b.id=?
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },

    updatestock: async (
      root,
      { id, ProductID, OrderID, Title, Movement, Qty },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM spa1.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const update = await db.sequelize.query(
        `UPDATE
          ${client}.ao_stocklist
        SET 
          ProductID=?,
          Qty=?,
          ModifiedOn=NOW(),
          ModifiedBy=?
          WHERE id=?
          `,
        {
          replacements: [ProductID, Qty, username, id],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          b.* , 
          (SELECT a.ProductName FROM spa1.ao_productlist a WHERE a.id = b.ProductID) as ProductName
        FROM ${client}.ao_stocklist b
        WHERE b.id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
