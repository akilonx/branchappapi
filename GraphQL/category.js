const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Category {
    id: ID!
    CategoryName: String
    Prettier: String
    ParentID: ID
  }

  extend type Query {
    categories: [Category]
  }
`

const resolvers = {
  Query: {
    categories: (root, {}, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT 
        *
        FROM ${client}.ao_categorylist 
        ORDER BY Ordering ASC
        `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
