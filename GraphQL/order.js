const { gql } = require('apollo-server-express')
const db = require('../database')
const pool = require('../_helpers/database')
const moment = require('moment-timezone')
const crypto = require('crypto')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Order {
    id: ID
    OrderNo: String
    CustomerCode: String
    CustomerName: String
    ZoneID: String
    NoOfCarton: String
    Remarks: String
    Status: String
    CreatedBy: String
    CreatedOn: Date
    ModifiedBy: String
    LastModified: Date
    DateLapse: String
    FirstName: String
    FullName: String
    LastName: String
    Phone: String
    Email: String
    Address1: String
    Address2: String
    State: String
    City: String
    PostCode: String
    DeliveryCharges: Float
    TotalAmount: Float
    FinalTotal: Float
    PaymentMethod: String
    PaymentRemark: String
    Received: String
    ReceivedBy: String
    ReceivedOn: Date
    Client: String
    Error: String
    Token: String
    Refund: Int
    RefundReason: String
    RefundBy: String
    RefundOn: Date
  }

  type OrderStatus {
    id: ID!
    OrderID: String
    Status: String
    CreatedBy: String
    CreatedOn: Date
  }

  type StatusList {
    id: ID
    StatusName: String
    Ordering: Int
    StatusCode: String
  }

  input CreateOrderInput {
    id: ID
    Title: String
    Qty: String
  }

  extend type Query {
    orderdetails(OrderID: ID, Token: ID): [OrderDetail]
    statuslist: [StatusList]
    orders: [Order]
    order(OrderID: ID, Token: ID): Order
    nozoneorders: [Order]
    orderstatus(OrderID: ID!): [OrderStatus]
  }

  extend type Mutation {
    createorderstaff(
      Customer: ID
      Orders: [CreateOrderInput!]!
      SalesPerson: String
      Password: String
      PaymentMode: String
      PaymentID: ID
      PaymentCCName: String
      PaymentRemark: String
    ): Order

    createorder(
      CustomerCode: String
      NoOfCarton: String
      Remarks: String
      User: String
    ): Order
    updateorder(
      id: ID!
      CustomerCode: String
      NoOfCarton: String
      Remarks: String
      User: String
    ): Order

    receivedorder(Token: ID, SalesPerson: String, Password: String): Order
    removeorder(id: ID!): Int
    updatezone(
      id: ID!
      ZoneID: String
      Status: String
      StatusText: String
      Remarks: String
      CreatedBy: String
    ): Order
  }
`

const resolvers = {
  Query: {
    orderdetails: async (
      root,
      { OrderID, Token },
      { username, role, client, sessionid, iat }
    ) => {
      if (Token) {
        const query = await db.sequelize.query(
          `SELECT 
            a.*,
            (case when (a.Category = 1) 
            THEN
                  ''
            ELSE
                  e.uom
            END)
            as ProductName2,
            d.Refund,
            d.RefundReason,
            d.RefundBy,
            d.RefundOn,
            b.ProductName,
            b.Category,
            (select FileName from spa1.ao_uploadlist  where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
          FROM spa1.ao_orderdetail a 
          LEFT JOIN spa1.ao_productlist b ON a.ProductID = b.id
          LEFT JOIN spa1.ao_pricelist e ON e.id = a.PriceID
          LEFT JOIN spa1.ao_transactionlist d ON d.TransactionID = a.OrderID AND d.TransactionRef = a.ProductID
          WHERE c.Token = ?
          ORDER BY a.id desc`,
          {
            replacements: [Token],
            type: QueryTypes.SELECT,
          }
        )
        return query
      } else {
        const query = await db.sequelize.query(
          `SELECT 
            a.*,
            (case when (a.Category = 1) 
            THEN
                  ''
            ELSE
                  e.uom
            END)
            as ProductName2,
            d.Refund,
            d.RefundReason,
            d.RefundBy,
            d.RefundOn,
            b.ProductName,
            b.Category,
            (select FileName from spa1.ao_uploadlist  where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
          FROM spa1.ao_orderdetail a 
          LEFT JOIN spa1.ao_productlist b ON a.ProductID = b.id
          LEFT JOIN spa1.ao_pricelist e ON e.id = a.PriceID
          LEFT JOIN spa1.ao_transactionlist d ON d.TransactionID = a.OrderID AND d.PriceID = a.PriceID
          WHERE a.OrderID = ?
          ORDER BY a.id desc`,
          {
            replacements: [OrderID],
            type: QueryTypes.SELECT,
          }
        )

        return query
      }
    },
    order: async (
      root,
      { OrderID, Token },
      { username, role, client, sessionid, iat }
    ) => {
      if (Token) {
        const query = await db.sequelize.query(
          `SELECT 
            a.*, 
            DATEDIFF(NOW(), a.CreatedOn) as DateLapse, 
            CONCAT(b.FirstName,' ', b.LastName, '\n', b.Phone) as FullName,
            b.FirstName, 
            b.LastName, 
            b.Address1, 
            b.Address2, 
            b.State, 
            b.City, 
            b.PostCode, 
            b.Phone, 
            b.Email,
            (select sum(InvoiceAmount) as TotalAmount from spa1.ao_orderdetail where OrderID = a.id ) as TotalAmount,
            (select (sum(InvoiceAmount) + a.DeliveryCharges) as FinalTotal from spa1.ao_orderdetail where OrderID = a.id ) as FinalTotal
          FROM spa1.ao_orderheader a 
          LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.UserID 
          WHERE a.Token=?`,
          {
            replacements: [Token],
            type: QueryTypes.SELECT,
          }
        )

        if (!query[0]) return {}

        return query[0]
      } else {
        const query = await db.sequelize.query(
          `SELECT 
            a.*, 
            DATEDIFF(NOW(), a.CreatedOn) as DateLapse, 
            CONCAT(b.FirstName,' ', b.LastName, '\n', b.Phone) as FullName,
            b.FirstName, 
            b.LastName, 
            b.Address1, 
            b.Address2, 
            b.State, 
            b.City, 
            b.PostCode, 
            b.Phone, 
            b.Email,
            (select sum(InvoiceAmount) as TotalAmount from spa1.ao_orderdetail where OrderID = a.id ) as TotalAmount,
            (select (sum(InvoiceAmount) + a.DeliveryCharges) as FinalTotal from spa1.ao_orderdetail where OrderID = a.id ) as FinalTotal
          FROM spa1.ao_orderheader a 
          LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.UserID 
          WHERE a.id=?`,
          {
            replacements: [OrderID],
            type: QueryTypes.SELECT,
          }
        )
        return query[0]
      }
    },
    statuslist: (root, args, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT * from ${client}.ao_statuslist WHERE HideSelect is null order by Ordering ASC`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
    orders: async (root, args, { username, role, client, sessionid, iat }) => {
      /*  sub: 136,
      username: 'ace',
      role: 'Booking Agent',
      client: 'asiaone_old7',
      sessionid: 'PU3hDduIQVTjlT5tZR_L',
      iat: 1581410557 */

      return db.sequelize.query(
        `SELECT 
            a.*, 
            DATEDIFF(NOW(), a.CreatedOn) as DateLapse, 
            CONCAT_WS(' ', b.FirstName, b.LastName, b.Phone) as FullName,
            b.FirstName, 
            b.LastName, 
            b.Address1, 
            b.Address2, 
            b.State, 
            b.City, 
            b.PostCode, 
            b.Phone, 
            b.Email,
            (select sum(InvoiceAmount) as TotalAmount from spa1.ao_orderdetail where OrderID = a.id ) as TotalAmount,
            (select (sum(InvoiceAmount) + a.DeliveryCharges) as FinalTotal from spa1.ao_orderdetail where OrderID = a.id ) as FinalTotal
          FROM spa1.ao_orderheader a 
          LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.UserID 
          LEFT JOIN
        (SELECT 
          count(*) as TotalProduct,
                  OrderID
        FROM
          spa1.ao_orderdetail
        WHERE
          Category = 1
        group by orderid) c ON c.OrderID = a.id
          WHERE a.Client=?
          ORDER BY a.OrderNo DESC limit 1000`,
        {
          replacements: [client],
          type: QueryTypes.SELECT,
        }
      )
    },
    /* db.order.findAll({
        order: [['id', 'DESC']],
        limit: 100
      }) */
    nozoneorders: (root, data, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT a.*, b.CustomerName, b.Address1, b.Address2 FROM ${client}.ao_orderheader a LEFT JOIN ${client}.ao_customerlist b ON b.CustomerCode = a.CustomerCode WHERE a.ZoneID is null ORDER BY a.id DESC`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
    orderstatus: (
      root,
      { OrderID },
      { username, role, client, sessionid, iat }
    ) =>
      db.sequelize.query(
        `SELECT * FROM ${client}.ao_orderstatus WHERE OrderID=? ORDER BY id ASC`,
        {
          replacements: [OrderID],
          type: QueryTypes.SELECT,
        }
      ),
  },
  Order: {
    Customer: (
      { CustomerCode },
      data,
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)
      db.customer.findOne({
        where: {
          CustomerCode,
        },
      })
    },
  },
  Mutation: {
    createorderstaff: async (
      root,
      {
        Customer,
        Orders,
        SalesPerson,
        Password,
        PaymentMode,
        PaymentID,
        PaymentCCName,
        PaymentRemark,
      },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM ${client}.ao_stafflist
        WHERE StaffID=? AND Password2=?
        `,
        {
          replacements: [SalesPerson, Password],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return { Error: 'Login error, please try again.' }

      const customer = await db.sequelize.query(
        `SELECT 
          a.*,
          b.Branch 
        FROM spa1.ao_stafflist a
        LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.id
        WHERE a.id=?
        `,
        {
          replacements: [Customer],
          type: QueryTypes.SELECT,
        }
      )

      /*  const loggedIn = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_puchcarddetail WHERE Username=? AND DATE(PunchDate)=? AND PunchType='Checkin'`,
        {
          replacements: [
            username,
            moment(new Date()).tz('Asia/Kuala_Lumpur').format().split('T')[0],
          ],
          type: QueryTypes.SELECT,
        }
      )

      if (!loggedIn[0]) return { Error: 'You are not Punched In' }
 */

      if (!customer[0]) return { Error: 'Customer not found error' }

      db.changeDB('spa1')

      const orderNoIncrease = await db.freightrunno.update(
        {
          LastNo: literal('LastNo + 1'),
        },
        {
          where: {
            Prefix: 'ODR',
          },
        }
      )

      const orderNo = await db.freightrunno.findOne({
        where: {
          Prefix: 'ODR',
        },
      })

      /* const prefix = await db.freightconfig.findOne({
        where: {
          ParmKey: 'ODRPFX',
        },
      }) */

      const OrderNo = `${db.pad(orderNo.LastNo, 5)}`

      const StatusCode = await db.sequelize.query(
        `SELECT * FROM spa1.ao_statuslist WHERE StatusCode='ORDERCREATED'`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      const token = crypto.randomBytes(64).toString('hex')

      return db.order
        .create({
          OrderNo,
          Status: StatusCode[0] && StatusCode[0].StatusName,
          StatusCode: StatusCode[0] && StatusCode[0].StatusCode,
          UserID: customer[0].id,
          CreatedBy: SalesPerson,
          /*  DeliveryCharges: postcodes[0].Price, */
          PaymentCCNo: PaymentRemark,
          PaymentMethod: PaymentMode,
          PaymentId: PaymentID,
          PaymentCCName: PaymentCCName,
          CreatedOn: fn('NOW'),
          Client: client,
          Token: token,
        })
        .then(async (a) => {
          const OrderID = a.id

          const insert = await db.sequelize.query(
            `INSERT
            spa1.ao_orderstatus
            SET
            OrderID = ?, Status = ?, CreatedBy = ?, StatusCode = ?, CreatedOn = NOW()`,
            {
              replacements: [
                OrderID,
                StatusCode[0] && StatusCode[0].StatusName,
                SalesPerson,
                StatusCode[0] && StatusCode[0].StatusCode,
              ],
              type: QueryTypes.INSERT,
            }
          )

          const prices = await db.sequelize.query(
            `SELECT a.*, b.ProductName, b.Category FROM spa1.ao_pricelist a LEFT JOIN spa1.ao_productlist b ON b.id = a.ProductID ORDER BY b.Category DESC`,
            {
              replacements: [],
              type: QueryTypes.SELECT,
            }
          )

          let saveItems = []
          let totalQty = 0
          Orders.map((a) => {
            //OrderNo
            const price = prices.find((b) => b.id == a.id)
            if (!price) return { Error: 'Invalid item' }
            //let output = [insert[0], a.id, a.Qty + price.UnitPrice, username]
            saveItems = [
              ...saveItems,
              [
                OrderID,
                customer[0].id,
                a.id,
                price.ProductID,
                a.Qty * price.UnitPrice,
                a.Qty,
                price.UnitPrice,
                SalesPerson,
                client,
                OrderNo,
              ],
            ]
            //if (a.Qty) totalQty = totalQty + parseFloat(a.Qty)
          })

          console.log(saveItems)

          // OrderID, UserID, ShipperID, ProductID, InvoiceAmount, Qty, PriceID, UnitPrice, SalesPerson, SalesClient, OrderNo
          const insertCart = await db.sequelize.query(
            `INSERT INTO 
          spa1.ao_cartlist
          (OrderID, UserID, PriceID, ProductID, InvoiceAmount, Qty, UnitPrice, SalesPerson, SalesClient, OrderNo) VALUES ?`,
            {
              replacements: [saveItems],
              type: QueryTypes.INSERT,
            }
          )

          const cartSave = await db.sequelize.query(
            `SELECT 
               OrderID, OrderNo, UserID, ShipperID, ProductID, ProductNo, OrderDate, RequiredDate, Freight, SalesTax, TimeStamp, TransactStatus, InvoiceAmount, PaymentDate, Qty, CreatedDate, PriceID, UnitPrice, SalesPerson, SalesClient,
              (select b.Category from ao_productlist b where b.id=a.ProductID) as Category
            FROM spa1.ao_cartlist a WHERE OrderID = ? AND UserID = ?
              `,
            {
              replacements: [OrderID, customer[0].id],
              type: QueryTypes.SELECT,
            }
          )

          const carts = await db.sequelize.query(
            `SELECT 
              a.SalesPerson, a.SalesClient, a.OrderID, a.OrderNo, a.UserID, a.ShipperID, a.ProductID, a.ProductNo, a.OrderDate, a.RequiredDate, a.Freight, a.SalesTax, a.TimeStamp, a.TransactStatus, a.InvoiceAmount, a.PaymentDate, a.Qty, a.CreatedDate, a.PriceID, a.UnitPrice, b.Category as PriceCategory, b.Uom as PriceUom, b.ServiceTimes as PriceServiceTimes,
              (SELECT ProductName from spa1.ao_productlist where id=a.ProductID ) as ProductName
            FROM spa1.ao_cartlist a LEFT JOIN spa1.ao_pricelist b ON b.id = a.PriceID WHERE a.OrderID = ? AND a.UserID = ?
              `,
            {
              replacements: [OrderID, customer[0].id],
              type: QueryTypes.SELECT,
            }
          )

          const totalInvoice = cartSave.reduce((a, b) => b.InvoiceAmount + a, 0)

          //console.log('cartSave', cartSave)
          cartSave.map((cart) => {
            if (cart.Category == 1) {
              cart.Status = 'Not Collected'
            } else {
              cart.Status = 'Completed'
            }
            cart.Client = client
          })

          const transactionSave = []

          const StatusCodeCompleted = await db.sequelize.query(
            `SELECT * FROM ${client}.ao_statuslist WHERE StatusCode='COMPLETED'`,
            {
              replacements: [],
              type: QueryTypes.SELECT,
            }
          )

          const totalProduct = cartSave.find((cart) => cart.Category == 1)

          if (!totalProduct) {
            const updateOrder = await db.sequelize.query(
              `UPDATE
                spa1.ao_orderheader
              SET 
                Status=?,
                StatusCode=?
                WHERE id=?
                `,
              {
                replacements: [
                  StatusCodeCompleted[0].StatusName,
                  StatusCodeCompleted[0].StatusCode,
                  OrderID,
                ],
                type: QueryTypes.UPDATE,
              }
            )
          }

          /*
            `PaymentMode` varchar(145) DEFAULT NULL,
            `PriceID` smallint DEFAULT NULL,
            `Refund` smallint DEFAULT NULL,
            `RefundReason` text,
            `RefundBy` varchar(45) DEFAULT NULL,
            `RefundOn` datetime DEFAULT NULL,
            `ProductID` smallint DEFAULT NULL,
            `OrderNo` varchar(45) DEFAULT NULL,
            `TransactionLocation` varchar(45) DEFAULT NULL,
          */

          carts.map((cart) => {
            let output = {
              UserID: customer[0].id,
              DebitCredit: 'CREDIT',
              TransactionID: OrderID,
              TransactionRef: cart.ProductID,
              TransactionDate: fn('NOW'),
              TransactionAmount: cart.InvoiceAmount,
              Active: 1,
              CreatedOn: fn('NOW'),
              CreatedBy: customer[0].StaffID,
              SalesPersonSingle: cart.SalesPerson,
              SalesPersonSingleClient: cart.SalesClient,
              SalesPersonPassive: customer[0].SalesPerson,
              SalesPersonPassiveClient: customer[0].SalesClient,
              Client: client,
              Qty: cart.Qty,
              UnitPrice: cart.UnitPrice,
              PaymentMode: PaymentMode,
              PriceID: cart.PriceID,
              ProductID: cart.ProductID,
              OrderNo: OrderNo,
              TransactionLocation: 'ONSITE',
            }

            if (cart.PriceCategory == 'Service') {
              output.TransactionType = 'BUY_SERVICE'
              output.TransactionDesc = cart.PriceUom
              output.ServiceTimes = cart.PriceServiceTimes * cart.Qty
            } else {
              output.TransactionType = 'BUY_PRODUCT'
              output.TransactionDesc = cart.ProductName
            }
            transactionSave.push(output)
          })

          const insertOrderDetails = await db.orderdetail.bulkCreate(cartSave)

          console.log('TRANSACTION', transactionSave)

          const insertTransaction = await db.transactionlist.bulkCreate(
            transactionSave
          )

          const remove = await db.sequelize.query(
            `DELETE FROM
              spa1.ao_cartlist 
            WHERE UserID=? AND OrderID is null`,
            {
              replacements: [customer[0].id],
              type: QueryTypes.DELETE,
            }
          )

          const orderheader = await db.sequelize.query(
            `SELECT a.*, b.CustomerName, b.Address1, b.Address2 FROM spa1.ao_orderheader a LEFT JOIN spa1.ao_customerlist b ON b.CustomerCode = a.CustomerCode where a.id=?`,
            {
              replacements: [OrderID],
              type: QueryTypes.SELECT,
            }
          )

          return orderheader[0]
        })

      /*  let saveItems = []
      let totalQty = 0
      Orders.map((a) => {
        let output = [insert[0], a.id, a.Qty, username]
        saveItems = [...saveItems, [insert[0], a.id, a.Qty, username]]
        if (a.Qty) totalQty = totalQty + parseFloat(a.Qty)
      })

      const insertStock = await db.sequelize.query(
        `INSERT INTO 
          ${client}.ao_stocklist
          (StockTakeID, ProductID, Qty, CreatedBy) VALUES ?`,
        {
          replacements: [saveItems],
          type: QueryTypes.INSERT,
        }
      )

      const updateStockTake = await db.sequelize.query(
        `UPDATE
          ${client}.ao_stocktakelist
        SET 
          Qty=?
          WHERE id=?
          `,
        {
          replacements: [totalQty, insert[0]],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          b.* 
        FROM ${client}.ao_stocktakelist b
        WHERE b.id=?
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0] */
    },

    createorder: async (
      root,
      { CustomerCode, NoOfCarton, Remarks, User },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB('spa1')

      const orderNoIncrease = await db.freightrunno.update(
        {
          LastNo: literal('LastNo + 1'),
        },
        {
          where: {
            Prefix: 'ODR',
          },
        }
      )

      const orderNo = await db.freightrunno.findOne({
        where: {
          Prefix: 'ODR',
        },
      })

      const prefix = await db.freightconfig.findOne({
        where: {
          ParmKey: 'ODRPFX',
        },
      })

      const OrderNo = `${prefix.ParamValue}${db.pad(orderNo.LastNo, 5)}`

      const StatusCode = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_statuslist WHERE StatusCode='ORDERCREATED'`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )
      /* 
      let token = ''
      require('crypto').randomBytes(48, function (err, buffer) {
        token = buffer.toString('hex')
      }) */

      return db.order
        .create({
          OrderNo,
          CustomerCode,
          NoOfCarton,
          Remarks,
          Status: StatusCode[0] && StatusCode[0].StatusName,
          StatusCode: StatusCode[0] && StatusCode[0].StatusCode,
          CreatedBy: User,
          CreatedOn: fn('NOW'),
          /* Token:token */
        })
        .then(async (a) => {
          const cartSave = await db.sequelize.query(
            `SELECT 
              OrderID, OrderNo, UserID, ShipperID, ProductID, ProductNo, OrderDate, RequiredDate, Freight, SalesTax, TimeStamp, TransactStatus, InvoiceAmount, PaymentDate, Qty, CreatedDate, PriceID, UnitPrice, SalesPerson, SalesClient,
              (select b.Category from ao_productlist b where b.id=a.ProductID) as Category
            FROM spa1.ao_cartlist a WHERE OrderID = ? AND UserID = ?
              `,
            {
              replacements: [a.id, user[0].id],
              type: QueryTypes.SELECT,
            }
          )

          const carts = await db.sequelize.query(
            `SELECT 
              a.SalesPerson, a.SalesClient, a.OrderID, a.OrderNo, a.UserID, a.ShipperID, a.ProductID, a.ProductNo, a.OrderDate, a.RequiredDate, a.Freight, a.SalesTax, a.TimeStamp, a.TransactStatus, a.InvoiceAmount, a.PaymentDate, a.Qty, a.CreatedDate, a.PriceID, a.UnitPrice, b.Category as PriceCategory, b.Uom as PriceUom, b.ServiceTimes as PriceServiceTimes,
              (SELECT ProductName from spa1.ao_productlist where id=a.ProductID ) as ProductName
            FROM spa1.ao_cartlist a LEFT JOIN spa1.ao_pricelist b ON b.id = a.PriceID WHERE a.OrderID =? AND a.UserID = ?
              `,
            {
              replacements: [a.id, user[0].id],
              type: QueryTypes.SELECT,
            }
          )

          const totalInvoice = cartSave.reduce((a, b) => b.InvoiceAmount + a, 0)

          //console.log('cartSave', cartSave)
          cartSave.map((cart) => {
            if (cart.Category == 1) {
              cart.Status = 'Not Collected'
            } else {
              cart.Status = 'Completed'
            }
            cart.Client = user[0].Branch
          })

          const transactionSave = []

          /*
            `PaymentMode` varchar(145) DEFAULT NULL,
            `PriceID` smallint DEFAULT NULL,
            `Refund` smallint DEFAULT NULL,
            `RefundReason` text,
            `RefundBy` varchar(45) DEFAULT NULL,
            `RefundOn` datetime DEFAULT NULL,
            `ProductID` smallint DEFAULT NULL,
            `OrderNo` varchar(45) DEFAULT NULL,
            `TransactionLocation` varchar(45) DEFAULT NULL,
          */

          carts.map((cart) => {
            let output = {
              UserID: user[0].id,
              DebitCredit: 'CREDIT',
              TransactionID: a.id,
              TransactionRef: cart.ProductID,
              TransactionDate: fn('NOW'),
              TransactionAmount: cart.InvoiceAmount,
              Active: 1,
              CreatedOn: fn('NOW'),
              CreatedBy: customer[0].StaffID,
              SalesPersonSingle: cart.SalesPerson,
              SalesPersonSingleClient: cart.SalesClient,
              SalesPersonPassive: user[0].SalesPerson,
              SalesPersonPassiveClient: user[0].SalesClient,
              Client: user[0].Branch,
              Qty: cart.Qty,
              UnitPrice: cart.UnitPrice,
              PaymentMode: PaymentMethod,
              PriceID: cart.PriceID,
              ProductID: cart.ProductID,
              OrderNo: OrderNo,
            }

            if (cart.PriceCategory == 'Service') {
              output.TransactionType = 'BUY_SERVICE'
              output.TransactionDesc = cart.PriceUom
              output.ServiceTimes = cart.PriceServiceTimes * cart.Qty
            } else {
              output.TransactionType = 'BUY_PRODUCT'
              output.TransactionDesc = cart.ProductName
            }
            transactionSave.push(output)
          })

          const insert = await db.orderdetail.bulkCreate(cartSave)

          console.log('TRANSACTION', transactionSave)

          const insertTransaction = await db.transactionlist.bulkCreate(
            transactionSave
          )

          const remove = await db.sequelize.query(
            `DELETE FROM
              spa1.ao_cartlist 
            WHERE UserID=? AND OrderID is null`,
            {
              replacements: [user[0].id],
              type: QueryTypes.DELETE,
            }
          )

          console.log('transactionamount', totalInvoice)
          //TransactionID, TransactionRef, TransactionDate, TransactionAmount, PaymentTo, Active, DebtorCode, TransactionType, TransactionDesc, CreatedOn, CreatedBy, PaymentTermID
          /*  const inserttransaction = await db.sequelize.query(
            `INSERT
            spa1.ao_transactionlist
            SET
            TransactionID = ?, TransactionRef = '', TransactionDate  = NOW(), TransactionAmount = ?, Active = 1, TransactionType = 'Order - Product', TransactionDesc = 'Purchased Product Online', CreatedBy = ?,  CreatedOn = NOW()`,
            {
              replacements: [OrderNo, totalInvoice, username],
              type: QueryTypes.INSERT,
            }
          ) */

          const query = await db.sequelize.query(
            `SELECT a.*, b.CustomerName, b.Address1, b.Address2 FROM ${client}.ao_orderheader a LEFT JOIN ${client}.ao_customerlist b ON b.CustomerCode = a.CustomerCode where a.id=?`,
            {
              replacements: [a.id],
              type: QueryTypes.SELECT,
            }
          )

          return query[0]
        })
    },
    updatezone: async (
      root,
      { id, ZoneID, Status, Remarks, CreatedBy, StatusText },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB(client)
      const StatusCode = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_statuslist WHERE StatusName=?`,
        {
          replacements: [Status],
          type: QueryTypes.SELECT,
        }
      )

      return db.order
        .update(
          {
            ZoneID,
            Status,
            StatusCode: StatusCode[0] && StatusCode[0].StatusCode,
            Remarks,
          },
          {
            where: {
              id,
            },
          }
        )
        .then(async () => {
          //id, OrderID, Status, CreatedBy, CreatedOn

          if (StatusText) Status = `${Status} - ${StatusText}`

          const insert = await db.sequelize.query(
            `INSERT
            ${client}.ao_orderstatus
            SET
            OrderID = ?, Status = ?, CreatedBy = ?, StatusCode = ?, CreatedOn = NOW()`,
            {
              replacements: [
                id,
                Status,
                CreatedBy,
                StatusCode[0] && StatusCode[0].StatusCode,
              ],
              type: QueryTypes.INSERT,
            }
          )

          const query = await db.sequelize.query(
            `SELECT a.*, b.CustomerName, b.Address1, b.Address2 FROM ${client}.ao_orderheader a LEFT JOIN ${client}.ao_customerlist b ON b.CustomerCode = a.CustomerCode where a.id=?`,
            {
              replacements: [id],
              type: QueryTypes.SELECT,
            }
          )

          return query[0]
          /* db.order.findOne({
          where: {
            id
          }
        }) */
        })
    },
    updateorder: (
      root,
      { id, CustomerCode, NoOfCarton, Remarks, User },
      { username, role, client, sessionid, iat }
    ) =>
      db.order
        .update(
          {
            CustomerCode,
            NoOfCarton,
            Remarks,
            ModifiedBy: User,
            LastModified: fn('NOW'),
          },
          {
            where: {
              id,
            },
          }
        )
        .then(async (a) => {
          const query = await db.sequelize.query(
            `SELECT a.*, b.CustomerName, b.Address1, b.Address2 FROM ${client}.ao_orderheader a LEFT JOIN ${client}.ao_customerlist b ON b.CustomerCode = a.CustomerCode where a.id=?`,
            {
              replacements: [id],
              type: QueryTypes.SELECT,
            }
          )

          return query[0]
        }),
    receivedorder: async (
      root,
      { Token, SalesPerson, Password },
      { username, role, client, sessionid, iat }
    ) => {
      /* const loggedIn = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_puchcarddetail WHERE Username=? AND DATE(PunchDate)=? AND PunchType='Checkin'`,
        {
          replacements: [
            username,
            moment(new Date()).tz('Asia/Kuala_Lumpur').format().split('T')[0],
          ],
          type: QueryTypes.SELECT,
        }
      )

      if (!loggedIn[0]) return { Error: 'You are not Punched In' } */

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM ${client}.ao_stafflist
        WHERE StaffID=? AND Password2=?
        `,
        {
          replacements: [SalesPerson, Password],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return { Error: 'Login error, please try again.' }

      const orderdetails = await db.sequelize.query(
        `SELECT 
          a.*,
          b.ProductName,
          b.Category,
          (select FileName from spa1.ao_uploadlist  where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
        FROM spa1.ao_orderheader c 
        LEFT JOIN spa1.ao_orderdetail a ON a.OrderID = c.id
        LEFT JOIN spa1.ao_productlist b ON a.ProductID = b.id
        WHERE c.Token=? AND b.Category='1'
        ORDER BY b.Ordering asc`,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      const save = await db.sequelize.query(
        `UPDATE
            spa1.ao_orderheader
          SET
            Status=?,
            StatusCode=?,
            Client=?,
            Received='Received',
            ReceivedBy=?,
            ReceivedOn=NOW()
          WHERE Token=?`,
        {
          replacements: ['Completed', 'COMPLETED', client, SalesPerson, Token],
          type: QueryTypes.UPDATE,
        }
      )

      const savedetail = await db.sequelize.query(
        `UPDATE
            spa1.ao_orderdetail
          SET
            Status='Completed'
          WHERE OrderNo=?`,
        {
          replacements: [orderdetails[0].OrderNo],
          type: QueryTypes.UPDATE,
        }
      )

      orderdetails.map(async (a) => {
        await db.sequelize.query(
          `INSERT INTO 
            ${client}.ao_stocklist
          SET 
            OrderID=?,
            ProductID=?,
            Title='Stock Out',
            Movement='Out',
            Qty=?,
            CreatedOn=NOW(),
            CreatedBy=?`,
          {
            replacements: [a.OrderID, a.ProductID, a.Qty, SalesPerson],
            type: QueryTypes.INSERT,
          }
        )
      })

      const query = await db.sequelize.query(
        `SELECT 
            a.*, 
            DATEDIFF(NOW(), a.CreatedOn) as DateLapse, 
            CONCAT(b.FirstName,' ', b.LastName, '\n', b.Phone) as FullName,
            b.FirstName, 
            b.LastName, 
            b.Address1, 
            b.Address2, 
            b.State, 
            b.City, 
            b.PostCode, 
            b.Phone, 
            b.Email,
            b.Branch,
            (select sum(InvoiceAmount) as TotalAmount from spa1.ao_orderdetail where OrderID = a.id ) as TotalAmount,
            (select (sum(InvoiceAmount) + a.DeliveryCharges) as FinalTotal from spa1.ao_orderdetail where OrderID = a.id ) as FinalTotal
          FROM spa1.ao_orderheader a 
          LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.UserID 
          WHERE a.Token=?`,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    removeorder: (root, { id }, { username, role, client, sessionid, iat }) => {
      db.changeDB(client)
      return db.order.destroy({
        where: {
          id,
        },
      })
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
