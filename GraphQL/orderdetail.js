const { gql } = require('apollo-server-express')
const db = require('../database')
const crypto = require('crypto')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type OrderDetail {
    id: ID
    OrderID: ID
    OrderNo: String
    UserID: ID
    ProductID: ID
    InvoiceAmount: Float
    Qty: Int
    CreatedDate: Date
    PriceID: ID
    UnitPrice: Float
    ProductName: String
    ProductName2: String
    Category: String
    ProductImage: String
    Status: String
    Refund: Int
    RefundReason: String
    RefundBy: String
    RefundOn: Date
  }

  type MyOrder {
    id: ID
    OrderNo: String
    Remarks: String
    Status: String
    CreatedBy: String
    CreatedOn: Date
    ModifiedBy: String
    LastModified: Date
    StatusCode: String
    TotalItem: Int
    TotalAmount: Float
    PaymentMethod: String
    DeliveryCharges: Float
    Token: String
    OrderDetails: [OrderDetail]
  }
  type MyPass {
    id: ID
    Title: String
    ServiceTimes: String
    PriceID: String
    ServiceExpiryDate: String
    TotalUsed: Int
  }

  extend type Query {
    myorders: [MyOrder]
    myorder(OrderNo: String!): MyOrder
    mypass(CustomerID: ID): [MyPass]
  }

  extend type Mutation {
    cartorder(PaymentMethod: String): Int
  }
`

const resolvers = {
  Query: {
    mypass: async (
      root,
      { CustomerID },
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT * from spa1.ao_stafflist WHERE id=? limit 1`,
        {
          replacements: [CustomerID],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return { Error: 'No such user' }

      const query = await db.sequelize.query(
        `SELECT
            a.id,
            SUM(a.ServiceTimes) AS ServiceTimes,
            a.PriceID,
            a.TransactionDesc as Title,
            a.ServiceExpiryDate,
            b.TotalUsed,
            d.MinBooking
        FROM
            spa1.ao_transactionlist a
                LEFT JOIN
            (SELECT
                SUM(TotalPerson) AS TotalUsed, PriceID
            FROM
                spa1.ao_bookinglist
            WHERE
                CreatedBy = ?
            GROUP BY PriceID) b ON b.PriceID = a.PriceID
                LEFT JOIN
            spa1.ao_pricelist d ON d.id = a.PriceID
        WHERE
            a.CreatedBy = ?
                AND a.TransactionType = 'BUY_SERVICE'
        GROUP BY a.PriceID
        order by id desc`,
        {
          replacements: [user[0].StaffID, user[0].StaffID],
          type: QueryTypes.SELECT,
        }
      )

      return query || []
    },
    myorders: async (
      root,
      args,
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT * from spa1.ao_stafflist WHERE staffid=? limit 1`,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT
            a.*,
            (select count(*) from spa1.ao_orderdetail where OrderID = a.id) as TotalItem,
            (select sum(InvoiceAmount) from spa1.ao_orderdetail where OrderID = a.id) as TotalAmount
        from spa1.ao_orderheader a
        WHERE a.UserID=?
        ORDER BY a.id Desc`,
        {
          replacements: [user[0].id],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
    myorder: async (
      root,
      { OrderNo },
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT * from spa1.ao_stafflist WHERE staffid=? limit 1`,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const query = await db.sequelize.query(
        `SELECT
            a.*,
            (select count(*) from spa1.ao_orderdetail where OrderID = a.id) as TotalItem,
            (select sum(InvoiceAmount) from spa1.ao_orderdetail where OrderID = a.id) as TotalAmount
        from spa1.ao_orderheader a
        WHERE a.UserID=? AND a.OrderNo=?
        ORDER BY a.id Desc`,
        {
          replacements: [user[0].id, OrderNo],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
  MyOrder: {
    OrderDetails: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT
          a.*,
          b.ProductName,
          b.Category,
          (select FileName from spa1.ao_uploadlist where ModuleID=a.ProductID and Module='UPLOAD_PRODUCTIMAGE' and Ordering=0 limit 1) as ProductImage
        FROM spa1.ao_orderdetail a
        LEFT JOIN spa1.ao_productlist b ON a.ProductID = b.id
        WHERE a.OrderID=?
        ORDER BY b.Ordering asc`,
        {
          replacements: [root.id],
          type: QueryTypes.SELECT,
        }
      )

      return query
    },
  },
  Mutation: {
    cartorder: async (
      root,
      { PaymentMethod },
      { username, role, client, sessionid, iat }
    ) => {
      // db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT
          a.*,
          b.Branch
        FROM spa1.ao_stafflist a
        LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.id
        WHERE a.StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      const orderNoIncrease = await db.freightrunno.update(
        {
          LastNo: literal('LastNo + 1'),
        },
        {
          where: {
            Prefix: 'ODR',
          },
        }
      )

      const orderNo = await db.freightrunno.findOne({
        where: {
          Prefix: 'ODR',
        },
      })

      const OrderNo = `${db.pad(orderNo.LastNo, 6)}`

      const StatusCode = await db.sequelize.query(
        `SELECT * FROM spa1.ao_statuslist WHERE StatusCode='ORDERCREATED'`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )
      /*
      const buyer = await db.sequelize.query(
        `SELECT
            Postcode
          FROM spa1.ao_buyerlist
          WHERE UserID=? LIMIT 1
        `,
        {
          replacements: [user[0].id],
          type: QueryTypes.SELECT,
        }
      )

      const postcodes = await db.sequelize.query(
        `SELECT * FROM spa1.ao_postcode WHERE PostCode = ? `,
        {
          replacements: [buyer[0].Postcode],
          type: QueryTypes.SELECT,
        }
      )

      console.log('here', postcodes[0].Price) */

      const token = crypto.randomBytes(64).toString('hex')

      return db.order
        .create({
          OrderNo,
          Status: StatusCode[0] && StatusCode[0].StatusName,
          StatusCode: StatusCode[0] && StatusCode[0].StatusCode,
          UserID: user[0].id,
          CreatedBy: username,
          /*  DeliveryCharges: postcodes[0].Price, */
          PaymentMethod,
          CreatedOn: fn('NOW'),
          Token: token,
          Client: user[0].Branch,
        })
        .then(async (a) => {
          const cartSave = await db.sequelize.query(
            `SELECT
              ? as OrderID, ? as OrderNo, UserID, ShipperID, ProductID, ProductNo, OrderDate, RequiredDate, Freight, SalesTax, TimeStamp, TransactStatus, InvoiceAmount, PaymentDate, Qty, CreatedDate, PriceID, UnitPrice, SalesPerson, SalesClient,
              (select b.Category from ao_productlist b where b.id=a.ProductID) as Category
            FROM spa1.ao_cartlist a WHERE OrderID is null AND UserID = ?
              `,
            {
              replacements: [a.id, OrderNo, user[0].id],
              type: QueryTypes.SELECT,
            }
          )

          const carts = await db.sequelize.query(
            `SELECT
              a.SalesPerson, a.SalesClient, ? as OrderID, ? as OrderNo, a.UserID, a.ShipperID, a.ProductID, a.ProductNo, a.OrderDate, a.RequiredDate, a.Freight, a.SalesTax, a.TimeStamp, a.TransactStatus, a.InvoiceAmount, a.PaymentDate, a.Qty, a.CreatedDate, a.PriceID, a.UnitPrice, b.Category as PriceCategory, b.Uom as PriceUom, b.ServiceTimes as PriceServiceTimes,
              (SELECT ProductName from spa1.ao_productlist where id=a.ProductID ) as ProductName
            FROM spa1.ao_cartlist a LEFT JOIN spa1.ao_pricelist b ON b.id = a.PriceID WHERE a.OrderID is null AND a.UserID = ?
              `,
            {
              replacements: [a.id, OrderNo, user[0].id],
              type: QueryTypes.SELECT,
            }
          )

          const totalInvoice = cartSave.reduce((a, b) => b.InvoiceAmount + a, 0)

          //console.log('cartSave', cartSave)
          cartSave.map((cart) => {
            if (cart.Category == 1) {
              cart.Status = 'Not Collected'
            } else {
              cart.Status = 'Completed'
            }
            cart.Client = user[0].Branch
          })

          const transactionSave = []

          /*
            `PaymentMode` varchar(145) DEFAULT NULL,
            `PriceID` smallint DEFAULT NULL,
            `Refund` smallint DEFAULT NULL,
            `RefundReason` text,
            `RefundBy` varchar(45) DEFAULT NULL,
            `RefundOn` datetime DEFAULT NULL,
            `ProductID` smallint DEFAULT NULL,
            `OrderNo` varchar(45) DEFAULT NULL,
            `TransactionLocation` varchar(45) DEFAULT NULL,
          */

          carts.map((cart) => {
            let output = {
              UserID: user[0].id,
              DebitCredit: 'CREDIT',
              TransactionID: a.id,
              TransactionRef: cart.ProductID,
              TransactionDate: fn('NOW'),
              TransactionAmount: cart.InvoiceAmount,
              Active: 1,
              CreatedOn: fn('NOW'),
              CreatedBy: username,
              SalesPersonSingle: cart.SalesPerson,
              SalesPersonSingleClient: cart.SalesClient,
              SalesPersonPassive: user[0].SalesPerson,
              SalesPersonPassiveClient: user[0].SalesClient,
              Client: user[0].Branch,
              Qty: cart.Qty,
              UnitPrice: cart.UnitPrice,
              PaymentMode: PaymentMethod,
              PriceID: cart.PriceID,
              ProductID: cart.ProductID,
              OrderNo: OrderNo,
            }

            if (cart.PriceCategory == 'Service') {
              output.TransactionType = 'BUY_SERVICE'
              output.TransactionDesc = cart.PriceUom
              output.ServiceTimes = cart.PriceServiceTimes
            } else {
              output.TransactionType = 'BUY_PRODUCT'
              output.TransactionDesc = cart.ProductName
            }
            transactionSave.push(output)
          })

          const insert = await db.orderdetail.bulkCreate(cartSave)

          console.log('TRANSACTION', transactionSave)

          const insertTransaction = await db.transactionlist.bulkCreate(
            transactionSave
          )

          const remove = await db.sequelize.query(
            `DELETE FROM
              spa1.ao_cartlist
            WHERE UserID=? AND OrderID is null`,
            {
              replacements: [user[0].id],
              type: QueryTypes.DELETE,
            }
          )

          console.log('transactionamount', totalInvoice)
          //TransactionID, TransactionRef, TransactionDate, TransactionAmount, PaymentTo, Active, DebtorCode, TransactionType, TransactionDesc, CreatedOn, CreatedBy, PaymentTermID
          /*  const inserttransaction = await db.sequelize.query(
            `INSERT
            spa1.ao_transactionlist
            SET
            TransactionID = ?, TransactionRef = '', TransactionDate  = NOW(), TransactionAmount = ?, Active = 1, TransactionType = 'Order - Product', TransactionDesc = 'Purchased Product Online', CreatedBy = ?,  CreatedOn = NOW()`,
            {
              replacements: [OrderNo, totalInvoice, username],
              type: QueryTypes.INSERT,
            }
          ) */

          return OrderNo
        })
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
