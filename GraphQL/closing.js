const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')
const moment = require('moment-timezone')

const typeDefs = gql`
  type ClosingHeader {
    id: ID
    ClosingDate: Date
    ClosingDateStart: Date
    ClosingType: String
    StockOutQty: Float
    StockInQty: Float
    ClosingBalanceQty: Float
    OpeningBalanceQty: Float
    CreatedOn: Date
    CreatedBy: String
    UpdatedOn: Date
    UpdatedBy: String
    Error: String
  }

  type ClosingHeaderDelete {
    id: ID
    Error: String
  }

  type ClosingDetail {
    id: ID
    ClosingID: ID
    ItemID: ID
    ItemName: String
    StockOutQty: Float
    StockInQty: Float
    ClosingBalanceQty: Float
    OpeningBalanceQty: Float
    CreatedOn: Date
    CreatedBy: String
  }

  extend type Query {
    closingheaders(BranchCode: ID!, ClosingType: String): [ClosingHeader]
    closingdetails(BranchCode: ID!, ClosingID: ID): [ClosingDetail]
  }

  extend type Mutation {
    insertclosingheader(ClosingDate: Date, BranchCode: ID!): ClosingHeader
    removeclosingheader(BranchCode: ID!, id: ID): ClosingHeaderDelete
  }
`

const resolvers = {
  Query: {
    closingheaders: async (
      root,
      { BranchCode, ClosingType },
      { username, role, client, sessionid, iat }
    ) => {
      return db.sequelize.query(
        `SELECT 
            a.*
          FROM ${BranchCode}.ao_closingheader a
          WHERE a.ClosingType=?
          ORDER BY a.id DESC
          `,
        {
          replacements: [ClosingType],
          type: QueryTypes.SELECT,
        }
      )
    },
    closingdetails: async (
      root,
      { BranchCode, ClosingID },
      { username, role, client, sessionid, iat }
    ) => {
      return db.sequelize.query(
        `SELECT 
            a.*,
            (CASE
              WHEN b.Uom = 'each' THEN c.ProductName
              ELSE CONCAT(c.ProductName, ' - ', b.Uom)
          END) AS ItemName
          FROM ${BranchCode}.ao_closingdetail a
          LEFT JOIN spa1.ao_pricelist b ON b.ProductID = a.ItemID
          LEFT JOIN spa1.ao_productlist c ON c.id = a.ItemID
          WHERE a.ClosingID = ?
          ORDER BY ItemName ASC
          `,
        {
          replacements: [ClosingID],
          type: QueryTypes.SELECT,
        }
      )
    },
  },

  Mutation: {
    insertclosingheader: async (
      root,
      { ClosingDate, BranchCode },
      { username, role, client, sessionid, iat }
    ) => {
      const closingDate = moment(ClosingDate)
        .tz('Asia/Kuala_Lumpur')
        .format('YYYY-MM-DD')

      const checkuser = await db.sequelize.query(
        `SELECT 
            Department 
          FROM ${client}.ao_stafflist
          WHERE StaffID=?
          `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      if (checkuser[0].Department != 'Management')
        return { Error: 'Not Allowed' }

      const laststocktake = await db.sequelize.query(
        ` SELECT * FROM ${BranchCode}.ao_closingheader
            ORDER BY ClosingDate DESC LIMIT 1
            `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      if (laststocktake[0] && laststocktake[0].ClosingDate > closingDate)
        return { Error: 'Invalid Date' }

      const laststockdetails = await db.sequelize.query(
        ` SELECT b.ItemID FROM ${BranchCode}.ao_closingheader a
            LEFT OUTER JOIN ${BranchCode}.ao_closingdetail b ON b.ClosingID = a.id
            ORDER BY a.ClosingDate DESC
            `,
        {
          replacements: [BranchCode],
          type: QueryTypes.SELECT,
        }
      )

      const products = await db.sequelize.query(
        `SELECT 
            b.*,
            (CASE
                WHEN Uom = 'each' THEN a.ProductName
                ELSE CONCAT(a.ProductName, ' - ', b.Uom)
            END) AS ProductName
        FROM
            spa1.ao_productlist a
                LEFT JOIN
            spa1.ao_pricelist b ON b.ProductID = a.id
            WHERE a.Category = 1`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      const stockOut = await db.sequelize.query(
        `SELECT
            sum(a.Qty) as Qty,
            a.ProductID,
          GROUP_CONCAT(a.id SEPARATOR ',') AS DataEntryID
          FROM ${BranchCode}.ao_stocklist a
          LEFT JOIN spa1.ao_productlist b ON b.id = a.ProductID
          WHERE (a.CreatedOn BETWEEN ? AND ? OR a.CreatedOn = ?) AND b.Category = 1 AND a.Movement = 'Out'
          GROUP BY a.ProductID
          `,
        {
          replacements: [
            (laststocktake[0] && laststocktake[0].ClosingDate) || '2021-01-01',
            closingDate,
            closingDate,
          ],
          type: QueryTypes.SELECT,
        }
      )

      if (stockOut.length == 0) return { Error: 'Data Entry is empty' }

      const insert = await db.sequelize.query(
        `INSERT INTO 
            ${BranchCode}.ao_closingheader 
          SET ClosingDate=?, ClosingDateStart=?, ClosingType='STOCK', CreatedBy=?, CreatedOn=NOW()
          `,
        {
          replacements: [
            closingDate,
            (laststocktake[0] && laststocktake[0].ClosingDate) || '2021-01-01',
            username,
          ],
          type: QueryTypes.INSERT,
        }
      )

      const convertBags = function (kg, kgtobags) {
        return kg / kgtobags
      }

      let closingOuts = []
      let closingOuts2 = []
      let totalStockOutQty = 0
      let totalStockOutBags = 0

      stockOut.map((a) => {
        if (a.ProductID) {
          closingOuts = [
            ...closingOuts,
            [insert[0], a.ProductID, a.Qty, username, a.DataEntryID],
          ]
          if (a.Qty) {
            totalStockOutQty = totalStockOutQty + parseFloat(a.Qty)
          }
        }
      })

      stockOut.map((a) => {
        if (a.ProductID) {
          closingOuts2 = [
            ...closingOuts2,
            { ItemID: a.ProductID, TotalQty: parseFloat(a.Qty) },
          ]
        }
      })

      if (closingOuts.length > 0) {
        const insertClosingOUTDetails = await db.sequelize.query(
          `INSERT INTO 
            ${BranchCode}.ao_closingoutdetail
            (ClosingID, ItemID, Qty, CreatedBy, DataEntryID) VALUES ?`,
          {
            replacements: [closingOuts],
            type: QueryTypes.INSERT,
          }
        )
      }

      //Stock IN
      const stockIn = await db.sequelize.query(
        `SELECT
            a.StockTakeDate,
            b.ProductID,
            a.TransactionType,
            GROUP_CONCAT(b.id SEPARATOR ',') AS DataEntryID,
            SUM(CASE 
              WHEN a.TransactionType = "ADD" 
              THEN b.Qty 
              ELSE 0 
              END) AS TotalAdd,
            SUM(CASE 
              WHEN a.TransactionType = "REDUCE" 
              THEN b.Qty 
              ELSE 0 
              END) AS TotalReduce
          FROM ${BranchCode}.ao_stocktakelist a
          LEFT OUTER JOIN ${BranchCode}.ao_stocklist b ON b.StockTakeID = a.id
          
          LEFT JOIN spa1.ao_productlist c ON c.id = b.ProductID
          where (a.StockTakeDate BETWEEN ? AND ?) AND Category = 1
          group by b.ProductID
            `,
        {
          replacements: [
            (laststocktake[0] && laststocktake[0].ClosingDate) || '2021-01-01',
            closingDate,
          ],
          type: QueryTypes.SELECT,
        }
      )

      //if (stockIn.length == 0) return { Error: 'Data Entry is empty' }

      let closingIns = []
      let closingIns2 = []
      let totalStockInQty = 0
      let totalStockInBags = 0
      stockIn.map((a) => {
        let Qty = a.TotalAdd - a.TotalReduce || 0
        if (a.ProductID) {
          closingIns = [
            ...closingIns,
            [insert[0], a.ProductID, Qty, username, a.DataEntryID],
          ]
          if (Qty) {
            totalStockInQty = totalStockInQty + parseFloat(Qty)
          }
        }
      })

      stockIn.map((a) => {
        let Qty = a.TotalAdd - a.TotalReduce || 0
        if (a.ProductID) {
          closingIns2 = [...closingIns2, { ItemID: a.ProductID, TotalQty: Qty }]
        }
      })

      if (closingIns.length > 0) {
        const insertClosingINDetails = await db.sequelize.query(
          `INSERT INTO 
            ${BranchCode}.ao_closingindetail
            (ClosingID, ItemID, Qty, CreatedBy, DataEntryID) VALUES ?`,
          {
            replacements: [closingIns],
            type: QueryTypes.INSERT,
          }
        )
      }

      //Stock IN ends

      console.log('stockbalances', laststockdetails, closingIns2, closingOuts2)
      let stockBalances = []

      products.map((product) => {
        let lastStock = laststockdetails.find(
          (a) => a.ItemID == product.ProductID
        )
        let closingIn = closingIns2.find((a) => a.ItemID == product.ProductID)
        let closingOut = closingOuts2.find((a) => a.ItemID == product.ProductID)

        if (lastStock || closingIn || closingOut) {
          let totalBalance =
            ((lastStock && lastStock.ClosingBalanceQty) || 0) +
            ((closingIn && closingIn.TotalQty) || 0) -
            ((closingOut && closingOut.TotalQty) || 0)

          stockBalances = [
            ...stockBalances,
            [
              insert[0],
              product.ProductID,
              username,
              (lastStock && lastStock.ClosingBalanceQty) || 0,
              (closingIn && closingIn.TotalQty) || 0,
              (closingOut && closingOut.TotalQty) || 0,
              totalBalance || 0,
            ],
          ]
        }
      })

      if (stockBalances.length > 0) {
        const insertStockBalanceDetails = await db.sequelize.query(
          `INSERT INTO 
            ${BranchCode}.ao_closingdetail
            (
              ClosingID,
              ItemID,
              CreatedBy,
              OpeningBalanceQty, 
              StockInQty, 
              StockOutQty, 
              ClosingBalanceQty
              ) VALUES ?`,
          {
            replacements: [stockBalances],
            type: QueryTypes.INSERT,
          }
        )
      }

      const checkClosingBalance = await db.sequelize.query(
        `SELECT sum(ClosingBalanceBags) as TotalClosingBalanceBags 
          FROM ${BranchCode}.ao_closingdetail 
          WHERE ClosingID=?
          `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      const updateClosingHeader = await db.sequelize.query(
        `UPDATE
            ${BranchCode}.ao_closingheader 
          SET StockOutQty=?, StockInQty=?, ClosingBalanceQty=?, OpeningBalanceQty=?
          WHERE id=?
          `,
        {
          replacements: [
            totalStockOutQty,
            totalStockInQty,
            ((laststocktake[0] && laststocktake[0].ClosingBalanceQty) || 0) +
              totalStockInQty -
              totalStockOutQty,
            (laststocktake[0] && laststocktake[0].ClosingBalanceQty) || 0,
            insert[0],
          ],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
            a.*
          FROM ${BranchCode}.ao_closingheader a
          WHERE a.id=? LIMIT 1
          `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    removeclosingheader: async (
      root,
      { id, BranchCode },
      { username, role, client, sessionid, iat }
    ) => {
      //db.changeDB(client)

      if (!BranchCode) return { Error: 'Please select outlet' }

      const checkuser = await db.sequelize.query(
        `SELECT 
            Department 
          FROM ${client}.ao_stafflist
          WHERE StaffID=?
          `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      if (checkuser[0].Department != 'Management')
        return { Error: 'Not Allowed' }

      const header = await db.sequelize.query(
        `SELECT 
            * 
          FROM ${BranchCode}.ao_closingheader
          WHERE id=?
          `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      const firstrecord = await db.sequelize.query(
        `SELECT 
            * 
          FROM ${BranchCode}.ao_closingheader
          ORDER BY id ASC LIMIT 1
          `,
        {
          replacements: [header[0].BranchCode],
          type: QueryTypes.SELECT,
        }
      )

      /* if (firstrecord[0].id == id)
          return { Error: 'First record is Not Allowed to Delete' }  */

      const query = await db.sequelize.query(
        `DELETE FROM
            ${BranchCode}.ao_closingheader 
          WHERE id=?
          `,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      )

      return { id }
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
