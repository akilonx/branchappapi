const { gql } = require('apollo-server-express')
const db = require('../database')
const pool = require('../_helpers/database')
const crypto = require('crypto')
const moment = require('moment-timezone')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Booking {
    id: String
    BookingNo: String
    BranchID: ID
    BranchName: String
    BranchCode: String
    BookFrom: Date
    BookTo: Date
    TotalHours: Float
    Status: String
    CreatedBy: String
    CreatedAt: Date
    UpdatedBy: String
    UpdatedAt: Date
    Token: String
    ReceivedBy: String
    ReceivedOn: Date
    Client: String
    FirstName: String
    LastName: String
    Error: String
    TotalPerson: Int
    ServiceTimes: Int
    Rating: Int
    RatingOn: Date
    FullName: String
    BookedBy: String
  }

  type BookingOutput {
    id: ID
    Error: String
  }

  type BookingSchedule {
    id: ID
    title: String
    value: String
  }

  type BookingOrder {
    id: ID
    title: String
    value: String
    MinBooking: Int
  }

  extend type Query {
    bookingschedules(BookingDate: Date): [BookingSchedule]
    bookings: [Booking]
    booking(id: ID): Booking
    bookingdetails(Token: ID): Booking
    bookingorders(id: ID): [BookingOrder]
  }

  extend type Mutation {
    receivedbooking(Token: ID, SalesPerson: String, Password: String): Booking
    insertbooking(
      CustomerID: String
      BookDate: Date
      BookTime: String
      TotalHours: Float
      TotalPerson: Int
      SalesPerson: String
      Password: String
    ): BookingOutput
    cancelbooking(id: ID, SalesPerson: String, Password: String): BookingOutput
  }
`

const resolvers = {
  Query: {
    bookingorders: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      const user = await db.sequelize.query(
        `SELECT 
          StaffID 
        FROM spa1.ao_stafflist
        WHERE id=? 
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return { Error: 'Login error, please try again.' }

      const orders = await db.sequelize.query(
        `
        SELECT 
            a.PriceID as id, SUM(a.ServiceTimes) AS TotalPurchased, a.PriceID as value, CONCAT(a.TransactionDesc, ' (Expires at ', DATE_FORMAT(a.ServiceExpiryDate, "%d-%m-%Y"), ')') AS title, b.TotalUsed, d.MinBooking
        FROM
            spa1.ao_transactionlist a
        LEFT JOIN 
          (SELECT sum(TotalPerson) as TotalUsed, PriceID FROM spa1.ao_bookinglist WHERE CreatedBy=? GROUP BY PriceID) b ON b.PriceID = a.PriceID
        LEFT JOIN
			    spa1.ao_pricelist d ON d.id = a.PriceID
        WHERE
            a.CreatedBy = ?
          AND a.TransactionType = 'BUY_SERVICE' 
          AND a.ServiceExpiryDate >= NOW()
        GROUP BY a.PriceID
      `,
        {
          replacements: [user[0].StaffID, user[0].StaffID],
          type: QueryTypes.SELECT,
        }
      )

      const output = orders.filter(
        (item) => !item.TotalUsed || item.TotalUsed < item.TotalPurchased
      )

      return output || []
    },

    bookingschedules: async (
      root,
      { BookingDate },
      { username, role, client, sessionid, iat }
    ) => {
      const freightconfig = await db.sequelize.query(
        `SELECT 
          a.*
        FROM ${client}.ao_freightconfig a
        WHERE
          a.ParmKey IN ('BOOKINGMINHOUR', 'TOTALROOMS', 'BOOKINGINTERVAL', 'BOOKINGOPENINGHOURS', 'BOOKINGCLOSINGHOURS' ) 
      `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      if (!freightconfig[0]) return { Error: 'No configuration' }

      const dateObj = moment.tz(BookingDate, 'Asia/Kuala_Lumpur')
      /* const month = dateObj.getUTCMonth() + 1 //months from 1-12
      const day = dateObj.getUTCDate()
      const year = dateObj.getUTCFullYear() */

      //const searchDate = year + '-' + month + '-' + day
      const searchDate = dateObj.format('YYYY-MM-DD')

      /* console.log(searchDate) */

      const bookingschedules = await db.sequelize.query(
        `SELECT 
          a.*
        FROM spa1.ao_bookinglist a
        WHERE
          a.BranchCode = ? AND DATE(a.BookFrom) = ?
      `,
        {
          replacements: [client, searchDate],
          type: QueryTypes.SELECT,
        }
      )

      //console.log(bookingschedules)

      const config = Object.fromEntries(
        freightconfig.map((a) => [a.ParmKey, a.ParamValue])
      )
      //console.log(config)

      const intervalConfig =
        parseInt(config.BOOKINGINTERVAL) + parseInt(config.BOOKINGMINHOUR) - 1
      let output = []
      //let today = moment.tz(`${searchDate} 08:00`, 'Asia/Kuala_Lumpur')

      const times = [
        {
          value: `${searchDate} 08:00`,
        },
        {
          value: `${searchDate} 08:30`,
        },
        {
          value: `${searchDate} 09:00`,
        },
        {
          value: `${searchDate} 09:30`,
        },

        {
          value: `${searchDate} 10:00`,
        },
        {
          value: `${searchDate} 10:30`,
        },
        {
          value: `${searchDate} 11:00`,
        },
        {
          value: `${searchDate} 11:30`,
        },

        {
          value: `${searchDate} 12:00`,
        },
        {
          value: `${searchDate} 12:30`,
        },
        {
          value: `${searchDate} 13:00`,
        },
        {
          value: `${searchDate} 13:30`,
        },

        {
          value: `${searchDate} 14:00`,
        },
        {
          value: `${searchDate} 14:30`,
        },
        {
          value: `${searchDate} 15:00`,
        },
        {
          value: `${searchDate} 15:30`,
        },

        {
          value: `${searchDate} 16:00`,
        },
        {
          value: `${searchDate} 16:30`,
        },
        {
          value: `${searchDate} 17:00`,
        },
        {
          value: `${searchDate} 17:30`,
        },

        {
          value: `${searchDate} 18:00`,
        },
        {
          value: `${searchDate} 18:30`,
        },
        {
          value: `${searchDate} 19:00`,
        },
        {
          value: `${searchDate} 19:30`,
        },

        {
          value: `${searchDate} 20:00`,
        },
        {
          value: `${searchDate} 20:30`,
        },
        {
          value: `${searchDate} 21:00`,
        },
        {
          value: `${searchDate} 21:30`,
        },

        {
          value: `${searchDate} 22:00`,
        },
      ]

      let diff, loopTime

      const openingTime = moment.tz(
        `${searchDate} ${config.BOOKINGOPENINGHOURS}`,
        'Asia/Kuala_Lumpur'
      )

      const closingTime = moment.tz(
        `${searchDate} ${config.BOOKINGCLOSINGHOURS}`,
        'Asia/Kuala_Lumpur'
      )

      const currentTime = moment.tz('Asia/Kuala_Lumpur')

      const getDiff = (a, b) => {
        const output = b > a ? b - a : a - b

        return Math.floor(output / (1000 * 60))
      }

      times.map((a, i) => {
        //loopTime = moment(a.value).tz('Asia/Kuala_Lumpur')
        loopTime = moment.tz(a.value, 'Asia/Kuala_Lumpur')
        /* console.log(
          loopTime.format(),
          'Opening',
          openingTime.format(),
          'closing',
          closingTime.format()
        ) */

        //BookFrom: moment(h.BookFrom).tz('Asia/Kuala_Lumpur'),
        if (currentTime < loopTime) {
          if (loopTime >= openingTime && loopTime <= closingTime) {
            //new booking object with diff
            let diffBookings = bookingschedules.map((h) => {
              console.log(
                moment.tz(h.BookFrom, 'Asia/Kuala_Lumpur').format(),
                moment.tz(a.value, 'Asia/Kuala_Lumpur').format()
              )

              return {
                BookFrom: h.BookFrom,
                Loop: loopTime.format(),
                Diff: getDiff(
                  moment.tz(h.BookFrom, 'Asia/Kuala_Lumpur'),
                  loopTime
                ),
              }
            })

            //get overlap bookings
            let totalOverlapBooking = diffBookings.filter(
              (a) => a.Diff < intervalConfig
            )

            //check if exceed total rooms
            if (totalOverlapBooking.length < parseInt(config.TOTALROOMS))
              output = [
                ...output,
                {
                  id: i,
                  title: loopTime.format('LT'),
                  value: loopTime.format('HH:mm'),
                },
              ]
          }
        }
      })

      return output
    },
    bookings: async (root, {}, { username, role, client, sessionid, iat }) =>
      //CONCAT(c.FirstName,' ', c.LastName, '\n', c.Phone) as FullName,
      db.sequelize.query(
        `SELECT 
        distinct
          a.*,
          DATE_FORMAT(a.BookFrom, '%d-%m-%Y %l:%i %p') as BookFrom,
          CONCAT(c.FirstName,' ', c.LastName, '\n', c.Phone) as FullName2,
          CONCAT_WS(' ', c.FirstName, c.LastName, c.Phone) as FullName,
          c.FirstName,
          c.LastName,
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM spa1.ao_bookinglist a
        LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
        LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
       WHERE a.BranchCode = ?
          GROUP BY a.id
        ORDER BY a.id DESC
      `,
        {
          replacements: [client],
          type: QueryTypes.SELECT,
        }
      ),

    booking: async (
      root,
      { id },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
          a.*, 
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM ${client}.ao_bookinglist a
        WHERE
          a.id = ? AND a.CreatedBy = ?
      `,
        {
          replacements: [id, username],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
    bookingdetails: async (
      root,
      { Token },
      { username, role, client, sessionid, iat }
    ) => {
      const query = await db.sequelize.query(
        `SELECT 
          a.*,
          DATE_FORMAT(a.BookFrom, '%d-%m-%Y %l:%i %p') as BookFrom,
          b.StaffID as Username,
          CONCAT(c.FirstName,' ', c.LastName, '\n', c.Phone) as FullName2,
          CONCAT_WS(' ', c.FirstName, c.LastName, c.Phone) as FullName,
          c.FirstName,
          c.LastName,
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM spa1.ao_bookinglist a
        LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
        LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
        WHERE
          a.Token = ?
      `,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      if (!query[0]) return { Error: 'Not Found.' }

      const usedservicetimes = await db.sequelize.query(
        `SELECT sum(TotalPerson) as TotalUsed FROM spa1.ao_bookinglist WHERE CreatedBy=?`,
        {
          replacements: [query[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      const transaction = await db.sequelize.query(
        `SELECT sum(ServiceTimes) as ServiceTimes FROM spa1.ao_transactionlist where TransactionType="BUY_SERVICE" and Refund is null and CreatedBy=? and Active=1;`,
        {
          replacements: [query[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      if (!transaction[0]) return { Error: 'No Service Times' }

      const remainingTimes =
        transaction[0].ServiceTimes - usedservicetimes[0].TotalUsed

      query[0].ServiceTimes = remainingTimes

      return query[0]
    },
  },
  Mutation: {
    cancelbooking: async (
      root,
      { id, SalesPerson, Password },
      { username, role, client, sessionid, iat }
    ) => {
      /* const loggedIn = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_puchcarddetail WHERE Username=? AND DATE(PunchDate)=? AND PunchType='Checkin'`,
        {
          replacements: [
            username,
            moment(new Date()).tz('Asia/Kuala_Lumpur').format().split('T')[0],
          ],
          type: QueryTypes.SELECT,
        }
      )

      if (!loggedIn[0]) return { Error: 'You are not Punched In' } */

      //db.changeDB(client)

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM ${client}.ao_stafflist
        WHERE StaffID=? AND Password2=?
        `,
        {
          replacements: [SalesPerson, Password],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return { Error: 'Login error, please try again.' }

      const bookings = await db.sequelize.query(
        `SELECT 
          *
        FROM
          spa1.ao_bookinglist 
        WHERE CreatedAt > DATE_SUB(NOW(), INTERVAL 5 HOUR) and id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      if (!bookings[0])
        return {
          Error:
            'Not allowed to delete booking that had passed more than 5 hours.',
        }

      const checkstatus = await db.sequelize.query(
        `SELECT 
            *
          FROM
            spa1.ao_bookinglist 
          WHERE id=?
          `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      if (checkstatus[0] && checkstatus[0].Status == 'Completed')
        return {
          Error: 'Cannot remove Completed booking',
        }

      const query = await db.sequelize.query(
        `DELETE FROM
          spa1.ao_bookinglist 
        WHERE id=? `,
        {
          replacements: [id],
          type: QueryTypes.DELETE,
        }
      )

      return { id }
    },
    insertbooking: async (
      root,
      {
        CustomerID,
        BookDate,
        BookTime,
        TotalHours,
        TotalPerson,
        SalesPerson,
        Password,
      },
      { username, role, client, sessionid, iat }
    ) => {
      /*  const loggedIn = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_puchcarddetail WHERE Username=? AND DATE(PunchDate)=? AND PunchType='Checkin'`,
        {
          replacements: [
            username,
            moment(new Date()).tz('Asia/Kuala_Lumpur').format().split('T')[0],
          ],
          type: QueryTypes.SELECT,
        }
      )

      if (!loggedIn[0]) return { Error: 'You are not Punched In' } */

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM ${client}.ao_stafflist
        WHERE StaffID=? AND Password2=?
        `,
        {
          replacements: [SalesPerson, Password],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return { Error: 'Login error, please try again.' }

      const customer = await db.sequelize.query(
        `SELECT 
          a.*,
          b.Branch,
          b.FirstName,
          b.LastName
        FROM spa1.ao_stafflist a
        LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.id
        WHERE a.id=?
        `,
        {
          replacements: [CustomerID],
          type: QueryTypes.SELECT,
        }
      )

      const usedservicetimes = await db.sequelize.query(
        `SELECT sum(TotalPerson) as TotalUsed FROM spa1.ao_bookinglist WHERE CreatedBy=?`,
        {
          replacements: [customer[0].StaffID],
          type: QueryTypes.SELECT,
        }
      )

      const transaction = await db.sequelize.query(
        `SELECT sum(ServiceTimes) as ServiceTimes FROM spa1.ao_transactionlist where TransactionType="BUY_SERVICE" and CreatedBy=? and Active=1;`,
        {
          replacements: [customer[0].StaffID],
          type: QueryTypes.SELECT,
        }
      )

      const remainingTimes =
        transaction[0].ServiceTimes - usedservicetimes[0].TotalUsed

      if (!transaction[0] || remainingTimes < 1)
        return {
          Error: `Not enough Booking Pass for user ${customer[0].FirstName} ${customer[0].LastName}`,
        }

      const freightconfig = await db.sequelize.query(
        `SELECT 
          a.*
        FROM ${client}.ao_freightconfig a
        WHERE
          a.ParmKey IN ('BOOKINGMINHOUR', 'TOTALROOMS', 'BOOKINGINTERVAL', 'BOOKINGOPENINGHOURS', 'BOOKINGCLOSINGHOURS' ) 
      `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )

      if (!freightconfig[0]) return { Error: 'No configuration' }

      const config = Object.fromEntries(
        freightconfig.map((a) => [a.ParmKey, a.ParamValue])
      )

      const dateObj = moment(BookDate)
        .tz('Asia/Kuala_Lumpur')
        .utc()
        .format('YYYY-MM-DD')
      /* const month = dateObj.month() + 1
      const day = dateObj.date()
      const year = dateObj.year() */

      const dateMalaysia = moment(BookDate)
        .tz('Asia/Kuala_Lumpur')
        .format('YYYY-MM-DD')

      db.changeDB('spa1')

      const bookingNoIncrease = await db.freightrunno.update(
        {
          LastNo: literal('LastNo + 1'),
        },
        {
          where: {
            Prefix: 'BOOKING',
          },
        }
      )

      const bookingNo = await db.freightrunno.findOne({
        where: {
          Prefix: 'BOOKING',
        },
      })

      /* const prefix = await db.freightconfig.findOne({
        where: {
          ParmKey: 'ODRPFX',
        },
      }) */

      const BookingNo = `${db.pad(bookingNo.LastNo, 5)}`

      const token = crypto.randomBytes(64).toString('hex')

      const TotalHour = Math.floor(parseInt(config.BOOKINGMINHOUR) / 60)

      const insert = await db.sequelize.query(
        `INSERT
          spa1.ao_bookinglist
        SET
          BookingNo = ?,
          BranchCode = ?, 
          BookFrom = ?, 
          TotalHours = ?, 
          Status = 'New', 
          CreatedBy = ?,
          TotalPerson = ?,
          CreatedAt = NOW(),
          BookedBy = ?,
          Token=?`,
        {
          replacements: [
            BookingNo,
            client,
            dateMalaysia + ' ' + BookTime + ':00',
            TotalHour,
            //username,
            customer[0].StaffID,
            TotalPerson,
            SalesPerson,
            token,
          ],
          type: QueryTypes.INSERT,
        }
      )

      const update = await db.sequelize.query(
        `UPDATE
            spa1.ao_bookinglist
          SET
            BookTo = DATE_ADD(BookFrom, INTERVAL ? HOUR) 
          WHERE
            id = ?`,
        {
          replacements: [TotalHour, insert[0]],
          type: QueryTypes.UPDATE,
        }
      )

      /* 
      const booking = await db.sequelize.query(
        `SELECT 
          * 
        FROM spa1.ao_bookinglist
        WHERE id=?
        `,
        {
          replacements: [insert[0]],
          type: QueryTypes.SELECT,
        }
      )

      console.log(booking)

      const checkbooking = await db.sequelize.query(
        `SELECT *
          FROM spa1.ao_bookinglist
        WHERE
          ((BookTo > ? AND BookTo <= ?) OR BookFrom = ?)
          AND BranchCode=?
          AND id!=?
        `,
        {
          replacements: [
            booking[0].BookFrom.toISOString().slice(0, 19).replace('T', ' '),
            booking[0].BookTo.toISOString().slice(0, 19).replace('T', ' '),
            booking[0].BookFrom.toISOString().slice(0, 19).replace('T', ' '),
            client,
            insert[0],
          ],
          type: QueryTypes.SELECT,
        }
      )

      const config = await db.sequelize.query(
        `SELECT 
          * 
        FROM ${client}.ao_freightconfig
        WHERE ParmKey='TOTALROOMS'
        `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ) */

      //update spa1.ao_bookinglist set BookTo = DATE_ADD(BookFrom, INTERVAL '1 30' HOUR_MINUTE)

      /* if (
        checkbooking &&
        checkbooking.length >= parseInt(config[0].ParamValue)
      ) {
        const remove = await db.sequelize.query(
          `DELETE FROM
            spa1.ao_bookinglist 
          WHERE id=? `,
          {
            replacements: [insert[0]],
            type: QueryTypes.DELETE,
          }
        )

        return { Error: 'Time Slot is taken. Please try another time slot.' }
      } */

      return { id: insert[0] }
    },
    receivedbooking: async (
      root,
      { Token, SalesPerson, Password },
      { username, role, client, sessionid, iat }
    ) => {
      //id, TransactionID, TransactionRef, TransactionDate, TransactionAmount, PaymentTo, Active, DebtorCode, TransactionType, TransactionDesc, CreatedOn, CreatedBy, PaymentTermID, ServiceTimes
      // SELECT * FROM ${client}.ao_puchcarddetail WHERE username=? AND PunchDate=CURDATE() AND PunchType='Checkin'
      /* const loggedIn = await db.sequelize.query(
        `SELECT * FROM ${client}.ao_puchcarddetail WHERE Username=? AND DATE(PunchDate)=? AND PunchType='Checkin'`,
        {
          replacements: [
            username,
            moment(new Date()).tz('Asia/Kuala_Lumpur').format().split('T')[0],
          ],
          type: QueryTypes.SELECT,
        }
      )

      if (!loggedIn[0]) return { Error: 'You are not Punched In' } */

      const user = await db.sequelize.query(
        `SELECT 
          id 
        FROM ${client}.ao_stafflist
        WHERE StaffID=? AND Password2=?
        `,
        {
          replacements: [SalesPerson, Password],
          type: QueryTypes.SELECT,
        }
      )

      if (!user[0]) return { Error: 'Login error, please try again.' }

      let query = await db.sequelize.query(
        `SELECT 
          a.*, 
          CONCAT(c.FirstName,' ', c.LastName, '\n', c.Phone) as FullName2,
          CONCAT_WS(' ', c.FirstName, c.LastName, c.Phone) as FullName,
          c.FirstName,
          c.LastName,
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM spa1.ao_bookinglist a
        LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
        LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
        WHERE
          a.Token = ?
      `,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      if (!query[0]) return { Error: 'Not Found.' }

      const bookings = await db.sequelize.query(
        `SELECT 
          *
        FROM
          spa1.ao_bookinglist 
        WHERE CreatedAt > DATE_SUB(NOW(), INTERVAL 5 HOUR) and Token=?
        `,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      if (!bookings[0])
        return {
          Error: 'Not allowed to check in booking more than 5 hours.',
        }

      const bookings2 = await db.sequelize.query(
        `SELECT 
            *
          FROM
            spa1.ao_bookinglist 
          WHERE CreatedAt < DATE_SUB(NOW(), INTERVAL 5 HOUR) and Token=?
          `,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      if (!bookings2[0])
        return {
          Error: 'Not allowed to check in booking earlier than 5 hours.',
        }

      const usedservicetimes = await db.sequelize.query(
        `SELECT sum(TotalPerson) as TotalUsed FROM spa1.ao_bookinglist WHERE CreatedBy=?`,
        {
          replacements: [query[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      const transaction = await db.sequelize.query(
        `SELECT sum(ServiceTimes) as ServiceTimes FROM spa1.ao_transactionlist where TransactionType="BUY_SERVICE" and CreatedBy=? and Active=1;`,
        {
          replacements: [query[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      if (!transaction[0])
        return { Error: 'This user do not have enough Booking Pass' }

      const remainingTimes =
        transaction[0].ServiceTimes - usedservicetimes[0].TotalUsed

      query[0].ServiceTimes = remainingTimes

      const save = await db.sequelize.query(
        `UPDATE
            spa1.ao_bookinglist
          SET
            Status='Completed',
            Client=?,
            ReceivedBy=?,
            ReceivedOn=NOW()
          WHERE Token=?`,
        {
          replacements: [client, SalesPerson, Token],
          type: QueryTypes.UPDATE,
        }
      )
      //replacements: [client, SalesPerson || username, Token],

      query = await db.sequelize.query(
        `SELECT 
          a.*, 
          CONCAT(c.FirstName,' ', c.LastName, '\n', c.Phone) as FullName2,
          CONCAT_WS(' ', c.FirstName, c.LastName, c.Phone) as FullName,
          c.FirstName,
          c.LastName,
          (select BranchName from systemcontrol.webpara where BranchCode = a.BranchCode) as BranchName
        FROM spa1.ao_bookinglist a
        LEFT JOIN spa1.ao_stafflist b ON b.StaffID = a.CreatedBy
        LEFT JOIN spa1.ao_buyerlist c ON c.UserID = b.id
        WHERE
          a.Token = ?
      `,
        {
          replacements: [Token],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
