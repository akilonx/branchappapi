const { gql } = require('apollo-server-express')
const db = require('../database')
const { Op } = require('sequelize')

const typeDefs = gql`
  type Staff {
    id: ID!
    StaffID: String
    StaffName: String
    Department: String
  }

  extend type Query {
    staffs: [Staff]
  }
`

const resolvers = {
  Query: {
    staffs: () =>
      db.staff.findAll({
        order: [['StaffName', 'ASC']]
      })
  }
}

module.exports = {
  typeDefs,
  resolvers
}
