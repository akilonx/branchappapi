const { gql } = require('apollo-server-express')
const { QueryTypes, fn, col, Op, literal } = require('sequelize')
const db = require('../database')

const pad = (n, width, z) => {
  z = z || '0'
  n = n + ''
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n
}

const typeDefs = gql`
  type Customer {
    id: ID!
    CustomerCode: String
    CustomerName: String
    CompanyCode: String
    PersonInCharge: String
    PersonInCharge2: String
    CustomerType: String
    CountryCode: String
    PaymentTermCode: String
    StaffID: String
    Address1: String
    Address2: String
    Address3: String
    Address4: String
    TelNo1: String
    TelNo2: String
    FaxNo1: String
    FaxNo2: String
    Email1: String
    Email2: String
    Remark: String
    UpdatedBy: String
    LastModified: Date
    CreatedBy: String
    CreateOn: Date
    Status: String
    ActiveQuotation: String
    CoLoaderCode: String
    CoLoaderName: String
    CustomerTypeName: String
    PaymentTermName: String
    CountryName: String
  }

  type CustomerPagination {
    total: Int
    data: [Customer]
  }

  extend type Query {
    customer(id: ID!): Customer
    customers(CustomerType: String): [Customer]
    agents: [Customer]
    customerspagination(limit: Int, offset: Int): CustomerPagination
    customersearchlimit: [Customer]
    searchcustomers(Search: String, CustomerType: String): [Customer]
  }

  extend type Mutation {
    createcustomer(
      CustomerName: String
      CompanyCode: String
      PersonInCharge: String
      PersonInCharge2: String
      CustomerType: String
      CountryCode: String
      PaymentTermCode: String
      CoLoaderCode: String
      StaffID: String
      Address1: String
      Address2: String
      Address3: String
      Address4: String
      TelNo1: String
      TelNo2: String
      FaxNo1: String
      FaxNo2: String
      Email1: String
      Email2: String
      Remark: String
    ): Customer
    updatecustomer(
      id: ID!
      CustomerCode: String
      CustomerName: String
      CompanyCode: String
      PersonInCharge: String
      PersonInCharge2: String
      CustomerType: String
      CountryCode: String
      PaymentTermCode: String
      CoLoaderCode: String
      StaffID: String
      Address1: String
      Address2: String
      Address3: String
      Address4: String
      TelNo1: String
      TelNo2: String
      FaxNo1: String
      FaxNo2: String
      Email1: String
      Email2: String
      Remark: String
      Status: String
    ): Customer
    deletecustomer(id: ID!): Int
    customersearch(search: String): [Customer]
  }
`

const resolvers = {
  Query: {
    customer: async (root, { id }) => {
      db.customer.update(
        {
          Ordering: literal('Ordering + 1')
        },
        {
          where: {
            id
          }
        }
      )

      const query = await db.sequelize.query(
        `SELECT 
          a.*, 
          (SELECT CountryName FROM ao_countrylist WHERE CountryCode = a.CountryCode) AS CountryName, 
          (SELECT CustomerName FROM ao_customerlist WHERE customercode = a.ColoaderCode) AS CoLoaderName, 
          (SELECT CompanyTypeName FROM ao_companytypelist WHERE CompanyTypeCode = a.CustomerType) AS CustomerTypeName, 
          (SELECT PaymentTermDesc FROM ao_paymenttermlist WHERE PaymentTermCode = a.PaymentTermCode) AS PaymentTermName
        from ao_customerlist a 
        WHERE a.id=?`,
        {
          replacements: [id],
          type: QueryTypes.SELECT
        }
      )

      return query[0]
    },
    agents: () =>
      db.sequelize.query(
        `SELECT * FROM ao_customerlist 
      WHERE (CustomerType='FGA_ADA' OR CustomerType='FGA_AGN') ORDER BY Ordering DESC, CustomerName ASC`,
        {
          replacements: [],
          type: QueryTypes.SELECT
        }
      ),
    /* db.customer.findAll({FGA_AGN
        where: { CustomerType: 'FGA_ADA' },
        order: [
          ['Ordering', 'DESC'],
          ['CustomerName', 'ASC']
        ]
      }) */

    searchcustomers: async (root, { Search, CustomerType }, context) => {
      if (!Search) return []
      if (Search.length < 2) return []
      if (context.role == 'Booking Agent') {
        const user = await db.sequelize.query(
          `SELECT * from ao_stafflist WHERE staffid=? limit 1`,
          {
            replacements: [context.username],
            type: QueryTypes.SELECT
          }
        )

        return db.sequelize.query(
          `SELECT * from ao_customerlist WHERE CoLoaderCode IN (?) and CustomerCode NOT IN (?)`,
          {
            replacements: [
              (user[0] && user[0].AgentCode.split(',')) || [],
              (user[0] && user[0].AgentCode.split(',')) || []
            ],
            type: QueryTypes.SELECT
          }
        )
      } else {
        if (CustomerType) {
          if (CustomerType == 'FGA_AIR') {
            return db.sequelize.query(
              `SELECT * from ao_customerlist WHERE (CustomerName LIKE '%${Search}%' OR  CustomerCode LIKE '%${Search}%') AND (CustomerType=? OR CustomerType='FGA_ESA') ORDER BY Ordering DESC, CustomerName ASC`,
              {
                replacements: [CustomerType],
                type: QueryTypes.SELECT
              }
            )
          }

          return db.sequelize.query(
            `SELECT * from ao_customerlist WHERE CustomerType=? AND (CustomerName LIKE '%${Search}%' OR  CustomerCode LIKE '%${Search}%') ORDER BY Ordering DESC, CustomerName ASC`,
            {
              replacements: [CustomerType],
              type: QueryTypes.SELECT
            }
          )
        }

        return db.sequelize.query(
          `SELECT * from ao_customerlist WHERE (CustomerName LIKE '%${Search}%' OR  CustomerCode LIKE '%${Search}%') ORDER BY Ordering DESC, CustomerName ASC`,
          {
            replacements: [],
            type: QueryTypes.SELECT
          }
        )
      }
    },

    customers: async (root, { CustomerType }, context) => {
      if (context.role == 'Booking Agent') {
        const user = await db.sequelize.query(
          `SELECT * from ao_stafflist WHERE staffid=? limit 1`,
          {
            replacements: [context.username],
            type: QueryTypes.SELECT
          }
        )

        if (CustomerType) {
          if (CustomerType == 'CREDITORS') {
            return db.sequelize.query(
              `SELECT * from ao_customerlist WHERE CustomerCode IN (?) ORDER BY Ordering DESC, CustomerName ASC`,
              {
                replacements: [(user[0] && user[0].AgentCode.split(',')) || []],
                type: QueryTypes.SELECT
              }
            )
          }
        }

        return db.sequelize.query(
          `SELECT id, CustomerCode, CustomerName, CoLoaderCode from ao_customerlist WHERE CoLoaderCode IN (?) and CustomerCode NOT IN (?)`,
          {
            replacements: [
              (user[0] && user[0].AgentCode.split(',')) || [],
              (user[0] && user[0].AgentCode.split(',')) || []
            ],
            type: QueryTypes.SELECT
          }
        )
      } else {
        if (CustomerType) {
          if (CustomerType == 'FGA_AIR') {
            return db.sequelize.query(
              `SELECT id, CustomerCode, CustomerName, CoLoaderCode from ao_customerlist WHERE (CustomerType=? OR CustomerType='FGA_ESA') ORDER BY  CustomerName ASC`,
              {
                replacements: [CustomerType],
                type: QueryTypes.SELECT
              }
            )
          }

          if (CustomerType == 'CREDITORS') {
            return db.sequelize.query(
              `SELECT id, CustomerCode, CustomerName, CoLoaderCode from ao_customerlist ORDER BY CustomerName ASC`,
              {
                replacements: [],
                type: QueryTypes.SELECT
              }
            )
          }

          return db.sequelize.query(
            `SELECT id, CustomerCode, CustomerName, CoLoaderCode from ao_customerlist WHERE CustomerType=? ORDER BY  CustomerName ASC`,
            {
              replacements: [CustomerType],
              type: QueryTypes.SELECT
            }
          )
        }

        return db.sequelize.query(
          `SELECT id, CustomerCode, CustomerName, CoLoaderCode from ao_customerlist ORDER BY CustomerName ASC`,
          {
            replacements: [],
            type: QueryTypes.SELECT
          }
        )
      }
    },
    /* db.customer.findAll({
        order: [
          ['Ordering', 'DESC'],
          ['CustomerName', 'ASC']
        ]
      }) */ customerspagination: (
      root,
      { limit, offset }
    ) =>
      db.customer
        .findAll({
          order: [
            ['Ordering', 'DESC'],
            ['CustomerName', 'ASC']
          ]
        })
        .then(d => {
          const data =
            offset === undefined || offset === 0 || offset === limit
              ? d.slice(0, limit)
              : d.slice(limit * offset, limit * offset + limit)
          const total = d.length
          //return data
          return {
            data,
            total
          }
        }),
    customersearchlimit: () =>
      db.customer.findAll({
        order: [['Ordering', 'DESC']],
        limit: 100
      })
  },
  Mutation: {
    createcustomer: async (
      root,
      {
        CustomerName,
        CompanyCode,
        PersonInCharge,
        PersonInCharge2,
        CustomerType,
        CountryCode,
        PaymentTermCode,
        CoLoaderCode,
        StaffID,
        Address1,
        Address2,
        Address3,
        Address4,
        TelNo1,
        TelNo2,
        FaxNo1,
        FaxNo2,
        Email1,
        Email2,
        Remark
      },
      { username, role, client, sessionid, iat }
    ) => {
      /* var custNo = await pool.query(`SELECT(select LastNo from ` + user.client + `.ao_freightrunno WHERE Prefix = 'CUS') AS LastNo, (select ParamValue FROM ` + user.client + `.ao_freightconfig WHERE ParmKey = 'CUSPFX') AS Prefix`);
        var custNoIncrease = await pool.query(`UPDATE ` + user.client + `.ao_freightrunno SET LastNo = ? WHERE Prefix = 'CUS'`, [custNo[0].LastNo + 1]);
        a.CustomerCode = custNo[0].Prefix + '-' + pool.pad(custNo[0].LastNo + 1, 5);
        a.Status = 'A'; */

      /* const prefix = await db.freightconfig.findOne({
        where: { ParmKey: 'CUSPFX' }
      }) */

      db.customer.update(
        {
          Ordering: literal('Ordering + 1')
        },
        {
          where: {
            CustomerCode: CoLoaderCode
          }
        }
      )

      db.country.update(
        {
          Ordering: literal('Ordering + 1')
        },
        {
          where: {
            CountryCode: CountryCode
          }
        }
      )

      db.customertype.update(
        {
          Ordering: literal('Ordering + 1')
        },
        {
          where: {
            CompanyTypeCode: CustomerType
          }
        }
      )

      db.paymentterm.update(
        {
          Ordering: literal('Ordering + 1')
        },
        {
          where: {
            PaymentTermCode: PaymentTermCode
          }
        }
      )

      const custNoIncrease = await db.freightrunno.update(
        {
          LastNo: literal('LastNo + 1')
        },
        {
          where: {
            Prefix: 'CUS'
          }
        }
      )

      const custNo = await db.freightrunno.findOne({
        where: {
          Prefix: 'CUS'
        }
      })

      const CustomerCode = `TMP-${pad(custNo.LastNo, 5)}`
      const Status = 'D'

      return db.customer.create({
        CustomerCode,
        CustomerName,
        CompanyCode,
        PersonInCharge,
        PersonInCharge2,
        CustomerType,
        CountryCode,
        PaymentTermCode,
        CoLoaderCode,
        StaffID,
        Address1,
        Address2,
        Address3,
        Address4,
        TelNo1,
        TelNo2,
        FaxNo1,
        FaxNo2,
        Email1,
        Email2,
        Remark,
        Status,
        CreatedBy: username,
        CreateOn: fn('NOW')
      })
    },
    updatecustomer: async (
      root,
      {
        id,
        CustomerCode,
        CustomerName,
        CompanyCode,
        PersonInCharge,
        PersonInCharge2,
        CustomerType,
        CountryCode,
        CoLoaderCode,
        PaymentTermCode,
        StaffID,
        Address1,
        Address2,
        Address3,
        Address4,
        TelNo1,
        TelNo2,
        FaxNo1,
        FaxNo2,
        Email1,
        Email2,
        Status,
        Remark
      },
      { username, role, client, sessionid, iat }
    ) => {
      let checkSame = true

      if (CustomerCode) {
        const checkDuplicate = await db.sequelize.query(
          `SELECT * FROM ao_customerlist WHERE CustomerCode=? AND id!=?`,
          {
            replacements: [CustomerCode, id],
            type: QueryTypes.SELECT
          }
        )
        checkSame = await db.sequelize.query(
          `SELECT * FROM ao_customerlist WHERE CustomerCode=? AND id=?`,
          {
            replacements: [CustomerCode, id],
            type: QueryTypes.SELECT
          }
        )

        old = await db.sequelize.query(
          `SELECT * FROM ao_customerlist WHERE id=?`,
          {
            replacements: [id],
            type: QueryTypes.SELECT
          }
        )

        if (checkDuplicate[0]) return {}
      }

      return db.customer
        .update(
          {
            CustomerCode,
            CustomerName,
            CompanyCode,
            PersonInCharge,
            PersonInCharge2,
            CustomerType,
            CountryCode,
            CoLoaderCode,
            PaymentTermCode,
            StaffID,
            Address1,
            Address2,
            Address3,
            Address4,
            TelNo1,
            TelNo2,
            FaxNo1,
            FaxNo2,
            Email1,
            Email2,
            Remark,
            Status,
            UpdatedBy: username,
            LastModified: fn('NOW')
          },
          {
            where: {
              id
            }
          }
        )
        .then(async () => {
          if (!checkSame[0]) {
            const updateBilling1 = await db.sequelize.query(
              `UPDATE
                  ao_billingheader
                SET
                shipperCode = ? WHERE shipperCode=?`,
              {
                replacements: [CustomerCode, old[0].CustomerCode],
                type: QueryTypes.UPDATE
              }
            )

            const updateBilling2 = await db.sequelize.query(
              `UPDATE
                  ao_billingheader
                SET 
                ConsigneeCode = ? WHERE ConsigneeCode=?`,
              {
                replacements: [CustomerCode, old[0].CustomerCode],
                type: QueryTypes.UPDATE
              }
            )

            const updateBilling3 = await db.sequelize.query(
              `UPDATE
                  ao_billingheader
                SET
                BillToCode = ? WHERE BillToCode=?`,
              {
                replacements: [CustomerCode, old[0].CustomerCode],
                type: QueryTypes.UPDATE
              }
            )

            const updateBilling4 = await db.sequelize.query(
              `UPDATE
                  ao_billingheader
                SET
                InvoiceToCode = ? WHERE InvoiceToCode=?`,
              {
                replacements: [CustomerCode, old[0].CustomerCode],
                type: QueryTypes.UPDATE
              }
            )

            const updateAwb = await db.sequelize.query(
              `UPDATE
                  ao_awb
                SET
                AgentID = ? WHERE AgentID=?`,
              {
                replacements: [CustomerCode, old[0].CustomerCode],
                type: QueryTypes.UPDATE
              }
            )

            const updateOrder = await db.sequelize.query(
              `UPDATE
                  ao_orderheader
                SET
                CustomerCode = ? WHERE CustomerCode=?`,
              {
                replacements: [CustomerCode, old[0].CustomerCode],
                type: QueryTypes.UPDATE
              }
            )

            const updateQuotation = await db.sequelize.query(
              `UPDATE
                  ao_quotationheader
                SET
                CustomerCode = ? WHERE CustomerCode=?`,
              {
                replacements: [CustomerCode, old[0].CustomerCode],
                type: QueryTypes.UPDATE
              }
            )

            const updateQuotationDetails = await db.sequelize.query(
              `UPDATE
                  ao_quotationdetail
                SET
                CustomerCode = ? WHERE CustomerCode=?`,
              {
                replacements: [CustomerCode, old[0].CustomerCode],
                type: QueryTypes.UPDATE
              }
            )

            const updateConsignment = await db.sequelize.query(
              `UPDATE
                  ao_consignmentnote
                SET
                ShipperCode = ? WHERE ShipperCode=?`,
              {
                replacements: [CustomerCode, old[0].CustomerCode],
                type: QueryTypes.UPDATE
              }
            )
          }

          const query = await db.sequelize.query(
            `SELECT 
              a.*, 
              (SELECT CustomerName FROM ao_customerlist WHERE customercode = a.ColoaderCode) AS CoLoaderName, 
              (SELECT CompanyTypeName FROM ao_companytypelist WHERE CompanyTypeCode = a.CustomerType) AS CustomerTypeName, 
              (SELECT PaymentTermDesc FROM ao_paymenttermlist WHERE PaymentTermCode = a.PaymentTermCode) AS PaymentTermName
            from ao_customerlist a 
            WHERE a.id=?`,
            {
              replacements: [id],
              type: QueryTypes.SELECT
            }
          )

          return query[0]
        })
    },
    deletecustomer: (root, { id }) =>
      db.customer.destroy({
        where: {
          id
        }
      }),

    customersearch: (root, { search }) =>
      db.customer.findAll({
        where: {
          CustomerName: {
            [Op.like]: `%${search}%`
          }
        }
      })
  }
}

module.exports = {
  typeDefs,
  resolvers
}

/* id
CustomerCode: String
CustomerName: String
CompanyCode: String
PersonInCharge: String
PersonInCharge2: String
CustomerType: String
CountryCode: String
PaymentTermCode: String
StaffID: String
Address1: String
Address2: String
Address3: String
Address4: String
TelNo1: String
TelNo2: String
FaxNo1: String
FaxNo2: String
Email1: String
Email2: String
Remark: String
UpdatedBy: String
LastModified: Date
CreatedBy: String
CreateOn: Date
Status: String
ActiveQuotation: String
CoLoaderCode : String
*/
