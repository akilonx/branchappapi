const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Branch {
    BranchName: ID
    BranchCode: String
  }

  extend type Query {
    branches: [Branch]
    groupbranches: [Branch]
  }
`

const resolvers = {
  Query: {
    branches: (root, {}, { username, role, client, sessionid, iat }) =>
      db.sequelize.query(
        `SELECT 
            a.*
          FROM systemcontrol.webpara a 
          WHERE a.Type='spa' AND a.BranchCode is not null
          ORDER BY a.BranchName desc`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
    groupbranches: async (
      root,
      {},
      { username, role, client, sessionid, iat }
    ) => {
      const webpara = await db.sequelize.query(
        `SELECT 
          a.*
        FROM systemcontrol.webpara a 
        WHERE a.BranchCode=? 
        Limit 1`,
        {
          replacements: [client],
          type: QueryTypes.SELECT,
        }
      )

      if (!webpara[0]) return []

      if (!webpara[0].GroupID) return webpara

      const webparagroup = await db.sequelize.query(
        `SELECT 
          a.*
        FROM systemcontrol.webpara a 
        WHERE a.GroupID=?
        ORDER BY a.BranchName ASC`,
        {
          replacements: [webpara[0].GroupID],
          type: QueryTypes.SELECT,
        }
      )

      return webparagroup
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
