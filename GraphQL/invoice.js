const { gql } = require('apollo-server-express')
const db = require('../database')
const { QueryTypes, where, fn, col } = require('sequelize')

const typeDefs = gql`
  type Invoice {
    id: ID
    InvoiceID: String
    InvoiceNo: String
    TotalConsignment: Int
    Amount: String
    InvoiceDate: Date
    InvoiceDate2: Date
  }

  extend type Query {
    invoices: [Invoice]
  }
`

const resolvers = {
  Query: {
    invoices: (root, args, { username, role, client, sessionid, iat }) => {
      const access = ['Customer Service', 'Accounts', 'Management']
      if (!access.includes(role)) return

      // db.invoice.findAll({ where: where(fn('YEAR', col('InvoiceDate')), 2018) })
      return db.sequelize.query(
        `SELECT
        id,
      DATE_FORMAT(InvoiceDate, '%Y-%m-%d') InvoiceID,
      group_concat(InvoiceNo) AS InvoiceNo,
      DATE_FORMAT(InvoiceDate, '%Y-%m-%d') InvoiceDate,
      COUNT(*) AS TotalConsignment,
      FORMAT(SUM(Amount), 2) AS Amount,
      DATE_FORMAT(InvoiceDate, '%d-%m-%Y') InvoiceDate2
  FROM ao_billingheader
  WHERE
      InvoiceNo is not null
  GROUP BY InvoiceDate
  ORDER BY InvoiceDate DESC limit 50`,
        {
          type: QueryTypes.SELECT
        }
      )
    }
  }
}

module.exports = {
  typeDefs,
  resolvers
}
