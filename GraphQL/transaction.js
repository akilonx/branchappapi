const { gql } = require('apollo-server-express')
const db = require('../database')
const crypto = require('crypto')
const { QueryTypes, Op, literal, fn } = require('sequelize')

const typeDefs = gql`
  type Transaction {
    id: ID
    TransactionID: String
    TransactionRef: String
    TransactionDate: Date
    TransactionAmount: String
    PaymentTo: String
    Active: String
    RefundOn: Date
    RefundBy: String
    RefundReason: String
    DebtorCode: String
    TransactionType: String
    TransactionDesc: String
    CreatedOn: Date
    CreatedBy: String
    PaymentTermID: String
    PaymentCCName: String
    PaymentCCNo: String
    ServiceTimes: String
    Client: String
    Qty: Int
    UnitPrice: Float
    OrderNo: String
    DebitCredit: String
    TransactionLocation: String
    PaymentMode: String
    BankName: String
    BuyerName: String
    Error: String
  }

  extend type Query {
    transactionhistory(
      FromDate: Date
      ToDate: Date
      Refund: String
    ): [Transaction]
  }

  extend type Mutation {
    inserttransaction(
      UserID: ID
      ProductID: ID
      Qty: Int
      PriceID: ID
      SalesPerson: String
    ): Transaction

    updatetransaction(id: ID, RefundReason: String): Transaction
  }
`

const resolvers = {
  Query: {
    transactionhistory: async (
      root,
      { FromDate, ToDate, Refund },
      { username, role, client, sessionid, iat }
    ) => {
      if (!FromDate) return { Error: 'Please provide From date' }

      if (!ToDate) return { Error: 'Please provide To date' }
      /* const fromsplit = FromDate.split('/')
      const fr = `${fromsplit[2]}-${fromsplit[0]}-${fromsplit[1]}`

      const tosplit = ToDate.split('/')
      const to = `${tosplit[2]}-${tosplit[0]}-${tosplit[1]}` */

      const checkuser = await db.sequelize.query(
        `SELECT
          Department
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      if (!checkuser[0]) return { Error: 'Not Allowed' }

      const groupid = await db.sequelize.query(
        `SELECT
          GroupID
        FROM systemcontrol.webpara
        WHERE OrgName=?
        `,
        {
          replacements: [client],
          type: QueryTypes.SELECT,
        }
      )

      if (!groupid[0]) return { Error: 'Invalid data request' }

      let webpara

      if (
        checkuser[0].Department == 'Spa Consultant' ||
        checkuser[0].Department == 'Branch Manager'
      ) {
        webpara = await db.sequelize.query(
          `SELECT
            DSN
          FROM systemcontrol.webpara
          WHERE GroupID=? AND BranchCode=?
          `,
          {
            replacements: [groupid[0].GroupID, client],
            type: QueryTypes.SELECT,
          }
        )
      } else {
        webpara = await db.sequelize.query(
          `SELECT
            DSN
          FROM systemcontrol.webpara
          WHERE GroupID=?
          `,
          {
            replacements: [groupid[0].GroupID],
            type: QueryTypes.SELECT,
          }
        )
      }

      //console.log(webpara)
      let transactionsql = ``
      webpara.map(async (para, i) => {
        if (Refund == 'true') {
          transactionsql += `(SELECT
          a.*,
          b.PaymentCCName,
          b.PaymentCCNo,
          c.BankName,
          (SELECT CONCAT(FirstName,' ', LastName, ' ', Phone) FROM spa1.ao_buyerlist WHERE UserID=a.UserID ) as BuyerName
          FROM spa1.ao_transactionlist a
          LEFT JOIN spa1.ao_orderheader b ON b.id = a.TransactionID
          LEFT JOIN spa1.ao_banklist c ON c.id = b.PaymentId
          WHERE ((a.TransactionDate BETWEEN '${FromDate.split('T')[0]}' and '${
            ToDate.split('T')[0]
          }') or DATE(a.TransactionDate) = '${
            ToDate.split('T')[0]
          }' or DATE(a.TransactionDate) = '${
            FromDate.split('T')[0]
          }') AND a.Client='${para.DSN}' AND a.Refund = 1
          ORDER BY a.id DESC)
          `
        } else {
          transactionsql += `(SELECT
          a.*,
          b.PaymentCCName,
          b.PaymentCCNo,
          c.BankName,
          (SELECT CONCAT(FirstName,' ', LastName, ' ', Phone) FROM spa1.ao_buyerlist WHERE UserID=a.UserID ) as BuyerName
          FROM spa1.ao_transactionlist a
          LEFT JOIN spa1.ao_orderheader b ON b.id = a.TransactionID
          LEFT JOIN spa1.ao_banklist c ON c.BankID = b.PaymentId
          WHERE ((a.TransactionDate BETWEEN '${FromDate.split('T')[0]}' and '${
            ToDate.split('T')[0]
          }') or DATE(a.TransactionDate) = '${
            ToDate.split('T')[0]
          }'or DATE(a.TransactionDate) = '${
            FromDate.split('T')[0]
          }') AND a.Client='${para.DSN}' AND a.Refund is null
          ORDER BY a.id DESC)
          `
        }
        if (i < webpara.length - 1) transactionsql += ` UNION `
        if (i == webpara.length - 1) transactionsql += ` ORDER BY id DESC `
        //console.log('output', output, i, webpara.length)
      })

      const transactions = await db.sequelize.query(transactionsql, {
        replacements: [],
        type: QueryTypes.SELECT,
      })
      ////console.log('output', output)
      return transactions
    },
  },

  Mutation: {
    inserttransaction: async (
      root,
      { UserID, ProductID, Qty, PriceID, SalesPerson },
      { username, role, client, sessionid, iat }
    ) => {
      db.changeDB('spa1')

      const user = await db.sequelize.query(
        `SELECT
          a.*,
          b.Branch
        FROM spa1.ao_stafflist a
        LEFT JOIN spa1.ao_buyerlist b ON b.UserID = a.id
        WHERE a.id=?
        `,
        {
          replacements: [UserID],
          type: QueryTypes.SELECT,
        }
      )

      const price = await db.sequelize.query(
        `SELECT
            a.UnitPrice,
            b.Category,
            b.ProductName,
            a.ServiceTimes
          FROM spa1.ao_pricelist a
          LEFT JOIN spa1.ao_productlist b ON a.ProductID=b.id
          WHERE a.id=?
          `,
        {
          replacements: [PriceID],
          type: QueryTypes.SELECT,
        }
      )

      /* const insert = await db.sequelize.query(
          `INSERT INTO
            spa1.ao_cartlist
           SET UserID=?, ProductID=?, PriceID=?, UnitPrice=?, InvoiceAmount=?, Qty=?, SalesPerson=?, SalesClient=?, CreatedDate=NOW()`,
          {
            replacements: [
              UserID,
              ProductID,
              PriceID,
              price[0] && price[0].UnitPrice,
              price[0] && price[0].UnitPrice * Qty,
              Qty,
              SalesPerson,
              client,
            ],
            type: QueryTypes.INSERT,
          }
        ) */

      const orderNoIncrease = await db.freightrunno.update(
        {
          LastNo: literal('LastNo + 1'),
        },
        {
          where: {
            Prefix: 'ODR',
          },
        }
      )

      const orderNo = await db.freightrunno.findOne({
        where: {
          Prefix: 'ODR',
        },
      })

      const OrderNo = `${db.pad(orderNo.LastNo, 6)}`

      const StatusCode = await db.sequelize.query(
        `SELECT * FROM spa1.ao_statuslist WHERE StatusCode='ORDERCREATED'`,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      )
      /*
        const buyer = await db.sequelize.query(
          `SELECT
              Postcode
            FROM spa1.ao_buyerlist
            WHERE UserID=? LIMIT 1
          `,
          {
            replacements: [user[0].id],
            type: QueryTypes.SELECT,
          }
        )

        const postcodes = await db.sequelize.query(
          `SELECT * FROM spa1.ao_postcode WHERE PostCode = ? `,
          {
            replacements: [buyer[0].Postcode],
            type: QueryTypes.SELECT,
          }
        )

         */
      //console.log('here', UserID, ProductID, Qty, PriceID, SalesPerson)

      const token = crypto.randomBytes(64).toString('hex')

      return db.order
        .create({
          OrderNo,
          Status: StatusCode[0] && StatusCode[0].StatusName,
          StatusCode: StatusCode[0] && StatusCode[0].StatusCode,
          UserID: user[0].id,
          CreatedBy: username,
          /*  DeliveryCharges: postcodes[0].Price, */
          PaymentMethod: 'Cash',
          CreatedOn: fn('NOW'),
          Token: token,
        })
        .then(async (a) => {
          const insertorder = await db.orderdetail.create({
            OrderID: a.id,
            OrderNo,
            UserID: user[0].id,
            ProductID,
            TimeStamp: fn('NOW'),
            InvoiceAmount: price[0].UnitPrice * Qty,
            Qty,
            CreatedDate: fn('NOW'),
            PriceID,
            UnitPrice: price[0].UnitPrice,
            Status: 'Completed',
            Category: price[0].Category,
            SalesPerson,
            SalesClient: client,
          })

          let output = {
            UserID: user[0].id,
            DebitCredit: 'CREDIT',
            TransactionID: a.id,
            TransactionRef: ProductID,
            TransactionDate: fn('NOW'),
            TransactionAmount: price[0].UnitPrice * Qty,
            Active: 1,
            CreatedOn: fn('NOW'),
            CreatedBy: username,
            SalesPersonSingle: SalesPerson,
            SalesPersonSingleClient: client,
            SalesPersonPassive: user[0].SalesPerson,
            SalesPersonPassiveClient: user[0].SalesClient,
            Client: client,
            Qty,
            UnitPrice: price[0].UnitPrice,
            PaymentMode: 'CASH',
            PriceID,
            ProductID,
            OrderNo: OrderNo,
            TransactionLocation: 'WALK_IN',
          }

          if (price[0].Category == 2) {
            output.TransactionType = 'BUY_SERVICE'
            output.TransactionDesc = price[0].ProductName
            output.ServiceTimes = price[0].ServiceTimes
          } else {
            output.TransactionType = 'BUY_PRODUCT'
            output.TransactionDesc = price[0].ProductName
          }
          //transactionSave.push(output)

          const insertTransaction = await db.transactionlist.create(output)

          return db.sequelize.query(
            ` SELECT
                *
              FROM spa1.ao_transactionlist
              WHERE id=?
            `,
            {
              replacements: [insertTransaction.id],
              type: QueryTypes.SELECT,
            }
          )
        })
    },

    updatetransaction: async (
      root,
      { id, RefundReason },
      { username, role, client, sessionid, iat }
    ) => {
      const checkuser = await db.sequelize.query(
        `SELECT
          Department
        FROM ${client}.ao_stafflist
        WHERE StaffID=?
        `,
        {
          replacements: [username],
          type: QueryTypes.SELECT,
        }
      )

      if (
        checkuser[0].Department != 'Management' &&
        checkuser[0].Department != 'Supervisor'
      )
        return { Error: 'Not Allowed' }

      const refundtransaction = await db.sequelize.query(
        `SELECT
            *
          FROM spa1.ao_transactionlist
          WHERE id=?
          `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      if (!refundtransaction[0]) return { Error: 'Error not found' }

      const usedservicetimes = await db.sequelize.query(
        `SELECT sum(TotalPerson) as TotalUsed FROM spa1.ao_bookinglist WHERE CreatedBy=?`,
        {
          replacements: [refundtransaction[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      const transaction = await db.sequelize.query(
        `SELECT sum(ServiceTimes) as ServiceTimes FROM spa1.ao_transactionlist where TransactionType="BUY_SERVICE" and Refund is null and CreatedBy=? and Active=1;`,
        {
          replacements: [refundtransaction[0].CreatedBy],
          type: QueryTypes.SELECT,
        }
      )

      const remainingTimes =
        transaction[0].ServiceTimes - usedservicetimes[0].TotalUsed

      if (refundtransaction[0].ServiceTimes > remainingTimes)
        return { Error: 'This user does not enough booking pass for refund' }

      const update = await db.sequelize.query(
        `UPDATE
          spa1.ao_transactionlist
        SET
          Active=0,
          RefundReason=?,
          Refund=1,
          RefundBy=?,
          RefundOn=now()
         WHERE id=? `,
        {
          replacements: [RefundReason, username, id],
          type: QueryTypes.UPDATE,
        }
      )

      const query = await db.sequelize.query(
        ` SELECT
            *
          FROM spa1.ao_transactionlist
          WHERE id=?
        `,
        {
          replacements: [id],
          type: QueryTypes.SELECT,
        }
      )

      return query[0]
    },
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
