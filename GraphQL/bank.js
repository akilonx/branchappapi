const { gql } = require('apollo-server-express')
const db = require('../database')

const { QueryTypes, Op, literal } = require('sequelize')

const typeDefs = gql`
  scalar Date

  type Bank {
    id: ID!
    BankID: String
    BankName: String
  }

  extend type Query {
    banks: [Bank]
  }
`

const resolvers = {
  Query: {
    banks: async (root, args) =>
      db.sequelize.query(
        `SELECT 
        *
        FROM spa1.ao_banklist 
        ORDER by BankName asc
        `,
        {
          replacements: [],
          type: QueryTypes.SELECT,
        }
      ),
  },
}

module.exports = {
  typeDefs,
  resolvers,
}
