CREATE TABLE `ao_closingdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ClosingID` int DEFAULT NULL,
  `ItemID` varchar(245) DEFAULT NULL,
  `BranchCode` varchar(20) DEFAULT NULL,
  `StockOutQty` decimal(10,1) DEFAULT NULL,
  `StockOutBags` decimal(10,1) DEFAULT NULL,
  `StockInQty` decimal(10,1) DEFAULT NULL,
  `StockInBags` decimal(10,1) DEFAULT NULL,
  `ClosingBalanceQty` decimal(10,1) DEFAULT NULL,
  `ClosingBalanceBags` decimal(10,1) DEFAULT NULL,
  `OpeningBalanceQty` decimal(10,1) DEFAULT NULL,
  `OpeningBalanceBags` decimal(10,1) DEFAULT NULL,
  `KGToBags` decimal(10,1) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ClosingHeader_idx3` (`ClosingID`),
  CONSTRAINT `ClosingHeader3` FOREIGN KEY (`ClosingID`) REFERENCES `ao_closingheader` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

CREATE TABLE `ao_closingheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ClosingDate` date DEFAULT NULL,
  `ClosingDateStart` date DEFAULT NULL,
  `ClosingType` varchar(20) DEFAULT NULL,
  `FarmCode` varchar(20) DEFAULT NULL,
  `StockOutQty` decimal(10,1) DEFAULT NULL,
  `StockOutBags` decimal(10,1) DEFAULT NULL,
  `StockInQty` decimal(10,1) DEFAULT NULL,
  `StockInBags` decimal(10,1) DEFAULT NULL,
  `ClosingBalanceQty` decimal(10,1) DEFAULT NULL,
  `ClosingBalanceBags` decimal(10,1) DEFAULT NULL,
  `OpeningBalanceQty` decimal(10,1) DEFAULT NULL,
  `OpeningBalanceBags` decimal(10,1) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

CREATE TABLE `ao_closingindetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ClosingID` int DEFAULT NULL,
  `ItemID` varchar(245) DEFAULT NULL,
  `DataEntryID` longtext,
  `FarmCode` varchar(20) DEFAULT NULL,
  `Qty` decimal(10,1) DEFAULT NULL,
  `Bags` decimal(10,1) DEFAULT NULL,
  `KGToBags` decimal(10,1) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ClosingHeader_idx` (`ClosingID`),
  CONSTRAINT `ClosingHeader` FOREIGN KEY (`ClosingID`) REFERENCES `ao_closingheader` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

CREATE TABLE `ao_closingoutdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ClosingID` int DEFAULT NULL,
  `ItemID` varchar(245) DEFAULT NULL,
  `DataEntryID` longtext,
  `FarmCode` varchar(20) DEFAULT NULL,
  `Qty` decimal(10,1) DEFAULT NULL,
  `Bags` decimal(10,1) DEFAULT NULL,
  `KGToBags` decimal(10,1) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ClosingHeader_idx` (`ClosingID`),
  KEY `ClosingHeader_idx2` (`ClosingID`),
  CONSTRAINT `ClosingHeader2` FOREIGN KEY (`ClosingID`) REFERENCES `ao_closingheader` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
