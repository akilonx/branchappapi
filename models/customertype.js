module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    'customertype',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      CompanyTypeCode: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      CompanyTypeName: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      Ordering: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    },
    {
      tableName: 'ao_companytypelist',
      timestamps: false
    }
  )
}
//id, CompanyTypeCode, CompanyTypeName
