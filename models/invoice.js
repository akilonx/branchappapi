module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    'invoice',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      InvoiceNo: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      ConsignmentNo: {
        type: DataTypes.STRING(1000),
        allowNull: true
      },

      ConsignmentID: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      InvoiceAmount: {
        type: DataTypes.DECIMAL(18, 2),
        allowNull: true
      },
      InvoiceDate: {
        type: DataTypes.DATE,
        allowNull: true
      }
    },
    {
      tableName: 'ao_invoiceheader',
      timestamps: false
    }
  )
}
