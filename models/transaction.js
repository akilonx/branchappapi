module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'transactionlist',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      TransactionID: {
        type: DataTypes.INTEGER(8),
        allowNull: true,
      },
      TransactionRef: {
        type: DataTypes.STRING(145),
        allowNull: true,
      },
      TransactionDate: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      TransactionAmount: {
        type: DataTypes.DECIMAL(18, 2),
        allowNull: true,
      },
      PaymentTo: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      DebtorCode: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      TransactionType: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      TransactionDesc: {
        type: DataTypes.STRING(245),
        allowNull: true,
      },
      CreatedOn: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      CreatedBy: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      PaymentTermID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      ServiceTimes: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      SalesPersonSingle: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      SalesPersonSingleClient: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      SalesPersonPassive: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      SalesPersonPassiveClient: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      Client: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      Qty: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      UnitPrice: {
        type: DataTypes.DECIMAL(18, 2),
        allowNull: true,
      },
      DebitCredit: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      OrderNo: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
      ProductID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      PriceID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      UserID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      TransactionLocation: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      PaymentMode: {
        type: DataTypes.STRING(10),
        allowNull: true,
      },
    },
    {
      tableName: 'ao_transactionlist',
      timestamps: false,
    }
  )
}

//id, TransactionID, TransactionRef, TransactionDate, TransactionAmount, PaymentTo, Active, DebtorCode, TransactionType,
//TransactionDesc, CreatedOn, CreatedBy, PaymentTermID, ServiceTimes
