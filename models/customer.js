module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    'customer',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      CustomerCode: {
        type: DataTypes.STRING(10),
        allowNull: false
      },
      CustomerName: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      CompanyCode: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      PersonInCharge: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      PersonInCharge2: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      CustomerType: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      CountryCode: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      PaymentTermCode: {
        type: DataTypes.STRING(10),
        allowNull: true
      },
      StaffID: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      Address1: {
        type: DataTypes.STRING(250),
        allowNull: true
      },
      Address2: {
        type: DataTypes.STRING(250),
        allowNull: true
      },
      Address3: {
        type: DataTypes.STRING(250),
        allowNull: true
      },
      Address4: {
        type: DataTypes.STRING(250),
        allowNull: true
      },
      TelNo1: {
        type: DataTypes.STRING(50),
        allowNull: true
      },
      TelNo2: {
        type: DataTypes.STRING(50),
        allowNull: true
      },
      FaxNo1: {
        type: DataTypes.STRING(50),
        allowNull: true
      },
      FaxNo2: {
        type: DataTypes.STRING(50),
        allowNull: true
      },
      Email1: {
        type: DataTypes.STRING(50),
        allowNull: true
      },
      Email2: {
        type: DataTypes.STRING(50),
        allowNull: true
      },
      Remark: {
        type: DataTypes.STRING(250),
        allowNull: true
      },
      UpdatedBy: {
        type: DataTypes.STRING(20),
        allowNull: true
      },
      LastModified: {
        type: DataTypes.DATE,
        allowNull: true
      },
      CreatedBy: {
        type: DataTypes.STRING(20),
        allowNull: true
      },
      CreateOn: {
        type: DataTypes.DATE,
        allowNull: true
      },
      Status: {
        type: DataTypes.STRING(4),
        allowNull: true
      },
      ActiveQuotation: {
        type: DataTypes.STRING(10),
        allowNull: true
      },
      CoLoaderCode: {
        type: DataTypes.STRING(10),
        allowNull: true
      },
      Ordering: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    },
    {
      tableName: 'ao_customerlist',
      timestamps: false
    }
  )
}
