module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    'person',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      ClientID: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      FirstName: {
        type: DataTypes.STRING(245),
        allowNull: true
      },
      LastName: {
        type: DataTypes.STRING(245),
        allowNull: true
      },
      Email: {
        type: DataTypes.STRING(245),
        allowNull: true
      },
      Mobile: {
        type: DataTypes.STRING(45),
        allowNull: true
      },
      Phone: {
        type: DataTypes.STRING(45),
        allowNull: true
      },
      PreAlert: {
        type: DataTypes.STRING(2),
        allowNull: true
      },
      Active: {
        type: DataTypes.STRING(1),
        allowNull: true
      },
      Username: {
        type: DataTypes.STRING(11),
        allowNull: true
      },
      Password: {
        type: DataTypes.STRING(11),
        allowNull: true
      },
      PersonInCharge: {
        type: DataTypes.STRING(2),
        allowNull: true
      }
    },
    {
      tableName: 'ao_personlist',
      timestamps: false
    }
  )
}
//id, ClientID, FirstName, LastName, Email, Mobile, Phone, PreAlert, CreatedDate, UpdatedOn, CreatedBy, UpdatedBy, Active, Username, Password
/*
CREATE TABLE `ao_personlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) DEFAULT NULL,
  `FirstName` varchar(145) DEFAULT NULL,
  `LastName` varchar(145) DEFAULT NULL,
  `Email` varchar(245) DEFAULT NULL,
  `Mobile` varchar(45) DEFAULT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `PreAlert` varchar(2) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `UpdatedOn` date DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `Active` varchar(1) DEFAULT NULL,
  `Username` varchar(45) DEFAULT NULL,
  `Password` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
*/
