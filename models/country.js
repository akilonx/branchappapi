module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    'country',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      CountryCode: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      CountryName: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      Ordering: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    },
    { tableName: 'ao_countrylist', timestamp: false }
  )
}
/*
CREATE TABLE `ao_countrylist` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `CountryCode` longtext NOT NULL,
  `CountryName` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;
 */
