module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    'staff',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      StaffID: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      StaffName: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      Department: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      Designation: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
      Password2: {
        type: DataTypes.STRING(145),
        allowNull: true
      },
      AgentCode: {
        type: DataTypes.STRING(45),
        allowNull: true
      }
    },
    {
      tableName: 'ao_stafflist',
      timestamps: false
    }
  )
}
//id, PaymentTermCode, PaymentTermDesc, PaymentTermDays
