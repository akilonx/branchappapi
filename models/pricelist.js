module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    'pricelist',
    {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      ProductID: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      Unit: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      UnitPrice: {
        type: DataTypes.DECIMAL(18, 2),
        allowNull: true,
      },
      Uom: {
        type: DataTypes.STRING(20),
        allowNull: true,
      },
      CreatedOn: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      CreatedBy: {
        type: DataTypes.STRING(45),
        allowNull: true,
      },
    },
    {
      tableName: 'ao_pricelist',
      timestamps: false,
    }
  )
}
//id, ClientID, FirstName, LastName, Email, Mobile, Phone, PreAlert, CreatedDate, UpdatedOn, CreatedBy, UpdatedBy, Active, Username, Password
/*
  CREATE TABLE `ao_productlist` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `ProductID` varchar(8) NOT NULL,
    `DepartmentID` varchar(8) NOT NULL,
    `Category` varchar(45) NOT NULL,
    `IDSKU` varchar(8) NOT NULL,
    `ProductName` varchar(45) NOT NULL,
    `Quantity` int(11) NOT NULL,
    `UnitPrice` decimal(10,0) NOT NULL,
    `Ranking` int(11) DEFAULT NULL,
    `ProductDesc` text,
    `UnitsInStock` int(11) DEFAULT NULL,
    `UnitsInOrder` int(11) DEFAULT NULL,
    `Picture` blob,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
  
  */
