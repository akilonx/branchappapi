-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: spa2
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ao_activitylog`
--

DROP TABLE IF EXISTS `ao_activitylog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_activitylog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `LogType` varchar(45) DEFAULT NULL,
  `ConsignmentID` int DEFAULT NULL,
  `MAWBID` int DEFAULT NULL,
  `HAWBID` int DEFAULT NULL,
  `User` varchar(45) DEFAULT NULL,
  `Action` varchar(245) DEFAULT NULL,
  `ActionType` varchar(45) DEFAULT NULL,
  `Module` varchar(45) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_activitylog`
--

LOCK TABLES `ao_activitylog` WRITE;
/*!40000 ALTER TABLE `ao_activitylog` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_activitylog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_authorisationlist`
--

DROP TABLE IF EXISTS `ao_authorisationlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_authorisationlist` (
  `ProgramID` varchar(20) DEFAULT NULL,
  `StaffID` varchar(20) DEFAULT NULL,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` varchar(20) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_authorisationlist`
--

LOCK TABLES `ao_authorisationlist` WRITE;
/*!40000 ALTER TABLE `ao_authorisationlist` DISABLE KEYS */;
INSERT INTO `ao_authorisationlist` VALUES ('CSG010              ','CHANDRA             ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CST010              ','CHELSEA             ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CUS010              ','CHANDRA             ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','YAPHS               ','2008-07-29 20:29:31','YAPHS               ',NULL,NULL),('INV010              ','YAPHS               ','2008-07-29 20:29:31','YAPHS               ',NULL,NULL),('MNT010              ','YAPHS               ','2008-07-29 20:29:31','YAPHS               ',NULL,NULL),('CSG010              ','CHELSEA             ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CST010              ','DAMIEN              ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CUS010              ','CHELSEA             ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','DAMIEN              ','2008-07-29 20:29:41','YAPHS               ',NULL,NULL),('INV010              ','DAMIEN              ','2008-07-29 20:29:41','YAPHS               ',NULL,NULL),('MNT010              ','DAMIEN              ','2008-07-29 20:29:41','YAPHS               ',NULL,NULL),('CSG010              ','CLTEH               ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CUS010              ','CLTEH               ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','CHELSEA             ','2011-09-22 12:17:20','PEILING             ',NULL,NULL),('INV010              ','CHELSEA             ','2011-09-22 12:17:20','PEILING             ',NULL,NULL),('CUS010              ','DAMIEN              ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CUS010              ','DRIVER              ','2011-11-09 12:27:30','PEILING             ',NULL,NULL),('CSG010              ','DAMIEN              ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CST010              ','JASLINE             ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CUS010              ','JASLINE             ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','CHAN                ','2009-05-22 10:46:05','CHAN                ',NULL,NULL),('INV010              ','CHAN                ','2009-05-22 10:46:05','CHAN                ',NULL,NULL),('MNT010              ','CHAN                ','2009-05-22 10:46:05','CHAN                ',NULL,NULL),('CSG010              ','JASLINE             ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('INQ010              ','IDA                 ','2009-10-19 19:50:20','JAYA                ',NULL,NULL),('CSG010              ','JAYA2               ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CUS010              ','LEE                 ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CSG010              ','LEE                 ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CST010              ','JAYA2               ','2011-09-23 14:53:39','PEILING             ',NULL,NULL),('CSG010              ','MAYYEN              ','2011-10-06 17:09:16','PEILING             ',NULL,NULL),('CUS010              ','MAYYEN              ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CST010              ','MAYYEN              ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CUS010              ','PEILING             ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('INQ010              ','INA                 ','2008-11-11 16:25:20','JAYA                ',NULL,NULL),('INV010              ','INA                 ','2008-11-11 16:25:20','JAYA                ',NULL,NULL),('CUS010              ','SESIM               ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CUS010              ','TGCHAN              ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CUS010              ','YAPHS               ','2011-09-23 14:24:58','PEILING             ',NULL,NULL),('CST010              ','PEILING             ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CST010              ','SESIM               ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CST010              ','TGCHAN              ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CST010              ','YAPHS               ','2011-09-23 14:52:40','PEILING             ',NULL,NULL),('CSG010              ','PEILING             ','2011-10-06 17:09:17','PEILING             ',NULL,NULL),('CSG010              ','SESIM               ','2011-10-06 17:09:17','PEILING             ',NULL,NULL),('CSG010              ','SYAFIQ              ','2011-10-07 10:11:33','PEILING             ',NULL,NULL),('CSG010              ','TGCHAN              ','2011-10-06 17:09:17','PEILING             ',NULL,NULL),('CSG010              ','YAPHS               ','2011-10-06 17:09:17','PEILING             ',NULL,NULL),('CUS010              ','JAYA2               ','2011-09-23 14:53:39','PEILING             ',NULL,NULL),('INQ010              ','JAYA2               ','2011-09-23 14:53:39','PEILING             ',NULL,NULL),('INV010              ','JAYA2               ','2011-09-23 14:53:39','PEILING             ',NULL,NULL),('CUS010              ','SYAFIQ              ','2011-10-07 10:11:33','PEILING             ',NULL,NULL),('CSG010              ','ZAC                 ','2011-11-01 13:56:23','PEILING             ',NULL,NULL),('CUS010              ','ZAC                 ','2011-11-01 13:56:23','PEILING             ',NULL,NULL),('MNT010              ','INA                 ','2008-11-11 16:25:20','JAYA                ',NULL,NULL),('INQ010              ','PEILING             ','2009-06-01 12:14:39','PEILING             ',NULL,NULL),('MNT010              ','PEILING             ','2009-06-01 12:14:39','PEILING             ',NULL,NULL),('RPT010              ','PEILING             ','2009-06-01 12:14:39','PEILING             ',NULL,NULL),('CSG010              ','HUSNA               ','2012-01-06 10:11:27','PEILING             ',NULL,NULL),('CUS010              ','HUSNA               ','2012-01-06 10:11:27','PEILING             ',NULL,NULL),('INQ010              ','HUSNA               ','2012-01-06 10:11:27','PEILING             ',NULL,NULL),('INQ010              ','LIM                 ','2009-02-10 17:01:35','JAYA                ',NULL,NULL),('INV010              ','LIM                 ','2009-02-10 17:01:35','JAYA                ',NULL,NULL),('INV010              ','HUSNA               ','2012-01-06 10:11:27','PEILING             ',NULL,NULL),('CSG010              ','VANI                ','2012-01-18 10:31:16','PEILING             ',NULL,NULL),('CUS010              ','VANI                ','2012-01-18 10:31:16','PEILING             ',NULL,NULL),('INQ010              ','VANI                ','2012-01-18 10:31:16','PEILING             ',NULL,NULL),('CSG010              ','ABU                 ','2012-02-21 14:43:24','PEILING             ',NULL,NULL),('CUS010              ','ABU                 ','2012-02-21 14:43:24','PEILING             ',NULL,NULL),('CSG010              ','NURUL               ','2012-04-17 11:02:02','PEILING             ',NULL,NULL),('CUS010              ','NURUL               ','2012-04-17 11:02:02','PEILING             ',NULL,NULL),('INQ010              ','NURUL               ','2012-04-17 11:02:02','PEILING             ',NULL,NULL),('INQ010              ','TGCHAN              ','2009-11-18 14:35:35','PEILING             ',NULL,NULL),('INV010              ','TGCHAN              ','2009-11-18 14:35:35','PEILING             ',NULL,NULL),('MNT010              ','TGCHAN              ','2009-11-18 14:35:35','PEILING             ',NULL,NULL),('RPT010              ','DAMIEN              ','2009-04-02 09:12:07','YAPHS               ',NULL,NULL),('RPT010              ','YAPHS               ','2009-04-02 09:12:07','YAPHS               ',NULL,NULL),('CSG010              ','SURI                ','2012-11-19 16:24:49','PEILING             ',NULL,NULL),('CUS010              ','SURI                ','2012-11-19 16:24:49','PEILING             ',NULL,NULL),('INQ010              ','SURI                ','2012-11-19 16:24:49','PEILING             ',NULL,NULL),('INV010              ','SURI                ','2012-11-19 16:24:49','PEILING             ',NULL,NULL),('CSG010              ','SHAMSUL             ','2012-11-19 16:25:21','PEILING             ',NULL,NULL),('CUS010              ','SHAMSUL             ','2012-11-19 16:25:21','PEILING             ',NULL,NULL),('CSG010              ','ROS                 ','2012-12-07 15:12:45','PEILING             ',NULL,NULL),('CUS010              ','ROS                 ','2012-12-07 15:12:45','PEILING             ',NULL,NULL),('INQ010              ','ROS                 ','2012-12-07 15:12:45','PEILING             ',NULL,NULL),('INV010              ','ROS                 ','2012-12-07 15:12:45','PEILING             ',NULL,NULL),('RPT010              ','CHAN                ','2009-05-22 10:46:05','CHAN                ',NULL,NULL),('INV010              ','PEILING             ','2009-06-01 12:14:39','PEILING             ',NULL,NULL),('INQ010              ','SELIM               ','2009-09-11 18:10:46','PEILING             ',NULL,NULL),('INV010              ','SELIM               ','2009-09-11 18:10:46','PEILING             ',NULL,NULL),('INQ010              ','SESIM               ','2009-09-11 18:11:35','PEILING             ',NULL,NULL),('INV010              ','SESIM               ','2009-09-11 18:11:35','PEILING             ',NULL,NULL),('INQ010              ','CHANDRA             ','2009-09-17 12:21:05','PEILING             ',NULL,NULL),('INV010              ','CHANDRA             ','2009-09-17 12:21:05','PEILING             ',NULL,NULL),('RPT010              ','TGCHAN              ','2009-11-18 14:35:35','PEILING             ',NULL,NULL),('INQ010              ','CLTEH               ','2011-08-12 10:55:31','PEILING             ',NULL,NULL),('INV010              ','CLTEH               ','2011-08-12 10:55:31','PEILING             ',NULL,NULL),('MNT010              ','CLTEH               ','2011-08-12 10:55:31','PEILING             ',NULL,NULL),('INQ010              ','LEE                 ','2011-08-12 11:05:03','CLTEH               ',NULL,NULL),('INV010              ','LEE                 ','2011-08-12 11:05:03','CLTEH               ',NULL,NULL),('INQ010              ','MAYYEN              ','2011-08-12 11:03:22','CLTEH               ',NULL,NULL),('INV010              ','MAYYEN              ','2011-08-12 11:03:22','CLTEH               ',NULL,NULL),('INQ010              ','JASLINE             ','2011-08-12 11:03:53','CLTEH               ',NULL,NULL),('INV010              ','JASLINE             ','2011-08-12 11:03:53','CLTEH               ',NULL,NULL);
/*!40000 ALTER TABLE `ao_authorisationlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_bookinglist`
--

DROP TABLE IF EXISTS `ao_bookinglist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_bookinglist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `BranchID` int DEFAULT NULL,
  `BranchCode` varchar(45) DEFAULT NULL,
  `BookFrom` datetime DEFAULT NULL,
  `BookTo` datetime DEFAULT NULL,
  `TotalHours` decimal(8,2) DEFAULT NULL,
  `Status` varchar(145) DEFAULT NULL,
  `CreatedBy` varchar(145) DEFAULT NULL,
  `CreatedAt` datetime DEFAULT NULL,
  `UpdatedBy` varchar(145) DEFAULT NULL,
  `UpdatedAt` datetime DEFAULT NULL,
  `Token` varchar(145) DEFAULT NULL,
  `TotalPerson` smallint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_bookinglist`
--

LOCK TABLES `ao_bookinglist` WRITE;
/*!40000 ALTER TABLE `ao_bookinglist` DISABLE KEYS */;
INSERT INTO `ao_bookinglist` VALUES (26,NULL,'spa2','2020-09-07 13:00:00','2020-09-07 14:00:00',1.00,'New','01110340242','2020-09-07 07:55:48',NULL,NULL,NULL,1),(30,NULL,'spa2','2020-09-07 14:00:00','2020-09-07 15:00:00',1.00,'New','01110340242','2020-09-07 07:57:43',NULL,NULL,NULL,1),(55,NULL,'spa2','2020-09-07 15:00:00','2020-09-07 16:30:00',1.50,'New','01110340242','2020-09-07 08:22:47',NULL,NULL,NULL,1),(60,NULL,'spa2','2020-09-07 17:00:00','2020-09-07 18:00:00',1.00,'New','01110340242','2020-09-07 08:27:30',NULL,NULL,NULL,1),(63,NULL,'spa2','2020-09-07 12:00:00','2020-09-07 13:00:00',1.00,'New','01110340242','2020-09-07 08:28:10',NULL,NULL,NULL,1),(66,NULL,'spa2','2020-09-07 11:00:00','2020-09-07 12:00:00',1.00,'New','01110340242','2020-09-07 08:41:18',NULL,NULL,NULL,1),(95,NULL,'spa2','2020-09-09 09:00:00','2020-09-09 10:00:00',1.00,'New','01110340242','2020-09-07 13:54:19',NULL,NULL,NULL,1),(97,NULL,'spa2','2020-09-10 09:00:00','2020-09-10 10:00:00',1.00,'New','01110340242','2020-09-07 13:54:58',NULL,NULL,NULL,1),(100,NULL,'spa2','2020-09-10 11:00:00','2020-09-10 12:00:00',1.00,'New','01110340242','2020-09-07 13:57:47',NULL,NULL,NULL,1),(102,NULL,'spa2','2020-09-06 12:00:00','2020-09-06 13:00:00',1.00,'New','01110340242','2020-09-07 13:59:40',NULL,NULL,NULL,1),(103,NULL,'spa2','2020-09-07 09:00:00','2020-09-07 10:30:00',1.50,'New','01110340242','2020-09-07 18:23:27',NULL,NULL,NULL,1),(105,NULL,'spa2','2020-09-09 11:00:00','2020-09-09 12:00:00',1.00,'New','01110340242','2020-09-09 10:46:02',NULL,NULL,NULL,1),(107,NULL,'spa2','2020-11-04 09:00:00','2020-11-04 10:00:00',1.00,'New','admin','2020-11-04 09:29:23',NULL,NULL,'46b60701805cb562192973a2bc213412ff2842f69b0f6918ec7f2cc87f08644cc4ac5111b9bcd55b244e243015fa8cf228fc8d1526a213cf9da0c048eaf0991d',1),(108,NULL,'spa2','2020-11-23 11:00:00','2020-11-23 12:00:00',1.00,'New','01110340242','2020-11-23 08:13:41',NULL,NULL,NULL,1),(110,NULL,'spa2','2020-11-24 11:00:00','2020-11-24 12:00:00',1.00,'New','01110340242','2020-11-23 08:14:05',NULL,NULL,NULL,1),(112,NULL,'spa2','2021-04-01 11:00:00','2020-11-23 12:00:00',1.00,'New','01110340242','2020-11-23 08:18:08',NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `ao_bookinglist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_buyerlist`
--

DROP TABLE IF EXISTS `ao_buyerlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_buyerlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `Email` varchar(245) DEFAULT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `FirstName` varchar(245) DEFAULT NULL,
  `LastName` varchar(245) DEFAULT NULL,
  `Address1` varchar(245) DEFAULT NULL,
  `Address2` varchar(245) DEFAULT NULL,
  `City` varchar(245) DEFAULT NULL,
  `Postcode` varchar(20) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_buyerlist`
--

LOCK TABLES `ao_buyerlist` WRITE;
/*!40000 ALTER TABLE `ao_buyerlist` DISABLE KEYS */;
INSERT INTO `ao_buyerlist` VALUES (3,174,'akilonx@gmail.com','01110340242','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47120','Selangor Darul Ehsan','Malaysia','2020-05-20 06:33:40','2020-06-24 16:17:42'),(4,175,'cynthialee@gmail.com','01110390242','Cynthia','Lee','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47130','Selangor Darul Ehsan','Malaysia','2020-05-20 06:45:32','2020-05-20 06:46:34'),(5,176,'kok_78@yahoo.com','0162221431','chew','kok ','No.105, Block 1, Jalan Nelayan 19','Jalan nelayan','Shah Alam','40300','Selangor Darul Ehsan','Malaysia','2020-05-21 10:21:39','2020-05-21 11:10:47'),(6,177,'ssimc@hotmail.com','0123053761','Tiki','Tiki','25, usj 13/2','UEP Subang Jaya','Subang Jaya','47630','Selangor Darul Ehsan','Malaysia','2020-05-27 15:06:26',NULL),(7,178,'Atoindesto@gmail.com','0123111531','Chin yee','Toon','48 jalan tpp 1/18','Taman Perindustrian Puchong ','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-05-27 15:06:27',NULL);
/*!40000 ALTER TABLE `ao_buyerlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_cartlist`
--

DROP TABLE IF EXISTS `ao_cartlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_cartlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `OrderID` varchar(8) DEFAULT NULL,
  `UserID` int DEFAULT NULL,
  `ShipperID` varchar(8) DEFAULT NULL,
  `ProductID` int DEFAULT NULL,
  `ProductNo` varchar(45) DEFAULT NULL,
  `OrderDate` date DEFAULT NULL,
  `RequiredDate` date DEFAULT NULL,
  `Freight` decimal(10,0) DEFAULT NULL,
  `SalesTax` decimal(10,0) DEFAULT NULL,
  `TimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TransactStatus` varchar(25) DEFAULT NULL,
  `InvoiceAmount` decimal(10,2) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `Qty` int DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `PriceID` int DEFAULT NULL,
  `UnitPrice` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UserID_idx` (`UserID`),
  KEY `ShipperID_idx` (`ShipperID`)
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_cartlist`
--

LOCK TABLES `ao_cartlist` WRITE;
/*!40000 ALTER TABLE `ao_cartlist` DISABLE KEYS */;
INSERT INTO `ao_cartlist` VALUES (265,NULL,54,NULL,38,NULL,NULL,NULL,NULL,NULL,'2020-11-04 09:21:20',NULL,1000.00,NULL,1,'2020-09-03 10:14:39',43,1000.00),(306,NULL,54,NULL,34,NULL,NULL,NULL,NULL,NULL,'2020-11-04 09:21:15',NULL,196.00,NULL,2,'2020-09-14 20:24:22',38,98.00),(307,NULL,174,NULL,38,NULL,NULL,NULL,NULL,NULL,'2020-09-18 07:17:25',NULL,110.00,NULL,1,'2020-09-18 07:17:25',42,110.00),(308,NULL,54,NULL,38,NULL,NULL,NULL,NULL,NULL,'2020-11-04 09:21:19',NULL,330.00,NULL,3,'2020-11-04 07:49:44',42,110.00);
/*!40000 ALTER TABLE `ao_cartlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_categorylist`
--

DROP TABLE IF EXISTS `ao_categorylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_categorylist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(255) DEFAULT NULL,
  `ParentID` int DEFAULT NULL,
  `CategoryType` varchar(45) DEFAULT NULL,
  `Ordering` int DEFAULT NULL,
  `Prettier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_categorylist`
--

LOCK TABLES `ao_categorylist` WRITE;
/*!40000 ALTER TABLE `ao_categorylist` DISABLE KEYS */;
INSERT INTO `ao_categorylist` VALUES (1,'Product',NULL,'product',1,'product'),(2,'Service',NULL,'product',2,'service');
/*!40000 ALTER TABLE `ao_categorylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_companytypelist`
--

DROP TABLE IF EXISTS `ao_companytypelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_companytypelist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CompanyTypeCode` longtext NOT NULL,
  `CompanyTypeName` longtext,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` varchar(45) DEFAULT NULL,
  `UpdateOn` datetime DEFAULT NULL,
  `UpdateBy` varchar(45) DEFAULT NULL,
  `Ordering` int DEFAULT NULL,
  `Active` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_companytypelist`
--

LOCK TABLES `ao_companytypelist` WRITE;
/*!40000 ALTER TABLE `ao_companytypelist` DISABLE KEYS */;
INSERT INTO `ao_companytypelist` VALUES (1,'FGA_ADA','AF Airline Direct Agent',NULL,NULL,NULL,NULL,3,'A'),(2,'FGA_AIR','AF Air Line',NULL,NULL,NULL,NULL,3,'A'),(3,'FGA_ECS','AF Export Consignee',NULL,NULL,NULL,NULL,NULL,NULL),(4,'FGA_ECT','AF Export Customer / Bill To',NULL,NULL,NULL,NULL,NULL,NULL),(5,'FGA_ESA','AF Export Sub Agent',NULL,NULL,NULL,NULL,NULL,NULL),(6,'FGA_ESP','AF Export Shipper',NULL,NULL,NULL,NULL,NULL,NULL),(7,'FGA_ICS','AF Import Consignee',NULL,NULL,NULL,NULL,NULL,NULL),(8,'FGA_ICT','AF Import Customer / Bill To',NULL,NULL,NULL,NULL,NULL,NULL),(9,'FGA_ISP','AF Import Shipper',NULL,NULL,NULL,NULL,NULL,NULL),(10,'FGA_OAG','AF Oversea Agent',NULL,NULL,NULL,NULL,NULL,NULL),(11,'FGA_VDR','AF Vendor',NULL,NULL,NULL,NULL,NULL,NULL),(15,'FGA_AGN','AF Agent',NULL,NULL,NULL,NULL,NULL,'A');
/*!40000 ALTER TABLE `ao_companytypelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_countrylist`
--

DROP TABLE IF EXISTS `ao_countrylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_countrylist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `CountryCode` longtext NOT NULL,
  `CountryName` longtext,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `Ordering` int DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_countrylist`
--

LOCK TABLES `ao_countrylist` WRITE;
/*!40000 ALTER TABLE `ao_countrylist` DISABLE KEYS */;
INSERT INTO `ao_countrylist` VALUES (1,'AD','ANDORRA',NULL,NULL,NULL,NULL,1),(2,'AE','UNITED ARAB EMIRATES',NULL,NULL,NULL,NULL,1),(3,'AF','AFGHANISTAN',NULL,NULL,NULL,NULL,1),(4,'AG','ANTIGUA AND BARBUDA',NULL,NULL,NULL,NULL,1),(5,'AI','ANGUILLA',NULL,NULL,'2020-03-04 07:54:16',NULL,2),(6,'AL','ALBANIA',NULL,NULL,NULL,NULL,1),(7,'AM','ARMENIA',NULL,NULL,NULL,NULL,1),(8,'AN','NETHERLANDS ANTILLES',NULL,NULL,NULL,NULL,1),(9,'AO','ANGOLA',NULL,NULL,'2020-03-04 07:59:28',NULL,2),(10,'AQ','ANTARCTICA',NULL,NULL,'2020-03-04 07:58:54',NULL,2),(11,'AR','ARGENTINA',NULL,NULL,NULL,NULL,1),(12,'AS','AMERICAN SAMOA',NULL,NULL,NULL,NULL,1),(13,'AT','AUSTRIA',NULL,NULL,NULL,NULL,1),(14,'AU','AUSTRALIA',NULL,NULL,NULL,NULL,1),(15,'AW','ARUBA',NULL,NULL,NULL,NULL,1),(16,'AZ','AZERBAIJAN',NULL,NULL,NULL,NULL,1),(17,'BA','BOSNIA AND HERZEGOWINA',NULL,NULL,NULL,NULL,1),(18,'BB','BARBADOS',NULL,NULL,NULL,NULL,1),(19,'BD','BANGLADESH',NULL,NULL,NULL,NULL,1),(20,'BE','BELGIUM',NULL,NULL,NULL,NULL,1),(21,'BF','BURKINA FASO',NULL,NULL,NULL,NULL,1),(22,'BG','BULGARIA',NULL,NULL,NULL,NULL,1),(23,'BH','BAHRAIN',NULL,NULL,NULL,NULL,1),(24,'BI','BURUNDI',NULL,NULL,NULL,NULL,1),(25,'BJ','BENIN',NULL,NULL,NULL,NULL,1),(26,'BM','BERMUDA',NULL,NULL,NULL,NULL,1),(27,'BN','BRUNEI DARUSSALAM',NULL,NULL,NULL,NULL,1),(28,'BO','BOLIVIA',NULL,NULL,NULL,NULL,1),(29,'BR','BRAZIL',NULL,NULL,NULL,NULL,1),(30,'BS','BAHAMAS',NULL,NULL,NULL,NULL,1),(31,'BT','BHUTAN',NULL,NULL,NULL,NULL,1),(32,'BU','BURMA (MYANMAR)',NULL,NULL,NULL,NULL,1),(33,'BV','BOUVET ISLAND',NULL,NULL,NULL,NULL,1),(34,'BW','BOTSWANA',NULL,NULL,NULL,NULL,1),(35,'BY','BELARUS',NULL,NULL,NULL,NULL,1),(36,'BZ','BELIZE',NULL,NULL,NULL,NULL,1),(37,'CA','CANADA',NULL,NULL,NULL,NULL,1),(38,'CC','COCOS (KEELING) ISLANDS',NULL,NULL,NULL,NULL,1),(39,'CF','CENTRAL AFRICAN REPUBLIC',NULL,NULL,NULL,NULL,1),(40,'CG','CONGO',NULL,NULL,NULL,NULL,1),(41,'CH','SWITZERLAND',NULL,NULL,NULL,NULL,1),(42,'CI','COTE D’IVOIRE',NULL,NULL,NULL,NULL,1),(43,'CK','COOK ISLANDS',NULL,NULL,NULL,NULL,1),(44,'CL','CHILE',NULL,NULL,NULL,NULL,1),(45,'CM','CAMEROON',NULL,NULL,NULL,NULL,1),(46,'CN','CHINA',NULL,NULL,NULL,NULL,1),(47,'CO','COLOMBIA',NULL,NULL,NULL,NULL,1),(48,'CR','COSTA RICA',NULL,NULL,NULL,NULL,1),(49,'CS','CZECH AND SLOVAK',NULL,NULL,NULL,NULL,1),(50,'CU','CUBA',NULL,NULL,NULL,NULL,1),(51,'CV','CAPE VERDE',NULL,NULL,NULL,NULL,1),(52,'CX','CHRISTMAS ISLAND',NULL,NULL,NULL,NULL,1),(53,'CY','CYPRUS',NULL,NULL,NULL,NULL,1),(54,'CZ','CZECH REPUPLIC',NULL,NULL,NULL,NULL,1),(55,'DD','GERMAN DEMOCRATIC REPUBLIC',NULL,NULL,NULL,NULL,1),(56,'DE','GERMANY',NULL,NULL,NULL,NULL,1),(57,'DJ','DJBOUTI',NULL,NULL,NULL,NULL,1),(58,'DK','DENMARK',NULL,NULL,NULL,NULL,1),(59,'DM','DOMINICA',NULL,NULL,NULL,NULL,1),(60,'DO','DOMINICAN REPUBLIC',NULL,NULL,NULL,NULL,1),(61,'DZ','ALGERIA',NULL,NULL,'2020-03-12 16:56:29',NULL,2),(62,'EC','ECUADOR',NULL,NULL,NULL,NULL,1),(63,'EE','ESTONIA',NULL,NULL,NULL,NULL,1),(64,'EG','EGYPT',NULL,NULL,NULL,NULL,1),(65,'EH','WESTERN SAHARA',NULL,NULL,NULL,NULL,1),(66,'ER','ERITREA',NULL,NULL,NULL,NULL,1),(67,'ES','SPAIN AND CANARY ISLANDS',NULL,NULL,NULL,NULL,1),(68,'ET','ETHIOPIA',NULL,NULL,NULL,NULL,1),(69,'FI','FINLAND',NULL,NULL,NULL,NULL,1),(70,'FJ','FIJI',NULL,NULL,NULL,NULL,1),(71,'FK','FALKLAND ISLANDS',NULL,NULL,NULL,NULL,1),(72,'FM','MICRONESIA',NULL,NULL,NULL,NULL,1),(73,'FO','FAROE ISLANDS',NULL,NULL,NULL,NULL,1),(74,'FR','FRANCE',NULL,NULL,NULL,NULL,1),(75,'GA','GABON',NULL,NULL,NULL,NULL,1),(76,'GB','UNITED KINGDOM',NULL,NULL,NULL,NULL,1),(77,'GD','GRENADA',NULL,NULL,NULL,NULL,1),(78,'GE','GEORGIA',NULL,NULL,NULL,NULL,1),(79,'GF','FRENCH GUIANA',NULL,NULL,NULL,NULL,1),(80,'GH','GHANA',NULL,NULL,NULL,NULL,1),(81,'GI','GIBRALTAR',NULL,NULL,NULL,NULL,1),(82,'GL','GREENLAND',NULL,NULL,NULL,NULL,1),(83,'GM','GAMBIA',NULL,NULL,NULL,NULL,1),(84,'GN','GUINEA',NULL,NULL,NULL,NULL,1),(85,'GP','GUADELOUPE',NULL,NULL,NULL,NULL,1),(86,'GQ','EQUAORITAL GUINEA',NULL,NULL,NULL,NULL,1),(87,'GR','GREECE',NULL,NULL,NULL,NULL,1),(88,'GT','GUATEMALA',NULL,NULL,NULL,NULL,1),(89,'GU','GUAM',NULL,NULL,NULL,NULL,1),(90,'GW','GUINEA-BISSAU',NULL,NULL,NULL,NULL,1),(91,'GY','GUYANA',NULL,NULL,NULL,NULL,1),(92,'HK','HONG KONG',NULL,NULL,NULL,NULL,1),(93,'HM','HEARD AND MCDONALD',NULL,NULL,NULL,NULL,1),(94,'HN','HONDURAS',NULL,NULL,NULL,NULL,1),(95,'HR','CROATIA',NULL,NULL,NULL,NULL,1),(96,'HT','HAITI',NULL,NULL,NULL,NULL,1),(97,'HU','HUNGARY',NULL,NULL,NULL,NULL,1),(98,'ID','INDONESIA',NULL,NULL,NULL,NULL,1),(99,'IE','IRELAND',NULL,NULL,NULL,NULL,1),(100,'IL','ISRAEL',NULL,NULL,NULL,NULL,1),(101,'IN','INDIA',NULL,NULL,NULL,NULL,1),(102,'IO','BRITISH INDIAN OCEAN TERRITORY',NULL,NULL,NULL,NULL,1),(103,'IQ','IRAQ',NULL,NULL,NULL,NULL,1),(104,'IR','IRAN',NULL,NULL,NULL,NULL,1),(105,'IS','CELAND',NULL,NULL,NULL,NULL,1),(106,'IT','ITALY',NULL,NULL,NULL,NULL,1),(107,'JM','JAMAICA',NULL,NULL,NULL,NULL,1),(108,'JO','JORDAN',NULL,NULL,NULL,NULL,1),(109,'JP','JAPAN',NULL,NULL,NULL,NULL,1),(110,'KE','KENYA',NULL,NULL,NULL,NULL,1),(111,'KG','KYRGYZSTAN',NULL,NULL,NULL,NULL,1),(112,'KH','CAMBODIA',NULL,NULL,NULL,NULL,1),(113,'KI','KIRIBATI',NULL,NULL,NULL,NULL,1),(114,'KM','COMOROS',NULL,NULL,NULL,NULL,1),(115,'KN','SAINT KITTS AND NEVIS',NULL,NULL,NULL,NULL,1),(116,'KP','KOREA, DEMOCRATIC PEOPLE’S REPUBLIC',NULL,NULL,NULL,NULL,1),(117,'KR','KOREA, REPUBLIC OF',NULL,NULL,NULL,NULL,1),(118,'KW','KUWAIT',NULL,NULL,NULL,NULL,1),(119,'KY','CAYMAN ISLANDS',NULL,NULL,NULL,NULL,1),(120,'KZ','KAZAKHSTAN',NULL,NULL,NULL,NULL,1),(121,'LA','LAO PEOPLE’S DEMOCRATIC REPUBLIC',NULL,NULL,NULL,NULL,1),(122,'LB','LEBONAN',NULL,NULL,NULL,NULL,1),(123,'LC','SAINT LUCIA',NULL,NULL,NULL,NULL,1),(124,'LI','LIECTENSTEIN',NULL,NULL,NULL,NULL,1),(125,'LK','SRI LANKA',NULL,NULL,NULL,NULL,1),(126,'LR','LIBERIA',NULL,NULL,NULL,NULL,1),(127,'LS','LESOTHO',NULL,NULL,NULL,NULL,1),(128,'LT','LITHUANIA',NULL,NULL,NULL,NULL,1),(129,'LU','LUXEMBOURG',NULL,NULL,NULL,NULL,1),(130,'LV','LATVIA',NULL,NULL,NULL,NULL,1),(131,'LY','LLIBYAN ARAB JAMAHIRIYA',NULL,NULL,NULL,NULL,1),(132,'MA','MOROCCO',NULL,NULL,NULL,NULL,1),(133,'MC','MONACO',NULL,NULL,NULL,NULL,1),(134,'MD','MOLDOVA, REPUBLIC OF',NULL,NULL,NULL,NULL,1),(135,'MG','MADAGASCAR',NULL,NULL,NULL,NULL,1),(136,'MH','MARSHALL ISLANDS',NULL,NULL,NULL,NULL,1),(137,'MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',NULL,NULL,NULL,NULL,1),(138,'ML','MALI',NULL,NULL,NULL,NULL,1),(139,'MM','MYANMAR',NULL,NULL,NULL,NULL,1),(140,'MN','MONGOLIA',NULL,NULL,NULL,NULL,1),(141,'MO','MACAU',NULL,NULL,NULL,NULL,1),(142,'MP','NORTHERN MARIANA ISLANDS',NULL,NULL,NULL,NULL,1),(143,'MQ','MARTINIQUE',NULL,NULL,NULL,NULL,1),(144,'MR','MAURITANIA',NULL,NULL,NULL,NULL,1),(145,'MS','MONTSERRAT',NULL,NULL,NULL,NULL,1),(146,'MT','MALTA',NULL,NULL,NULL,NULL,1),(147,'MU','MAURITITUS',NULL,NULL,NULL,NULL,1),(148,'MV','MALDIVES',NULL,NULL,NULL,NULL,1),(149,'MW','MALAWI',NULL,NULL,NULL,NULL,1),(150,'MX','MEXICO',NULL,NULL,NULL,NULL,1),(151,'MY','MALAYSIA',NULL,NULL,NULL,NULL,1),(152,'MZ','MOZAMBIQUE',NULL,NULL,NULL,NULL,1),(153,'NA','NAMIBIA',NULL,NULL,NULL,NULL,1),(154,'NC','NEW CALEDONIA',NULL,NULL,NULL,NULL,1),(155,'NE','NIGER',NULL,NULL,NULL,NULL,1),(156,'NF','NORFOLK ISLAND',NULL,NULL,NULL,NULL,1),(157,'NG','NIGERIA',NULL,NULL,NULL,NULL,1),(158,'NI','NICARAGUA',NULL,NULL,NULL,NULL,1),(159,'NL','NETHERLANDS',NULL,NULL,NULL,NULL,1),(160,'NO','NORWAY',NULL,NULL,NULL,NULL,1),(161,'NP','NEPAL',NULL,NULL,NULL,NULL,1),(162,'NR','NAURU',NULL,NULL,NULL,NULL,1),(163,'NT','NEUTRAL ZONE',NULL,NULL,NULL,NULL,1),(164,'NU','NIUE',NULL,NULL,NULL,NULL,1),(165,'NZ','NEW ZEALAND',NULL,NULL,NULL,NULL,1),(166,'OM','OMAN',NULL,NULL,NULL,NULL,1),(167,'PA','PANAMA',NULL,NULL,NULL,NULL,1),(168,'PE','PERU',NULL,NULL,NULL,NULL,1),(169,'PF','FRENCH POLYNESIA',NULL,NULL,NULL,NULL,1),(170,'PG','PAPUA NEW GUINEA',NULL,NULL,NULL,NULL,1),(171,'PH','PHILIPPINES',NULL,NULL,NULL,NULL,1),(172,'PK','PAKISTAN',NULL,NULL,NULL,NULL,1),(173,'PL','POLAND',NULL,NULL,NULL,NULL,1),(174,'PM','ST. PIERRE AND MIQUELON',NULL,NULL,NULL,NULL,1),(175,'PN','PITCAIRA',NULL,NULL,NULL,NULL,1),(176,'PR','PUERTO RICO',NULL,NULL,NULL,NULL,1),(177,'PT','PORTUGAL',NULL,NULL,NULL,NULL,1),(178,'PW','PALAU',NULL,NULL,NULL,NULL,1),(179,'PY','PARAGUAY',NULL,NULL,NULL,NULL,1),(180,'QA','QATAR',NULL,NULL,NULL,NULL,1),(181,'RE','REUNION',NULL,NULL,NULL,NULL,1),(182,'RO','ROMANIA',NULL,NULL,NULL,NULL,1),(183,'RU','RUSSIAN FEDERATION',NULL,NULL,NULL,NULL,1),(184,'RW','RWANDA',NULL,NULL,NULL,NULL,1),(185,'SA','SAUDI ARABIA',NULL,NULL,NULL,NULL,1),(186,'SB','SOLOMON ISLANDS',NULL,NULL,NULL,NULL,1),(187,'SC','SEYCHELLES',NULL,NULL,NULL,NULL,1),(188,'SD','SUDAN',NULL,NULL,NULL,NULL,1),(189,'SE','SWEDEN',NULL,NULL,NULL,NULL,1),(190,'SG','SINGAPORE',NULL,NULL,NULL,NULL,1),(191,'SH','ST. HELENA',NULL,NULL,NULL,NULL,1),(192,'SI','SLOVENIA',NULL,NULL,NULL,NULL,1),(193,'SJ','SVALBARD AND JAN MAYEN ISLANDS',NULL,NULL,NULL,NULL,1),(194,'SK','SLOVAKIA',NULL,NULL,NULL,NULL,1),(195,'SL','SIERRA LEONE',NULL,NULL,NULL,NULL,1),(196,'SM','SAN MARINO',NULL,NULL,NULL,NULL,1),(197,'SN','SENEGAL',NULL,NULL,NULL,NULL,1),(198,'SO','SOMALIA',NULL,NULL,NULL,NULL,1),(199,'SR','SURINAM',NULL,NULL,NULL,NULL,1),(200,'ST','SAO TOME AND PRINCIPE',NULL,NULL,NULL,NULL,1),(201,'SU','RUSSIA',NULL,NULL,NULL,NULL,1),(202,'SV','EL SALVADOR',NULL,NULL,NULL,NULL,1),(203,'SY','SYRIAN ARAB REPUBLIC',NULL,NULL,NULL,NULL,1),(204,'SZ','SWAZILAND',NULL,NULL,NULL,NULL,1),(205,'TC','TURKS AND CAICOS ISLANDS',NULL,NULL,NULL,NULL,1),(206,'TD','CHAD',NULL,NULL,NULL,NULL,1),(207,'TF','FRENCH SOUTHERN TERRITORIES',NULL,NULL,NULL,NULL,1),(208,'TG','TOGO',NULL,NULL,NULL,NULL,1),(209,'TH','THAILAND',NULL,NULL,NULL,NULL,1),(210,'TJ','TAJISKISTAN',NULL,NULL,NULL,NULL,1),(211,'TK','TOKELAU',NULL,NULL,NULL,NULL,1),(212,'TM','TURMENISTAN',NULL,NULL,NULL,NULL,1),(213,'TN','TUNISIA',NULL,NULL,NULL,NULL,1),(214,'TO','TONGA',NULL,NULL,NULL,NULL,1),(215,'TP','EAST TIMOR',NULL,NULL,NULL,NULL,1),(216,'TR','TURKEY',NULL,NULL,NULL,NULL,1),(217,'TT','TRINIDAD AND TOBAGO',NULL,NULL,NULL,NULL,1),(218,'TV','TUVALU',NULL,NULL,NULL,NULL,1),(219,'TW','TAIWAN',NULL,NULL,NULL,NULL,1),(220,'TZ','TANZANIA, UNITED REPUBLIC OF',NULL,NULL,NULL,NULL,1),(221,'UA','UKRAINE',NULL,NULL,NULL,NULL,1),(222,'UG','UGANDA',NULL,NULL,NULL,NULL,1),(223,'UM','UNITED STATES MINOR',NULL,NULL,NULL,NULL,1),(224,'UN','VIET NAM',NULL,NULL,NULL,NULL,1),(225,'US','UNITED STATES',NULL,NULL,NULL,NULL,1),(226,'UY','URUGUAY',NULL,NULL,NULL,NULL,1),(227,'UZ','UZBEKISTAN',NULL,NULL,NULL,NULL,1),(228,'VA','HOLY SEE (VATICAN CITY STATE)',NULL,NULL,NULL,NULL,1),(229,'VC','SAINT VINCENT AND THE GRENADINES',NULL,NULL,NULL,NULL,1),(230,'VE','VENEZUELA',NULL,NULL,NULL,NULL,1),(231,'VG','VIRGIN ISLANDS (BRITISH)',NULL,NULL,NULL,NULL,1),(232,'VI','VIRGIN ISLANDS (U.S.)',NULL,NULL,NULL,NULL,1),(233,'VN','VIETNAM',NULL,NULL,NULL,NULL,1),(234,'VU','VANUATU',NULL,NULL,NULL,NULL,1),(235,'WF','WALLIS AND FUTUNA ISLANDS',NULL,NULL,NULL,NULL,1),(236,'WS','SAMOA',NULL,NULL,NULL,NULL,1),(237,'XU','RUSSIAN FEDERATION',NULL,NULL,NULL,NULL,1),(238,'YE','YEMEN',NULL,NULL,NULL,NULL,1),(239,'YT','MAYOTTE',NULL,NULL,NULL,NULL,1),(240,'YU','YUGOSLAVIA',NULL,NULL,NULL,NULL,1),(241,'ZA','SOUTH AFRICA',NULL,NULL,NULL,NULL,1),(242,'ZM','ZAMBIA',NULL,NULL,NULL,NULL,1),(243,'ZR','ZAIRIE',NULL,NULL,NULL,NULL,1),(244,'ZW','ZIMBABWE',NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `ao_countrylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_freightconfig`
--

DROP TABLE IF EXISTS `ao_freightconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_freightconfig` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ParmKey` varchar(20) DEFAULT NULL,
  `ParamDesc` longtext,
  `ParamValue` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_freightconfig`
--

LOCK TABLES `ao_freightconfig` WRITE;
/*!40000 ALTER TABLE `ao_freightconfig` DISABLE KEYS */;
INSERT INTO `ao_freightconfig` VALUES (1,'COMNAM','Company Name',''),(2,'COMADD','Company Address',''),(3,'COMTEL','Company Telephone',''),(4,'COMFAX','Company Fax','603-51247888'),(5,'COMEML','Company Email','general@asiaone.com.my'),(6,'COMURL','Company URL','http://www.asiaone.com.my'),(7,'COMDIR','Associate Director','Damien Tan'),(10,'COMREG','Company Registration',''),(11,'INVPFX','Invoice Prefix','201302'),(41,'TNC','Terms & Condition','<div>\r <div class=\"fr-view\">\r <p><strong>Coverage area&nbsp;</strong></p>\r <p>We cover areas within Klang Valley only.</p>\r <p>Please do contact us if your address is out of our coverage area and interested to place order.</p>\r <p>&nbsp;</p>\r <p><strong>Delivery time</strong></p>\r <p>2 - 3 working day from order date.</p>\r <p>Our working hours are Monday to Friday, 9am to 5pm. We are closed on Saturday, Sunday and Public Holiday.</p>\r <p>&nbsp;</p>\r <p><strong>Delivery charge</strong></p>\r <p>RM6.00 to RM15.00 based on location.</p>\r <p>&nbsp;</p>\r <p><strong>Contact details&nbsp;</strong></p>\r <p>Contact us through&nbsp;<strong>Whatsapp at +6012 311 1531</strong>&nbsp;or email&nbsp;<strong><a href=\"mailto:info@sfnoodles.com\" data-fr-linked=\"true\">info@sfnoodles.com</a>&nbsp;</strong>if you need further assistance.</p>\r </div>\r </div>'),(13,'ODRPFX','Order Prefix','ODR'),(15,'GSTID','Company GST ID','001826537472'),(38,'COLORPRIMARY','Primary Color','#3C2071'),(39,'COLORSECONDARY','Secondary Color','#00acee'),(40,'FAQ','FAQ','<p><strong>1. Will you deliver my order during MCO?</strong></p>\n<p>Yes, we have gotten approval from MITI to operate throughout the MCO. Our trucks are able to travel on the road to deliver to your doorsteps.</p>\n<p>As requested by the government authorities, all of our trucks are disinfected before and after each batch of deliveries. Also, our drivers are equipped with face mask and hand sanitizer, to ensure they are safe to perform the deliveries.</p>\n<p>&nbsp;</p>\n<p><strong>2. When will I receive my order and where do you deliver to?</strong></p>\n<p>We do&nbsp;<em><strong>2-3 days delivery</strong>&nbsp;</em>from Monday to Friday only. Orders made on Friday, Saturday and Sunday will be delivered on the following Monday. Please refer to&nbsp;<em>Terms and Conditions</em>&nbsp;page for more information.&nbsp;</p>'),(22,'COMLOGO','Company Logo Image','logo.png'),(23,'COMSHO','Company Short Name','MVKL'),(35,'BTBANKNAME','Bank Transfer Bank Name','Public Bank'),(36,'BTACCNO','Bank Transfer Account No','3176375112'),(37,'BTACCNAME','Bank Transfer Account Name','Soon Fatt Foods Sdn Bhd'),(31,'COMADD1','Address1','No 38, Jalan Anggerik Mokara 31/60, '),(32,'COMADD2','Address2','Kota Kemuning, 40460 Shah Alam,'),(33,'COMADD3','Address3','Selangor, Malaysia.'),(34,'WHATSAPP','Whatsapp No','0123111531'),(43,'TOTALROOMS','Total Rooms','2'),(44,'BOOKINGINTERVAL','Booking Interval','15'),(45,'BOOKINGOPENINGHOURS','Opening Hours','10:00:00'),(46,'BOOKINGCLOSINGHOURS','Closing Hours','21:00:00'),(47,'BOOKINGMINHOUR','Booking Hour','60');
/*!40000 ALTER TABLE `ao_freightconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_freightrunno`
--

DROP TABLE IF EXISTS `ao_freightrunno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_freightrunno` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Prefix` longtext,
  `LastNo` int DEFAULT NULL,
  `RunDesc` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_freightrunno`
--

LOCK TABLES `ao_freightrunno` WRITE;
/*!40000 ALTER TABLE `ao_freightrunno` DISABLE KEYS */;
INSERT INTO `ao_freightrunno` VALUES (1,'CUS',12071,'Customer'),(2,'QUA',6087,'Quotation'),(3,'201302',38573,'Invoice'),(4,'COS',0,'Costing Item'),(5,'ODR',104101,'Order'),(8,'HAWB',50,'HAWB'),(9,'JOB',58671,'Job No');
/*!40000 ALTER TABLE `ao_freightrunno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_invoicedetail`
--

DROP TABLE IF EXISTS `ao_invoicedetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_invoicedetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `InvoiceNo` varchar(255) DEFAULT NULL,
  `InvoiceID` int DEFAULT NULL,
  `ConsignmentNo` varchar(255) DEFAULT NULL,
  `ConsignmentID` int DEFAULT NULL,
  `ItemDesc` varchar(255) DEFAULT NULL,
  `AccountsCode` varchar(255) DEFAULT NULL,
  `Amount` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `InvoiceNo` (`InvoiceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_invoicedetail`
--

LOCK TABLES `ao_invoicedetail` WRITE;
/*!40000 ALTER TABLE `ao_invoicedetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_invoicedetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_invoiceheader`
--

DROP TABLE IF EXISTS `ao_invoiceheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_invoiceheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `InvoiceNo` varchar(100) NOT NULL,
  `ConsignmentNo` varchar(100) NOT NULL,
  `ConsignmentID` int DEFAULT NULL,
  `InvoiceAmount` decimal(18,2) NOT NULL,
  `InvoiceDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `InvoiceNo` (`InvoiceNo`),
  KEY `ConsignmentNo` (`ConsignmentNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_invoiceheader`
--

LOCK TABLES `ao_invoiceheader` WRITE;
/*!40000 ALTER TABLE `ao_invoiceheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_invoiceheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_leavelist`
--

DROP TABLE IF EXISTS `ao_leavelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_leavelist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `LeaveType` varchar(145) DEFAULT NULL,
  `FromDate` date DEFAULT NULL,
  `ToDate` date DEFAULT NULL,
  `NoOfDays` decimal(10,1) DEFAULT NULL,
  `Approved` smallint DEFAULT NULL,
  `LeaveDesc` varchar(245) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_leavelist`
--

LOCK TABLES `ao_leavelist` WRITE;
/*!40000 ALTER TABLE `ao_leavelist` DISABLE KEYS */;
INSERT INTO `ao_leavelist` VALUES (11,54,'Full Day','2020-09-01','2020-09-02',1.0,NULL,NULL,'admin','2020-09-19 07:54:39',NULL,'2020-09-19 07:54:39'),(12,54,'Full Day','2020-09-01','2020-09-25',-24.0,NULL,NULL,'admin','2020-09-19 09:03:22',NULL,'2020-09-19 09:03:22'),(13,54,'Full Day','2020-09-01','2020-09-08',0.0,NULL,NULL,'admin','2020-09-19 09:06:20',NULL,'2020-09-19 09:06:20'),(14,54,'Full Day','2020-09-01','2020-09-08',11.0,NULL,NULL,'admin','2020-09-19 09:07:12',NULL,'2020-09-19 09:07:12');
/*!40000 ALTER TABLE `ao_leavelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_loggedin`
--

DROP TABLE IF EXISTS `ao_loggedin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_loggedin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `DSN` varchar(45) DEFAULT NULL,
  `User` varchar(45) DEFAULT NULL,
  `LoggedDate` datetime DEFAULT NULL,
  `SessionID` varchar(230) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=551 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_loggedin`
--

LOCK TABLES `ao_loggedin` WRITE;
/*!40000 ALTER TABLE `ao_loggedin` DISABLE KEYS */;
INSERT INTO `ao_loggedin` VALUES (262,'asiaone_old7','54','2020-04-17 01:00:07','4Uj5hb9sJKhBMaD1w2Ac'),(305,'akishop','165','2020-05-13 15:47:48','A5fWxauHnHqJxCWLobUx'),(400,'soonfatt','174','2020-05-28 10:30:33','dUlFAVoYHMQxPLJisvEU'),(422,'shop','174','2020-07-05 11:41:07','jrlYcaKJ9xDvz23cJEg7'),(446,'spa1','174','2020-09-09 18:44:40','55M4G0SjewV49PEGhpA6'),(550,'spa2','182','2021-11-17 09:02:40','lPD6FQRnLBSQrOGl1S34');
/*!40000 ALTER TABLE `ao_loggedin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_orderdetail`
--

DROP TABLE IF EXISTS `ao_orderdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_orderdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `OrderID` int DEFAULT NULL,
  `OrderNo` varchar(10) DEFAULT NULL,
  `UserID` int DEFAULT NULL,
  `ShipperID` varchar(8) DEFAULT NULL,
  `ProductID` int DEFAULT NULL,
  `ProductNo` varchar(45) DEFAULT NULL,
  `OrderDate` date DEFAULT NULL,
  `RequiredDate` date DEFAULT NULL,
  `Freight` decimal(10,0) DEFAULT NULL,
  `SalesTax` decimal(10,0) DEFAULT NULL,
  `TimeStamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TransactStatus` varchar(25) DEFAULT NULL,
  `InvoiceAmount` decimal(10,2) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `Qty` int DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `PriceID` int DEFAULT NULL,
  `UnitPrice` decimal(10,2) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UserID_idx` (`UserID`),
  KEY `ShipperID_idx` (`ShipperID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_orderdetail`
--

LOCK TABLES `ao_orderdetail` WRITE;
/*!40000 ALTER TABLE `ao_orderdetail` DISABLE KEYS */;
INSERT INTO `ao_orderdetail` VALUES (1,1,'104068',174,NULL,34,NULL,NULL,NULL,NULL,NULL,'2020-11-23 03:10:55',NULL,294.00,NULL,3,'2020-11-23 03:10:55',38,98.00,'Completed','1'),(2,2,'104069',174,NULL,34,NULL,NULL,NULL,NULL,NULL,'2020-11-23 03:14:08',NULL,392.00,NULL,4,'2020-11-23 03:14:08',38,98.00,'Completed','1');
/*!40000 ALTER TABLE `ao_orderdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_orderheader`
--

DROP TABLE IF EXISTS `ao_orderheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_orderheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `OrderNo` varchar(10) DEFAULT NULL,
  `CustomerCode` varchar(255) DEFAULT NULL,
  `ZoneID` varchar(255) DEFAULT NULL,
  `NoOfCarton` varchar(255) DEFAULT NULL,
  `Remarks` text,
  `Status` varchar(255) DEFAULT NULL,
  `OrderBy` varchar(255) DEFAULT NULL,
  `OrderPhone` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(255) DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `StatusCode` varchar(45) DEFAULT NULL,
  `UserID` int DEFAULT NULL,
  `PaymentMethod` varchar(45) DEFAULT NULL,
  `DeliveryCharges` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `OrderNo` (`OrderNo`),
  KEY `CustomerCode` (`CustomerCode`),
  KEY `ZoneID` (`ZoneID`),
  KEY `zonestatus` (`ZoneID`,`Status`,`CreatedOn`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_orderheader`
--

LOCK TABLES `ao_orderheader` WRITE;
/*!40000 ALTER TABLE `ao_orderheader` DISABLE KEYS */;
INSERT INTO `ao_orderheader` VALUES (1,'104068',NULL,NULL,NULL,NULL,'Order Created',NULL,NULL,'01110340242','2020-11-23 03:10:55',NULL,NULL,'ORDERCREATED',174,'Cash',NULL),(2,'104069',NULL,NULL,NULL,NULL,'Order Created',NULL,NULL,'01110340242','2020-11-23 03:14:07',NULL,NULL,'ORDERCREATED',174,'Cash',NULL);
/*!40000 ALTER TABLE `ao_orderheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_orderstatus`
--

DROP TABLE IF EXISTS `ao_orderstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_orderstatus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `OrderID` varchar(255) NOT NULL,
  `Status` varchar(255) NOT NULL,
  `CreatedBy` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `StatusCode` varchar(45) DEFAULT NULL,
  `HideSelect` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `OrderID` (`OrderID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_orderstatus`
--

LOCK TABLES `ao_orderstatus` WRITE;
/*!40000 ALTER TABLE `ao_orderstatus` DISABLE KEYS */;
INSERT INTO `ao_orderstatus` VALUES (1,'7','Order Created','admin','2021-03-29 15:02:15','ORDERCREATED',NULL);
/*!40000 ALTER TABLE `ao_orderstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_paymentdetail`
--

DROP TABLE IF EXISTS `ao_paymentdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_paymentdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `DebtorCode` int DEFAULT NULL,
  `PaymentID` int DEFAULT NULL,
  `TransactionID` int DEFAULT NULL,
  `PaidAmount` decimal(13,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_paymentdetail`
--

LOCK TABLES `ao_paymentdetail` WRITE;
/*!40000 ALTER TABLE `ao_paymentdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_paymentdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_paymentlist`
--

DROP TABLE IF EXISTS `ao_paymentlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_paymentlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `DebtorCode` int DEFAULT NULL,
  `PaymentMode` varchar(145) DEFAULT NULL,
  `Amount` decimal(13,2) DEFAULT NULL,
  `PaymentDate` date DEFAULT NULL,
  `PaymentRef` varchar(45) DEFAULT NULL,
  `PaymentDesc` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_paymentlist`
--

LOCK TABLES `ao_paymentlist` WRITE;
/*!40000 ALTER TABLE `ao_paymentlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_paymentlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_paymenttermlist`
--

DROP TABLE IF EXISTS `ao_paymenttermlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_paymenttermlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `PaymentTermCode` varchar(255) NOT NULL,
  `PaymentTermDesc` varchar(255) DEFAULT NULL,
  `PaymentTermDays` int DEFAULT NULL,
  `Ordering` int DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `Paymenttermcode` (`PaymentTermCode`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_paymenttermlist`
--

LOCK TABLES `ao_paymenttermlist` WRITE;
/*!40000 ALTER TABLE `ao_paymenttermlist` DISABLE KEYS */;
INSERT INTO `ao_paymenttermlist` VALUES (1,'14D','14 Days',14,2),(2,'30D','30 Days',30,1),(3,'45D','45 Days',45,3),(4,'60D','60 Days',60,2),(5,'90D','90 Days',90,1),(6,'COD','C.O.D',0,1);
/*!40000 ALTER TABLE `ao_paymenttermlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_payrolldetail`
--

DROP TABLE IF EXISTS `ao_payrolldetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_payrolldetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `PayrollID` int DEFAULT NULL,
  `Title` varchar(245) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_payrolldetail`
--

LOCK TABLES `ao_payrolldetail` WRITE;
/*!40000 ALTER TABLE `ao_payrolldetail` DISABLE KEYS */;
INSERT INTO `ao_payrolldetail` VALUES (2,4,'abnnas',221.30,'admin','2020-09-22 19:12:34','admin','2020-09-22 19:19:08');
/*!40000 ALTER TABLE `ao_payrolldetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_payrollheader`
--

DROP TABLE IF EXISTS `ao_payrollheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_payrollheader` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `FromDate` date DEFAULT NULL,
  `ToDate` date DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_payrollheader`
--

LOCK TABLES `ao_payrollheader` WRITE;
/*!40000 ALTER TABLE `ao_payrollheader` DISABLE KEYS */;
INSERT INTO `ao_payrollheader` VALUES (1,183,'2020-09-01','2020-09-30','New',NULL,NULL,NULL,NULL),(2,183,'2020-09-01','2020-09-30','New','admin','2020-09-22 10:55:47',NULL,NULL),(3,183,'2020-10-01','2020-10-31','New','admin','2020-09-22 10:56:41','admin','2020-09-22 17:48:14'),(4,183,'2020-08-01','2020-08-31','New','admin','2020-09-22 17:50:40',NULL,NULL),(5,183,'2020-07-01','2020-07-31','New','admin','2020-09-22 17:50:53',NULL,NULL),(6,188,'2020-11-01','2020-11-30','New','admin','2020-11-04 03:19:42',NULL,NULL),(7,182,'2020-11-01','2020-11-30','New','admin','2020-11-04 15:57:01',NULL,NULL),(8,182,'2020-08-01','2020-11-30','New','liubao','2020-11-06 06:13:44',NULL,NULL);
/*!40000 ALTER TABLE `ao_payrollheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_personlist`
--

DROP TABLE IF EXISTS `ao_personlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_personlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ClientID` int DEFAULT NULL,
  `FirstName` varchar(145) DEFAULT NULL,
  `LastName` varchar(145) DEFAULT NULL,
  `Email` varchar(245) DEFAULT NULL,
  `Mobile` varchar(45) DEFAULT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `PreAlert` varchar(2) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `UpdatedOn` date DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `Active` varchar(1) DEFAULT NULL,
  `Username` varchar(45) DEFAULT NULL,
  `Password` varchar(145) DEFAULT NULL,
  `PersonInCharge` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ClientID` (`ClientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_personlist`
--

LOCK TABLES `ao_personlist` WRITE;
/*!40000 ALTER TABLE `ao_personlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_personlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_postcode`
--

DROP TABLE IF EXISTS `ao_postcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_postcode` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Town` varchar(245) DEFAULT NULL,
  `State` varchar(145) DEFAULT NULL,
  `PostCode` varchar(10) DEFAULT NULL,
  `Price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_postcode`
--

LOCK TABLES `ao_postcode` WRITE;
/*!40000 ALTER TABLE `ao_postcode` DISABLE KEYS */;
INSERT INTO `ao_postcode` VALUES (4,'Petaling Jaya','Selangor Darul Ehsan','46150',15.00),(5,'Petaling Jaya','Selangor Darul Ehsan','46200',15.00),(6,'Petaling Jaya','Selangor Darul Ehsan','46300',15.00),(7,'Petaling Jaya','Selangor Darul Ehsan','46350',15.00),(8,'Petaling Jaya','Selangor Darul Ehsan','46400',15.00),(9,'Petaling Jaya','Selangor Darul Ehsan','47300',15.00),(10,'Petaling Jaya','Selangor Darul Ehsan','47301',15.00),(11,'Petaling Jaya','Selangor Darul Ehsan','47400',15.00),(12,'Petaling Jaya','Selangor Darul Ehsan','47800',15.00),(13,'Petaling Jaya','Selangor Darul Ehsan','47810',15.00),(14,'Petaling Jaya','Selangor Darul Ehsan','47820',15.00),(15,'Puchong','Selangor Darul Ehsan','47100',6.00),(16,'Puchong','Selangor Darul Ehsan','47110',10.00),(17,'Puchong','Selangor Darul Ehsan','47120',10.00),(18,'Puchong','Selangor Darul Ehsan','47130',10.00),(19,'Puchong','Selangor Darul Ehsan','47140',6.00),(20,'Puchong','Selangor Darul Ehsan','47150',6.00),(21,'Puchong','Selangor Darul Ehsan','47160',6.00),(22,'Puchong','Selangor Darul Ehsan','47170',6.00),(23,'Puchong','Selangor Darul Ehsan','47180',8.00),(24,'Puchong','Selangor Darul Ehsan','47190',6.00),(25,'Kuala Lumpur','WP Kuala Lumpur','50088',15.00),(26,'Kuala Lumpur','WP Kuala Lumpur','50100',15.00),(27,'Kuala Lumpur','WP Kuala Lumpur','50250',15.00),(28,'Kuala Lumpur','WP Kuala Lumpur','50300',15.00),(29,'Kuala Lumpur','WP Kuala Lumpur','50350',15.00),(30,'Kuala Lumpur','WP Kuala Lumpur','50400',15.00),(31,'Kuala Lumpur','WP Kuala Lumpur','50450',15.00),(32,'Kuala Lumpur','WP Kuala Lumpur','50460',15.00),(33,'Kuala Lumpur','WP Kuala Lumpur','50470',15.00),(34,'Kuala Lumpur','WP Kuala Lumpur','50480',15.00),(35,'Kuala Lumpur','WP Kuala Lumpur','50490',15.00),(36,'Kuala Lumpur','WP Kuala Lumpur','50500',15.00),(37,'Kuala Lumpur','WP Kuala Lumpur','50586',15.00),(38,'Kuala Lumpur','WP Kuala Lumpur','50603',15.00),(39,'Kuala Lumpur','WP Kuala Lumpur','51000',15.00),(40,'Kuala Lumpur','WP Kuala Lumpur','51100',15.00),(41,'Kuala Lumpur','WP Kuala Lumpur','51200',15.00),(42,'Kuala Lumpur','WP Kuala Lumpur','52000',15.00),(43,'Kuala Lumpur','WP Kuala Lumpur','52100',15.00),(44,'Kuala Lumpur','WP Kuala Lumpur','52200',15.00),(45,'Kuala Lumpur','WP Kuala Lumpur','53000',15.00),(46,'Kuala Lumpur','WP Kuala Lumpur','53100',15.00),(47,'Kuala Lumpur','WP Kuala Lumpur','53200',15.00),(48,'Kuala Lumpur','WP Kuala Lumpur','53300',15.00),(49,'Kuala Lumpur','WP Kuala Lumpur','54000',15.00),(50,'Kuala Lumpur','WP Kuala Lumpur','54100',15.00),(51,'Kuala Lumpur','WP Kuala Lumpur','54200',15.00),(52,'Kuala Lumpur','WP Kuala Lumpur','55000',15.00),(53,'Kuala Lumpur','WP Kuala Lumpur','55100',15.00),(54,'Kuala Lumpur','WP Kuala Lumpur','55200',15.00),(55,'Kuala Lumpur','WP Kuala Lumpur','55300',15.00),(56,'Kuala Lumpur','WP Kuala Lumpur','56000',15.00),(57,'Kuala Lumpur','WP Kuala Lumpur','56100',15.00),(58,'Kuala Lumpur','WP Kuala Lumpur','57000',15.00),(59,'Kuala Lumpur','WP Kuala Lumpur','57100',15.00),(78,'Shah Alam','Selangor Darul Ehsan','40000',15.00),(79,'Shah Alam','Selangor Darul Ehsan','40100',15.00),(80,'Shah Alam','Selangor Darul Ehsan','40150',15.00),(81,'Shah Alam','Selangor Darul Ehsan','40160',15.00),(82,'Shah Alam','Selangor Darul Ehsan','40170',15.00),(83,'Shah Alam','Selangor Darul Ehsan','40200',15.00),(84,'Shah Alam','Selangor Darul Ehsan','40300',15.00),(85,'Shah Alam','Selangor Darul Ehsan','40400',15.00),(86,'Shah Alam','Selangor Darul Ehsan','40450',15.00),(87,'Shah Alam','Selangor Darul Ehsan','40460',15.00),(88,'Shah Alam','Selangor Darul Ehsan','40470',15.00),(89,'Subang Jaya','Selangor Darul Ehsan','47200',15.00),(90,'Subang Jaya','Selangor Darul Ehsan','47500',10.00),(91,'Subang Jaya','Selangor Darul Ehsan','47600',10.00),(92,'Subang Jaya','Selangor Darul Ehsan','47610',10.00),(93,'Subang Jaya','Selangor Darul Ehsan','47620',10.00),(94,'Subang Jaya','Selangor Darul Ehsan','47630',10.00),(95,'Subang Jaya','Selangor Darul Ehsan','47640',10.00),(96,'Subang Jaya','Selangor Darul Ehsan','47650',10.00),(99,'Seri Kembangan','Selangor','43300',15.00),(101,'Putrajaya','Wilayah Persekutuan','62000',15.00),(103,'Cyberjaya','Selangor','63000',15.00),(104,'Cheras','Selangor','43200',15.00),(105,'Dengkil','Selangor','43800',15.00),(106,'Kuala Lumpur','Wilayah Persekutuan','59000',15.00),(107,'Kuala Lumpur','Wilayah Persekutuan','59200',15.00),(108,'Kuala Lumpur','Wilayah Persekutuan','59100',15.00),(109,'Kuala Lumpur','Wilayah Persekutuan','58200',15.00),(110,'Kuala Lumpur','Wilayah Persekutuan','50490',15.00),(111,'Kuala Lumpur','Wilayah Persekutuan','50470',15.00),(112,'Kuala Lumpur','Wilayah Persekutuan','58200',15.00);
/*!40000 ALTER TABLE `ao_postcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_pricelist`
--

DROP TABLE IF EXISTS `ao_pricelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_pricelist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ProductID` int DEFAULT NULL,
  `Unit` int DEFAULT NULL,
  `UnitPrice` decimal(10,2) DEFAULT NULL,
  `Uom` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(20) DEFAULT NULL,
  `UnitPriceMember` decimal(10,2) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  `ServiceTimes` varchar(45) DEFAULT NULL,
  `NoDelete` smallint DEFAULT NULL,
  `SingleItem` smallint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_pricelist`
--

LOCK TABLES `ao_pricelist` WRITE;
/*!40000 ALTER TABLE `ao_pricelist` DISABLE KEYS */;
INSERT INTO `ao_pricelist` VALUES (7,2,1,10.50,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,3,1,10.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,4,1,13.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,5,1,5.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,6,1,12.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,7,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,12,1,4.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,13,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,14,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,15,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,16,20,10.00,'piece',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,17,1,3.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,18,1,10.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,19,1,7.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,1,1,98.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,20,1,10.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,21,1,5.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,22,1,10.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,24,1,21.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,30,1,112.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,31,1,12.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,25,1,100.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,33,1,100.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(38,34,1,98.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,35,1,2688.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,36,1,2000.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(41,37,1,128.00,'each',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(42,38,1,110.00,'One Time Pass',NULL,NULL,80.00,'Service','1',NULL,1),(43,38,1,1000.00,'Package A - 14 Time Pass',NULL,NULL,NULL,'Service','14',1,1),(44,38,1,2000.00,'Package B - 26 Time Pass',NULL,NULL,NULL,'Service','26',1,1),(45,38,1,3000.00,'Package C - 40 Times Pass',NULL,NULL,NULL,'Service','40',1,1),(47,38,1,4000.00,'Package D - 54 Times Pass',NULL,NULL,NULL,'Service','54',1,1);
/*!40000 ALTER TABLE `ao_pricelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_productlist`
--

DROP TABLE IF EXISTS `ao_productlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_productlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ProductID` varchar(8) DEFAULT NULL,
  `DepartmentID` varchar(8) DEFAULT NULL,
  `Category` int DEFAULT NULL,
  `IDSKU` varchar(8) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `Quantity` int DEFAULT NULL,
  `UnitPrice` decimal(10,2) DEFAULT NULL,
  `Ranking` int DEFAULT NULL,
  `ProductDesc` text,
  `UnitsInStock` int DEFAULT '0',
  `UnitsInOrder` int DEFAULT '0',
  `Picture` blob,
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `Ordering` int DEFAULT NULL,
  `OriginalURL` text,
  `PrettyUrl` varchar(255) DEFAULT NULL,
  `DoneScrap` smallint DEFAULT NULL,
  `Youtube` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_productlist`
--

LOCK TABLES `ao_productlist` WRITE;
/*!40000 ALTER TABLE `ao_productlist` DISABLE KEYS */;
INSERT INTO `ao_productlist` VALUES (34,NULL,NULL,1,NULL,'SB96 - Health Series',NULL,NULL,NULL,'<p>商品详情</p>\n<p>SB96 拥有 3 大功效</p>\n<p>1. 杀菌消毒</p>\n<p>2. 消炎修复</p>\n<p>3. 消除疲劳</p>\n<p>► 完全无酒精 &bull; 无氯&nbsp;&bull;&nbsp;无杀菌剂 &bull; 无色素 &bull;&nbsp;无化学防腐剂</p>\n<p>► 完全是天然的成份哦!</p>\n<p>只需要RM98 ( SB96 100ml ) 就能把健康带回家！SB96 让你用得开心！ 用得安心！</p>\n<p>SB96 SIBEH GAOLAT Powerful Sterilizer During COVID - 19</p>\n<p>►Experimental report proven effectiveness up to 99.99%</p>\n<p>►SB96 3 Effective Uses</p>\n<p>1. Sterilization</p>\n<p>2. Decrease inflammation</p>\n<p>3. Reduce Tiredness</p>\n<p>Nature and health are our focus and needs in our products. It is the route we pursue towards a pure and healthy lifestyle and attitude. An inspiration from the mother nature, has been developed to now known as Light Activated Energy, which has the ability to improve our blood circulation. SB96 has 4 unique elements present, which gives SB96 the ability to improve metabolism, the healing of wounds, the ability to kill bacteria and many more. Experience 4.5 billion years of light together with the mother nature. Energy, harmony as well as prosperity, all given in one. SB96 wholeheartedly protects your health.</p>',10000000,0,NULL,NULL,NULL,1,NULL,'sb96-health-series',NULL,'<iframe width=\"100%\" height=\"220\" src=\"https://www.youtube.com/embed/h3s05mEUAYs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowFullScreen=\"allowFullScreen\"></iframe>'),(35,NULL,NULL,1,NULL,'WTE - Beauty Series',NULL,NULL,NULL,'<p>快速，有效，安全清洗！</p>\n<p>EASY, QUICK YET SAFE FOR CLEANING!</p>\n<p>In this era of digital technologies, EMR pollution is widespread. Affecting not just human beings but all life forms on Earth.</p>\n<p>Water, the essential needs for survival, is not only polluted with chemical waste. In fact, it is mainly polluted by EMR. More than half of the world\'s population consumes water of big water molecules resulting in increasing diseases caused by metabolic problems.</p>',10000000,0,NULL,NULL,NULL,3,NULL,'wte-beauty-series',NULL,'<iframe width=\"100%\" height=\"220\" src=\"https://www.youtube.com/embed/JegUBlosVN0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowFullScreen=\"allowFullScreen\"></iframe>'),(36,NULL,NULL,1,NULL,'LAE - Beauty Series',NULL,NULL,NULL,'<div class=\"s2\">\n<div class=\"full-desc ckeditor\">\n<p>快速，有效，安全清洗！</p>\n<p>EASY, QUICK YET SAFE FOR CLEANING!</p>\n<p>In this era of digital technologies, EMR pollution is widespread. Affecting not just human beings but all life forms on Earth.</p>\n<p>Water, the essential needs for survival, is not only polluted with chemical waste. In fact, it is mainly polluted by EMR. More than half of the world\'s population consumes water of big water molecules resulting in increasing diseases caused by metabolic problems.</p>\n</div>\n</div>',10000000,0,NULL,NULL,NULL,4,NULL,'lae-beauty-series',NULL,NULL),(37,NULL,NULL,1,NULL,'Ezurose - Beauty Series',NULL,NULL,NULL,'<p>Experience the finest skin care product, specially developed based on our core technology LAE Light Activated Energy.<br /><br />A beauty secret finally revealed. The world\'s first beauty and healthcare 2-in-1 product.<br />6 substantial efficacies and 4 unique elements are all you need to instantly restores skin elasticity.<br /><br />ezurose, a rose hidden within the light</p>\n<p>&nbsp;</p>\n<p>ezurose 936光的秘密，美颜大公开！</p>\n<p><strong>世界首创美容与保健双效合一的护肤产品</strong>，拥有&nbsp;<strong>LAE育成光能&nbsp;</strong>技术，<strong>天然成分&nbsp;</strong>和&nbsp;<strong>淡淡的玫瑰香</strong>。她就是女人的最佳选择！</p>\n<p><strong>6大功效&nbsp;</strong>和&nbsp;<strong>4大独特研发配方&nbsp;</strong>就是您恢复肌肤弹性的秘诀</p>\n<p>随身随地带着一瓶ezurose，让她的玫瑰香 散发出您的魅力</p>',10000000,0,NULL,NULL,NULL,2,NULL,'ezurose-beauty-series',NULL,'<iframe width=\"100%\" height=\"220\" src=\"https://www.youtube.com/embed/Oai6nPGX8-I\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowFullScreen=\"allowFullScreen\"></iframe>'),(38,NULL,NULL,2,NULL,'936 LAE Energy Room Treatment',NULL,NULL,NULL,'<p>商品详情</p>\n<p>SB96 拥有 3 大功效</p>\n<p>1. 杀菌消毒</p>\n<p>2. 消炎修复</p>\n<p>3. 消除疲劳</p>\n<p>► 完全无酒精 &bull; 无氯&nbsp;&bull;&nbsp;无杀菌剂 &bull; 无色素 &bull;&nbsp;无化学防腐剂</p>\n<p>► 完全是天然的成份哦!</p>\n<p>只需要RM98 ( SB96 100ml ) 就能把健康带回家！SB96 让你用得开心！ 用得安心！</p>\n<p>SB96 SIBEH GAOLAT Powerful Sterilizer During COVID - 19</p>\n<p>►Experimental report proven effectiveness up to 99.99%</p>\n<p>►SB96 3 Effective Uses</p>\n<p>1. Sterilization</p>\n<p>2. Decrease inflammation</p>\n<p>3. Reduce Tiredness</p>\n<p>Nature and health are our focus and needs in our products. It is the route we pursue towards a pure and healthy lifestyle and attitude. An inspiration from the mother nature, has been developed to now known as Light Activated Energy, which has the ability to improve our blood circulation. SB96 has 4 unique elements present, which gives SB96 the ability to improve metabolism, the healing of wounds, the ability to kill bacteria and many more. Experience 4.5 billion years of light together with the mother nature. Energy, harmony as well as prosperity, all given in one. SB96 wholeheartedly protects your health.</p>',1,0,NULL,NULL,NULL,1,NULL,'936-lae-energy-room-treatment',NULL,NULL);
/*!40000 ALTER TABLE `ao_productlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_puchcarddetail`
--

DROP TABLE IF EXISTS `ao_puchcarddetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_puchcarddetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `intime` varchar(145) DEFAULT NULL,
  `outtime` varchar(145) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `Username` varchar(45) DEFAULT NULL,
  `PunchType` varchar(45) DEFAULT NULL,
  `PunchDate` datetime DEFAULT NULL,
  `Token` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_puchcarddetail`
--

LOCK TABLES `ao_puchcarddetail` WRITE;
/*!40000 ALTER TABLE `ao_puchcarddetail` DISABLE KEYS */;
INSERT INTO `ao_puchcarddetail` VALUES (16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkin','2021-03-30 17:37:01','04caae48df968e2148b5019085084a282a381bd98f54202b76027d7f38426a9d0a3ac12bc0e47806b70abcdc0d99f9cf2e3cde3989b8ff4eac1d00bc4e09c4ed'),(17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkout','2020-09-25 16:40:21','7669b56de4935ddf86d58953123f4fa7417350c3d2d002560dfb06ed4a54a21328e76bcc6be32515f90ceb24c156fbe746ed195870749bad4279bdfc43b0c541'),(18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkin','2020-09-25 16:46:25','7a43c3d042b3bc1eabfcdbc60709b66baf924a1a935e190087e6c53075396cd47f442818e19e0498bf0c9187ca9b751c7fd8ce4d6a04e06693b5b0a4e16892fd'),(19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkin','2020-09-25 16:47:03','d434f6a4231b85944620b7618c4f41454b80c6375004e841e56f02e8c3471a5cac3dc39d7b85cf515825d4f382c773771e0b0b880fee4f67fc826302a1e8316e'),(20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','Checkout','2020-09-25 16:47:37','fe92a7d3eb8f229c1ed48b4a9ca988fc79750056e86af74c81e87ad2dc9069e3cde97ca89abcc7e1f5033d12ac3ec44a23ad1631a1f6c1b25d035b69a02aefc2'),(21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'liubao','Checkout','2020-09-25 17:13:41','9e4677eb14924731b364727efe63905ae8700ef5d5e0257da50801eca591aeb8c88c38b0845c4077c61d7d265edfee2703c40b1b1cd84cfe5e97c932007f9eba'),(22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'liubao','Checkout','2020-09-25 17:34:00','e680f10d4894397670ec0ad47d1edae50a72c8801ebbc33997032b410e7c0c8d8db952446eeb908d358e45c60803bd3899cc2642a15b61363ca4b54abeb5d235'),(23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'liubao','Checkin','2020-09-25 17:35:06','5975ecf47facbad71576c5498c84c88a9c373357da3db1949da4c37e183c8a80dbb83d706146546d792e1a5736c6d184d017b1ab1bd7ff5cb39a57db509dc55d'),(24,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'929ba66a112e7f045149b0bd338a38f8330cbaf9bfa4955d0947ec14a70aa393180644ec679513c17f66910792cf383d5f8b35384d42648017197d3a5b94e680');
/*!40000 ALTER TABLE `ao_puchcarddetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_staffdetail`
--

DROP TABLE IF EXISTS `ao_staffdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_staffdetail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `Email` varchar(245) DEFAULT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `FirstName` varchar(245) DEFAULT NULL,
  `LastName` varchar(245) DEFAULT NULL,
  `Address1` varchar(245) DEFAULT NULL,
  `Address2` varchar(245) DEFAULT NULL,
  `City` varchar(245) DEFAULT NULL,
  `Postcode` varchar(20) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `LastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_staffdetail`
--

LOCK TABLES `ao_staffdetail` WRITE;
/*!40000 ALTER TABLE `ao_staffdetail` DISABLE KEYS */;
INSERT INTO `ao_staffdetail` VALUES (39,179,'akilonx@gmail.com','01110340242','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-21 09:55:07','2020-09-21 09:55:07'),(40,180,'akilonx@gmail.com','01110340242','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-21 10:18:00','2020-09-21 10:18:00'),(41,181,'akilonx@gmail.com','01110340242','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-21 10:19:20','2020-09-21 10:19:20'),(42,182,'akilonx@gmail.com','01110340242','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-21 11:18:13','2020-09-21 11:18:13'),(44,185,'akilonx@gmail.com','1111111111','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor Darul Ehsan','Malaysia','2020-09-29 02:29:06','2020-09-29 02:29:06'),(45,187,'akilonx@gmail.com','234234234','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor','Malaysia','2020-11-04 03:14:39','2020-11-04 03:14:39'),(46,188,'akilonx@gmail.com','2323','Akilon','Krishnan','A-3-8, Impian Heights Apartment','Off Jalan Pipit','Puchong','47100','Selangor','Malaysia','2020-11-04 03:16:28','2020-11-04 03:16:28');
/*!40000 ALTER TABLE `ao_staffdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_staffgrouplist`
--

DROP TABLE IF EXISTS `ao_staffgrouplist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_staffgrouplist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `GroupName` varchar(255) DEFAULT NULL,
  `GroupID` varchar(255) DEFAULT NULL,
  `StartPage` varchar(255) DEFAULT NULL,
  `PageDesc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_staffgrouplist`
--

LOCK TABLES `ao_staffgrouplist` WRITE;
/*!40000 ALTER TABLE `ao_staffgrouplist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ao_staffgrouplist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_stafflist`
--

DROP TABLE IF EXISTS `ao_stafflist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_stafflist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `StaffID` longtext,
  `StaffName` longtext,
  `Designation` longtext,
  `Department` longtext,
  `Email` longtext,
  `Password` longtext,
  `Active` varchar(1) DEFAULT NULL,
  `CreateOn` datetime DEFAULT NULL,
  `CreateBy` longtext,
  `LastModified` datetime DEFAULT NULL,
  `ModifiedBy` longtext,
  `ContactNo` longtext,
  `Role` varchar(15) DEFAULT NULL,
  `Password2` varchar(145) DEFAULT NULL,
  `AgentCode` varchar(45) DEFAULT NULL,
  `Member` smallint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=190 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_stafflist`
--

LOCK TABLES `ao_stafflist` WRITE;
/*!40000 ALTER TABLE `ao_stafflist` DISABLE KEYS */;
INSERT INTO `ao_stafflist` VALUES (37,'akilon','Akilon','Sales Exec','Supervisor','akilon@blackforestcreations.com','f0849d6bc21c233f6e73fc3a13dd866e:74GqnEXZvTVQHYMlCAY663jlAZ6OzoAh','','2013-12-24 05:12:27','asiaone-admin','2014-01-02 02:40:36','AKILON','011-123802242',NULL,'akilon',NULL,NULL),(54,'admin','Administrator','Administrator','Management','admin@sfnoodles.com.my','9dcf18b675d6de976bf3e2298bc2d147:sE4Wv6cdEFgRdq1Iw6g2RdTYdZk693Fz','','2014-01-06 17:12:21','Akilon','2014-10-27 15:10:39','Akilon','012-3111531',NULL,'password','',NULL),(175,'01110390242',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'password',NULL,NULL),(176,'0162221431',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'88888888',NULL,NULL),(174,'01110340242',NULL,NULL,'Management',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'password',NULL,NULL),(172,'0123707774',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'akilon@321',NULL,NULL),(177,'0123053761',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'494104',NULL,NULL),(178,'0123111531',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sp2304329$P',NULL,NULL),(179,'dahong',NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-21 09:55:07','admin',NULL,NULL,NULL,NULL,'dahong',NULL,NULL),(180,'hongpao',NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-21 10:18:00','admin',NULL,NULL,NULL,NULL,'hongpao',NULL,NULL),(181,'jinjun',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-09-21 10:19:20','admin',NULL,NULL,NULL,NULL,'jinjun',NULL,NULL),(182,'liubao',NULL,NULL,'Management',NULL,NULL,NULL,'2020-09-21 11:18:12','admin',NULL,NULL,NULL,NULL,'liubao',NULL,NULL),(185,'teststaff',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-09-29 02:29:06','liubao',NULL,NULL,NULL,NULL,'teststaff123',NULL,NULL),(186,'attendance',NULL,NULL,'Attendance',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'attendance',NULL,NULL),(187,'asdfasdf',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-11-04 03:14:39','admin',NULL,NULL,NULL,NULL,'',NULL,NULL),(188,'asdfasdf1',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-11-04 03:16:27','admin',NULL,NULL,NULL,NULL,'abc123',NULL,NULL),(189,'sdasda',NULL,NULL,'Staff',NULL,NULL,NULL,'2020-11-04 03:19:07','admin',NULL,NULL,NULL,NULL,'abc123',NULL,NULL);
/*!40000 ALTER TABLE `ao_stafflist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_statuslist`
--

DROP TABLE IF EXISTS `ao_statuslist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_statuslist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `StatusName` varchar(45) DEFAULT NULL,
  `Ordering` smallint DEFAULT NULL,
  `StatusCode` varchar(45) DEFAULT NULL,
  `HideSelect` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_statuslist`
--

LOCK TABLES `ao_statuslist` WRITE;
/*!40000 ALTER TABLE `ao_statuslist` DISABLE KEYS */;
INSERT INTO `ao_statuslist` VALUES (1,'Assigned',1,'ASSIGNED','Yes'),(2,'Pending Pickup',2,'PENDINGPICKUP',NULL),(3,'Miss Pickup',3,'MISSPICKUP',NULL),(4,'Out Of Area',4,'OUTOFAREA',NULL),(5,'Wrong Info',5,'WRONGINFO',NULL),(6,'Parcel Not Ready',6,'PARCELNOTREADY',NULL),(7,'Closed',7,'CLOSED',NULL),(8,'Completed',8,'COMPLETED',NULL),(9,'Order Created',9,'ORDERCREATED','Yes'),(10,'Order Confirmed',10,'ORDERCONFIRMED','Yes');
/*!40000 ALTER TABLE `ao_statuslist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_stocklist`
--

DROP TABLE IF EXISTS `ao_stocklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_stocklist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `StockTakeID` int DEFAULT NULL,
  `OrderID` int DEFAULT NULL,
  `ProductID` varchar(20) DEFAULT NULL,
  `Title` varchar(245) DEFAULT NULL,
  `Movement` varchar(10) DEFAULT NULL,
  `FarmCode` varchar(20) DEFAULT NULL,
  `Qty` decimal(10,2) DEFAULT NULL,
  `Bags` decimal(10,2) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stocktakeid_idx` (`StockTakeID`),
  CONSTRAINT `stocktakeid` FOREIGN KEY (`StockTakeID`) REFERENCES `ao_stocktakelist` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_stocklist`
--

LOCK TABLES `ao_stocklist` WRITE;
/*!40000 ALTER TABLE `ao_stocklist` DISABLE KEYS */;
INSERT INTO `ao_stocklist` VALUES (1,39,NULL,'34',NULL,NULL,NULL,10.00,NULL,'liubao',NULL,NULL,NULL),(2,39,NULL,'37',NULL,NULL,NULL,10.00,NULL,'liubao',NULL,NULL,NULL),(3,40,NULL,'34',NULL,NULL,NULL,10.00,NULL,'liubao',NULL,NULL,NULL),(4,40,NULL,'37',NULL,NULL,NULL,10.00,NULL,'liubao',NULL,NULL,NULL),(5,NULL,10,'34','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 08:47:53',NULL,NULL),(6,NULL,5,'34','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 08:48:11',NULL,NULL),(7,NULL,5,'37','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 08:48:11',NULL,NULL),(8,NULL,8,'34','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 08:49:13',NULL,NULL),(9,NULL,2,'34','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 09:38:14',NULL,NULL),(10,NULL,4,'34','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 09:45:30',NULL,NULL),(11,41,NULL,'36',NULL,NULL,NULL,10.00,NULL,'liubao',NULL,NULL,NULL),(12,42,NULL,'36',NULL,NULL,NULL,10.00,NULL,'liubao',NULL,NULL,NULL),(13,NULL,6,'34','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 10:08:28',NULL,NULL),(14,NULL,6,'36','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 10:08:28',NULL,NULL),(15,NULL,7,'34','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 10:08:38',NULL,NULL),(16,NULL,9,'34','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 10:31:39',NULL,NULL),(17,NULL,10,'36','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-06 10:51:04',NULL,NULL),(18,43,NULL,'34',NULL,NULL,NULL,1.00,NULL,'liubao',NULL,NULL,NULL),(19,43,NULL,'37',NULL,NULL,NULL,1.00,NULL,'liubao',NULL,NULL,NULL),(20,NULL,14,'37','Stock Out','Out',NULL,20.00,NULL,'liubao','2021-04-06 20:16:30',NULL,NULL),(21,NULL,11,'34','Stock Out','Out',NULL,1.00,NULL,'admin','2021-04-17 07:29:28',NULL,NULL),(22,NULL,10,'34','Stock Out','Out',NULL,3.00,NULL,'liubao','2021-04-17 07:30:41',NULL,NULL),(23,NULL,14,'34','Stock Out','Out',NULL,1.00,NULL,'liubao','2021-04-17 07:41:02',NULL,NULL);
/*!40000 ALTER TABLE `ao_stocklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_stocktakelist`
--

DROP TABLE IF EXISTS `ao_stocktakelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_stocktakelist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `StockTakeDate` date DEFAULT NULL,
  `TransactionType` varchar(10) DEFAULT NULL,
  `LocationID` varchar(20) DEFAULT NULL,
  `Qty` decimal(10,2) DEFAULT NULL,
  `Bags` decimal(10,2) DEFAULT NULL,
  `KGToBags` decimal(10,2) DEFAULT NULL,
  `Reason` varchar(20) DEFAULT NULL,
  `Remarks` text,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_stocktakelist`
--

LOCK TABLES `ao_stocktakelist` WRITE;
/*!40000 ALTER TABLE `ao_stocktakelist` DISABLE KEYS */;
INSERT INTO `ao_stocktakelist` VALUES (39,'2021-04-05','ADD','spa2',20.00,NULL,NULL,NULL,NULL,'liubao','2021-04-05 17:57:37',NULL,NULL),(40,'2021-04-06','ADD','spa2',20.00,NULL,NULL,NULL,NULL,'liubao','2021-04-06 06:45:52',NULL,NULL),(41,'2021-04-06','ADD','spa2',10.00,NULL,NULL,NULL,NULL,'liubao','2021-04-06 10:08:01',NULL,NULL),(42,'2021-04-06','ADD','spa2',10.00,NULL,NULL,NULL,NULL,'liubao','2021-04-06 10:08:01',NULL,NULL),(43,'2021-04-06','ADD','spa2',2.00,NULL,NULL,NULL,NULL,'liubao','2021-04-06 19:36:56',NULL,NULL);
/*!40000 ALTER TABLE `ao_stocktakelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_transactionlist`
--

DROP TABLE IF EXISTS `ao_transactionlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_transactionlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `TransactionID` int DEFAULT NULL,
  `TransactionRef` varchar(145) DEFAULT NULL,
  `TransactionDate` date DEFAULT NULL,
  `TransactionAmount` decimal(13,2) DEFAULT NULL,
  `PaymentTo` varchar(245) DEFAULT NULL,
  `Active` int DEFAULT '1',
  `DebtorCode` varchar(45) DEFAULT NULL,
  `TransactionType` varchar(45) DEFAULT NULL,
  `TransactionDesc` varchar(245) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `PaymentTermID` int DEFAULT NULL,
  `ServiceTimes` int DEFAULT NULL,
  `SalesPersonSingle` varchar(45) DEFAULT NULL,
  `SalesPersonPassive` varchar(45) DEFAULT NULL,
  `SalesPersonSingleClient` varchar(45) DEFAULT NULL,
  `SalesPersonPassiveClient` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_transactionlist`
--

LOCK TABLES `ao_transactionlist` WRITE;
/*!40000 ALTER TABLE `ao_transactionlist` DISABLE KEYS */;
INSERT INTO `ao_transactionlist` VALUES (1,1,'34','2020-11-23',294.00,NULL,1,NULL,'BUY_PRODUCT','936 Wellness Retreat Mist','2020-11-23 03:10:56','01110340242',NULL,NULL,NULL,NULL,NULL,NULL),(2,2,'34','2020-11-23',392.00,NULL,1,NULL,'BUY_PRODUCT','936 Wellness Retreat Mist','2020-11-23 03:14:08','01110340242',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ao_transactionlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_unitlist`
--

DROP TABLE IF EXISTS `ao_unitlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_unitlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `UnitName` varchar(145) DEFAULT NULL,
  `UnitCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_unitlist`
--

LOCK TABLES `ao_unitlist` WRITE;
/*!40000 ALTER TABLE `ao_unitlist` DISABLE KEYS */;
INSERT INTO `ao_unitlist` VALUES (1,'Pieces','pieces'),(2,'KG','kg'),(3,'Pack','pack');
/*!40000 ALTER TABLE `ao_unitlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ao_uploadlist`
--

DROP TABLE IF EXISTS `ao_uploadlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ao_uploadlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Module` varchar(45) DEFAULT NULL,
  `ModuleID` int DEFAULT NULL,
  `FileName` varchar(245) DEFAULT NULL,
  `FileType` varchar(200) DEFAULT NULL,
  `FileSize` int DEFAULT NULL,
  `Ordering` int DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `LastModified` datetime DEFAULT NULL,
  `Active` smallint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modulefilename` (`Module`,`FileName`) /*!80000 INVISIBLE */,
  KEY `moduleorderingactive` (`Module`,`Ordering`,`Active`,`ModuleID`),
  KEY `moduleactive` (`Module`,`Active`,`ModuleID`),
  KEY `moduleid` (`Module`,`ModuleID`,`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ao_uploadlist`
--

LOCK TABLES `ao_uploadlist` WRITE;
/*!40000 ALTER TABLE `ao_uploadlist` DISABLE KEYS */;
INSERT INTO `ao_uploadlist` VALUES (35,'UPLOAD_PRODUCTIMAGE',3,'Pan_Mee_1588422466110.jpg','image/jpeg',51445,1,'damien','2020-05-02 20:27:46',NULL,NULL,1),(36,'UPLOAD_PRODUCTIMAGE',4,'Hakka_Mee_1588422512346.jpg','image/jpeg',54148,1,'damien','2020-05-02 20:28:32',NULL,NULL,1),(37,'UPLOAD_PRODUCTIMAGE',6,'Kueh_Teow_Bulat_1588422989749.jpg','image/jpeg',48423,1,'damien','2020-05-02 20:36:29',NULL,NULL,1),(38,'UPLOAD_PRODUCTIMAGE',7,'Laksa_Pendek_1588423075051.jpg','image/jpeg',57248,1,'damien','2020-05-02 20:37:55',NULL,NULL,1),(39,'UPLOAD_PRODUCTIMAGE',12,'Laksa_Panjang_1588423337523.jpg','image/jpeg',58596,1,'damien','2020-05-02 20:42:17',NULL,NULL,1),(40,'UPLOAD_PRODUCTIMAGE',13,'Mee_Kuning_Kecil_1588423427739.jpg','image/jpeg',88655,0,'damien','2020-05-02 20:43:47',NULL,NULL,1),(41,'UPLOAD_PRODUCTIMAGE',14,'Mee_Besar_1588423491088.jpg','image/jpeg',62911,1,'damien','2020-05-02 20:44:51',NULL,NULL,1),(42,'UPLOAD_PRODUCTIMAGE',15,'Kueh_Teow_Halus_1588423733799.jpg','image/jpeg',68111,1,'damien','2020-05-02 20:48:53',NULL,NULL,1),(43,'UPLOAD_PRODUCTIMAGE',16,'Kueh_Teow_Kasar_1588423880453.jpg','image/jpeg',68563,0,'damien','2020-05-02 20:51:20',NULL,NULL,1),(44,'UPLOAD_PRODUCTIMAGE',17,'Yee_Mee_1588424001884.jpg','image/jpeg',45895,1,'damien','2020-05-02 20:53:21',NULL,NULL,1),(46,'UPLOAD_PRODUCTIMAGE',19,'Fucuk_Goreng_1588424192101.jpg','image/jpeg',52643,1,'damien','2020-05-02 20:56:32',NULL,NULL,1),(47,'UPLOAD_PRODUCTIMAGE',18,'MSFD500_1590213717942.png','image/png',478583,0,'admin','2020-05-23 06:01:58',NULL,NULL,1),(48,'UPLOAD_PRODUCTIMAGE',17,'YMS5_1590213749875.png','image/png',238401,0,'admin','2020-05-23 06:02:29',NULL,NULL,1),(49,'UPLOAD_PRODUCTIMAGE',19,'fucuk300_1590213793336.png','image/png',375390,0,'admin','2020-05-23 06:03:13',NULL,NULL,1),(50,'UPLOAD_PRODUCTIMAGE',5,'kulitwantan_1590213843388.png','image/png',251778,0,'admin','2020-05-23 06:04:03',NULL,NULL,1),(51,'UPLOAD_PRODUCTIMAGE',5,'kulit wantan_1590213855969.png','image/png',455623,1,'admin','2020-05-23 06:04:16',NULL,NULL,1),(54,'UPLOAD_PRODUCTIMAGE',15,'KTP900_1590214032016.png','image/png',326242,0,'admin','2020-05-23 06:07:12',NULL,NULL,1),(55,'UPLOAD_PRODUCTIMAGE',15,'KTM900_1590214041545.png','image/png',340063,1,'admin','2020-05-23 06:07:21',NULL,NULL,1),(56,'UPLOAD_PRODUCTIMAGE',14,'mb900_1590214071222.png','image/png',320241,0,'admin','2020-05-23 06:07:51',NULL,NULL,1),(57,'UPLOAD_PRODUCTIMAGE',13,'MK MEEDO900_1590214096051.png','image/png',346192,1,'admin','2020-05-23 06:08:16',NULL,NULL,1),(58,'UPLOAD_PRODUCTIMAGE',12,'LPJ_1590214114260.png','image/png',317011,0,'admin','2020-05-23 06:08:34',NULL,NULL,1),(59,'UPLOAD_PRODUCTIMAGE',7,'LSF_1590214140862.png','image/png',282130,0,'admin','2020-05-23 06:09:00',NULL,NULL,1),(60,'UPLOAD_PRODUCTIMAGE',6,'ccff_1590214169068.png','image/png',254562,0,'admin','2020-05-23 06:09:29',NULL,NULL,1),(61,'UPLOAD_PRODUCTIMAGE',6,'ccfbox_1590214179346.png','image/png',410487,1,'admin','2020-05-23 06:09:39',NULL,NULL,1),(62,'UPLOAD_PRODUCTIMAGE',4,'HKpacking_1590214218421.png','image/png',342367,0,'admin','2020-05-23 06:10:18',NULL,NULL,1),(63,'UPLOAD_PRODUCTIMAGE',3,'PMpacking_1590214252651.png','image/png',327033,0,'admin','2020-05-23 06:10:52',NULL,NULL,1),(64,'UPLOAD_PRODUCTIMAGE',3,'PK2packing_1590214258274.png','image/png',326722,1,'admin','2020-05-23 06:10:58',NULL,NULL,1),(65,'UPLOAD_PRODUCTIMAGE',2,'RamenFD500_1590214320207.png','image/png',384140,0,'admin','2020-05-23 06:12:00',NULL,NULL,1),(66,'UPLOAD_PRODUCTIMAGE',20,'FB CKT_1590217676200.jpg','image/jpeg',61930,1,'admin','2020-05-23 07:07:56',NULL,NULL,1),(67,'UPLOAD_PRODUCTIMAGE',20,'FBCKT_1590218580349.png','image/png',149345,0,'admin','2020-05-23 07:23:00',NULL,NULL,1),(68,'UPLOAD_PRODUCTIMAGE',21,'shabupacking_1590218999223.png','image/png',441585,1,'admin','2020-05-23 07:29:59',NULL,NULL,1),(70,'UPLOAD_PRODUCTIMAGE',21,'Shabu10_1590219030552.png','image/png',389830,0,'admin','2020-05-23 07:30:30',NULL,NULL,1),(72,'UPLOAD_PRODUCTIMAGE',22,'Pan_Mee_1590657445850.jpg','image/jpeg',51445,0,'admin','2020-05-28 09:17:25',NULL,NULL,1),(73,'UPLOAD_ORDERRECEIPT',104019,'thumbnail_image001_1590657656915.jpg','image/jpeg',55361,0,'01110390242','2020-05-28 09:20:56',NULL,NULL,1),(74,'UPLOAD_ORDERRECEIPT',104020,'thumbnail_image001_1590659033781.jpg','image/jpeg',55361,0,'01110390242','2020-05-28 09:43:53',NULL,NULL,1),(75,'UPLOAD_PRODUCTIMAGE',25,'WhatsApp Image 2020-05-12 at 4_1593057665751.jpeg','image/jpeg',33973,0,'01110340242','2020-06-25 12:01:05',NULL,NULL,1),(76,'UPLOAD_PRODUCTIMAGE',25,'ba7499f4-4260-4d98-8d23-a0cda2a35380_1593057749773.jpg','image/jpeg',50017,1,'01110340242','2020-06-25 12:02:30',NULL,NULL,1),(77,'UPLOAD_PRODUCTIMAGE',30,'Free_Sample_By_Wix_1593059100852.jpg','image/jpeg',2896,0,'01110340242','2020-06-25 12:25:00',NULL,NULL,1),(78,'UPLOAD_PRODUCTIMAGE',31,'b6985496-7a8d-4721-86e4-86b14d4a3ec0_1593059119926.jpg','image/jpeg',60130,0,'01110340242','2020-06-25 12:25:20',NULL,NULL,1),(84,'UPLOAD_PRODUCTIMAGE',25,'product_1593117823029.jpg','image/jpeg',12941,2,'01110340242','2020-06-26 04:43:43',NULL,NULL,1),(91,'UPLOAD_PRODUCTIMAGE',25,'1b5c00bc2389345e571a8a6fb27f21b6_1593119219581.jpg','image/jpeg',104808,3,'01110340242','2020-06-26 05:06:59',NULL,NULL,1),(103,'UPLOAD_PRODUCTIMAGE',2,'1b5c00bc2389345e571a8a6fb27f21b6_1593120629571.jpg','image/jpeg',104808,1,'01110340242','2020-06-26 05:30:29',NULL,NULL,1),(104,'UPLOAD_PRODUCTIMAGE',25,'1b5c00bc2389345e571a8a6fb27f21b6_1593120689806.jpg','image/jpeg',104808,4,'01110340242','2020-06-26 05:31:29',NULL,NULL,1),(106,'UPLOAD_PRODUCTIMAGE',21,'1b5c00bc2389345e571a8a6fb27f21b6_1593120777660.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 05:32:57',NULL,NULL,1),(107,'UPLOAD_PRODUCTIMAGE',12,'1b5c00bc2389345e571a8a6fb27f21b6_1593120897181.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 05:34:57',NULL,NULL,1),(108,'UPLOAD_PRODUCTIMAGE',12,'product_1593120905604.jpg','image/jpeg',65100,3,'01110340242','2020-06-26 05:35:05',NULL,NULL,1),(109,'UPLOAD_PRODUCTIMAGE',18,'1b5c00bc2389345e571a8a6fb27f21b6_1593122905517.jpg','image/jpeg',104808,1,'01110340242','2020-06-26 06:08:25',NULL,NULL,1),(110,'UPLOAD_PRODUCTIMAGE',6,'1b5c00bc2389345e571a8a6fb27f21b6_1593122935648.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 06:08:55',NULL,NULL,1),(111,'UPLOAD_PRODUCTIMAGE',17,'1b5c00bc2389345e571a8a6fb27f21b6_1593123023627.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 06:10:23',NULL,NULL,1),(112,'UPLOAD_PRODUCTIMAGE',2,'1b5c00bc2389345e571a8a6fb27f21b6_1593123154330.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 06:12:34',NULL,NULL,1),(113,'UPLOAD_PRODUCTIMAGE',4,'1b5c00bc2389345e571a8a6fb27f21b6_1593123172035.jpg','image/jpeg',104808,2,'01110340242','2020-06-26 06:12:52',NULL,NULL,1),(116,'UPLOAD_PRODUCTIMAGE',15,'product_1593124372802.jpg','image/jpeg',65286,2,'01110340242','2020-06-26 06:32:52',NULL,NULL,1),(118,'UPLOAD_PRODUCTIMAGE',21,'product_1593161221649.jpg','image/jpeg',64004,3,'01110340242','2020-06-26 16:47:01',NULL,NULL,1),(125,'UPLOAD_PRODUCTIMAGE',33,'product_1593793122261.jpg','image/png',906614,0,'01110340242','2020-07-04 00:18:42',NULL,NULL,1),(127,'UPLOAD_TOUCHNGO',NULL,'product_1593889562795.jpg','image/png',738241,NULL,'01110340242','2020-07-05 03:06:02',NULL,NULL,NULL),(129,'UPLOAD_PRODUCTIMAGE',33,'product_1598545563033.jpg','image/png',304993,1,'admin','2020-08-28 00:26:03',NULL,NULL,1),(130,'UPLOAD_PRODUCTIMAGE',33,'80d7c9769aae5_1598545771847.jpg','image/jpeg',89695,2,'admin','2020-08-28 00:29:31',NULL,NULL,1),(131,'UPLOAD_PRODUCTIMAGE',33,'01_1598545792198.jpg','image/jpeg',2209863,3,'admin','2020-08-28 00:29:52',NULL,NULL,1),(133,'UPLOAD_PRODUCTIMAGE',34,'1_1598551788588.jpg','image/jpeg',687757,1,'admin','2020-08-28 02:09:48',NULL,NULL,1),(135,'UPLOAD_PRODUCTIMAGE',34,'2_1598551846286.jpg','image/jpeg',688682,1,'admin','2020-08-28 02:10:46',NULL,NULL,1),(136,'UPLOAD_PRODUCTIMAGE',34,'3_1598551851202.jpg','image/jpeg',734656,1,'admin','2020-08-28 02:10:51',NULL,NULL,1),(137,'UPLOAD_PRODUCTIMAGE',34,'4_1598551859521.jpg','image/jpeg',812156,1,'admin','2020-08-28 02:10:59',NULL,NULL,1),(138,'UPLOAD_PRODUCTIMAGE',34,'5_1598551869261.jpg','image/jpeg',746288,1,'admin','2020-08-28 02:11:09',NULL,NULL,1),(139,'UPLOAD_PRODUCTIMAGE',34,'6_1598551873427.jpg','image/jpeg',690382,1,'admin','2020-08-28 02:11:13',NULL,NULL,1),(140,'UPLOAD_PRODUCTIMAGE',34,'0_1598552186660.jpg','image/jpeg',163640,0,'admin','2020-08-28 02:16:26',NULL,NULL,1),(141,'UPLOAD_PRODUCTIMAGE',35,'a4d0a1aa0fed5_1598552445447.jpg','image/jpeg',98515,0,'admin','2020-08-28 02:20:45',NULL,NULL,1),(142,'UPLOAD_PRODUCTIMAGE',35,'01_1598552451065.jpg','image/jpeg',2120002,1,'admin','2020-08-28 02:20:51',NULL,NULL,1),(143,'UPLOAD_PRODUCTIMAGE',35,'02_1598552460526.jpg','image/jpeg',1277044,2,'admin','2020-08-28 02:21:00',NULL,NULL,1),(144,'UPLOAD_PRODUCTIMAGE',35,'03_1598552467338.jpg','image/jpeg',1277217,3,'admin','2020-08-28 02:21:07',NULL,NULL,1),(145,'UPLOAD_PRODUCTIMAGE',35,'04_1598552472779.jpg','image/jpeg',1123567,4,'admin','2020-08-28 02:21:12',NULL,NULL,1),(146,'UPLOAD_PRODUCTIMAGE',35,'05_1598552476942.jpg','image/jpeg',1299255,5,'admin','2020-08-28 02:21:16',NULL,NULL,1),(147,'UPLOAD_PRODUCTIMAGE',35,'06_1598552481064.jpg','image/jpeg',1099641,6,'admin','2020-08-28 02:21:21',NULL,NULL,1),(148,'UPLOAD_PRODUCTIMAGE',35,'07_1598552484395.jpg','image/jpeg',1186792,7,'admin','2020-08-28 02:21:24',NULL,NULL,1),(149,'UPLOAD_PRODUCTIMAGE',36,'LAE2_1598553179051.png','image/png',506436,0,'admin','2020-08-28 02:32:59',NULL,NULL,1),(150,'UPLOAD_PRODUCTIMAGE',36,'01_1598553185151.jpg','image/jpeg',2209863,1,'admin','2020-08-28 02:33:05',NULL,NULL,1),(151,'UPLOAD_PRODUCTIMAGE',36,'02_1598553188922.jpg','image/jpeg',12494280,2,'admin','2020-08-28 02:33:08',NULL,NULL,1),(152,'UPLOAD_PRODUCTIMAGE',36,'03_1598553192414.jpg','image/jpeg',13150355,3,'admin','2020-08-28 02:33:12',NULL,NULL,1),(153,'UPLOAD_PRODUCTIMAGE',36,'04_1598553197458.jpg','image/jpeg',12645023,4,'admin','2020-08-28 02:33:17',NULL,NULL,1),(154,'UPLOAD_PRODUCTIMAGE',36,'05_1598553201086.jpg','image/jpeg',13217028,5,'admin','2020-08-28 02:33:21',NULL,NULL,1),(155,'UPLOAD_PRODUCTIMAGE',36,'06_1598553204521.jpg','image/jpeg',12848088,6,'admin','2020-08-28 02:33:24',NULL,NULL,1),(156,'UPLOAD_PRODUCTIMAGE',36,'07_1598553208341.jpg','image/jpeg',12595955,7,'admin','2020-08-28 02:33:28',NULL,NULL,1),(157,'UPLOAD_PRODUCTIMAGE',36,'08_1598553212155.jpg','image/jpeg',13094966,8,'admin','2020-08-28 02:33:32',NULL,NULL,1),(158,'UPLOAD_PRODUCTIMAGE',37,'0ddb998159ae5_1598553433196.jpg','image/jpeg',86146,0,'admin','2020-08-28 02:37:13',NULL,NULL,1),(159,'UPLOAD_PRODUCTIMAGE',37,'1_1598553437764.jpg','image/jpeg',398957,1,'admin','2020-08-28 02:37:17',NULL,NULL,1),(160,'UPLOAD_PRODUCTIMAGE',37,'2_1598553441978.jpg','image/jpeg',422469,2,'admin','2020-08-28 02:37:21',NULL,NULL,1),(161,'UPLOAD_PRODUCTIMAGE',37,'3_1598553445787.jpg','image/jpeg',423305,3,'admin','2020-08-28 02:37:25',NULL,NULL,1),(162,'UPLOAD_PRODUCTIMAGE',37,'4_1598553450355.jpg','image/jpeg',406487,4,'admin','2020-08-28 02:37:30',NULL,NULL,1),(163,'UPLOAD_PRODUCTIMAGE',37,'5_1598553455212.jpg','image/jpeg',528101,5,'admin','2020-08-28 02:37:35',NULL,NULL,1),(165,'UPLOAD_PRODUCTIMAGE',37,'6_1598553465858.jpg','image/jpeg',441034,6,'admin','2020-08-28 02:37:45',NULL,NULL,1),(167,'UPLOAD_PRODUCTIMAGE',38,'01_1598815116313.jpg','image/jpeg',18691273,0,'01110340242','2020-08-31 03:18:36',NULL,NULL,1),(168,'UPLOAD_PRODUCTIMAGE',38,'02_1598815120433.jpg','image/jpeg',18225654,1,'01110340242','2020-08-31 03:18:40',NULL,NULL,1),(169,'UPLOAD_PRODUCTIMAGE',38,'03_1598815124245.jpg','image/jpeg',18553373,1,'01110340242','2020-08-31 03:18:44',NULL,NULL,1),(170,'UPLOAD_PRODUCTIMAGE',38,'04_1598815129487.jpg','image/jpeg',18592317,1,'01110340242','2020-08-31 03:18:49',NULL,NULL,1),(171,'UPLOAD_PRODUCTIMAGE',38,'05_1598815133758.jpg','image/jpeg',18289768,1,'01110340242','2020-08-31 03:18:53',NULL,NULL,1),(172,'UPLOAD_PRODUCTIMAGE',38,'06_1598815142096.jpg','image/jpeg',18559272,1,'01110340242','2020-08-31 03:19:02',NULL,NULL,1),(173,'UPLOAD_PRODUCTIMAGE',38,'07_1598815146245.jpg','image/jpeg',18538367,1,'01110340242','2020-08-31 03:19:06',NULL,NULL,1);
/*!40000 ALTER TABLE `ao_uploadlist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18  9:41:42
