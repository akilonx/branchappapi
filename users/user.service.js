﻿const config = require('config.json')
const jwt = require('jsonwebtoken')
const pool = require('../_helpers/database')
const nodemailer = require('nodemailer')
const passwordgenerator = require('password-generator')
// users hardcoded for simplicity, store in a db for production applications
//const users = [{ id: 1, username: 'test', password: 'test', firstName: 'Test', lastName: 'User' }];

const util = require('util')
const exec = util.promisify(require('child_process').exec)

//const mysql_import = require('mysql-import');
var rerun = true

module.exports = {
  authenticate,
}

async function authenticate({ username, password, client, clientlogin }) {
  try {
    //if not a client login
    var dsn = await pool.query(
      'SELECT * FROM systemcontrol.webpara WHERE Orgname = ? ',
      [client]
    )

    if (!dsn[0]) throw 'Invalid login, please try again.'
    if (!dsn[0].DSN) throw 'Invalid login, please try again.'

    var user = await pool.query(
      `SELECT * FROM ` +
        dsn[0].DSN +
        `.ao_stafflist WHERE StaffID = ? AND Password2 = ?`,
      [username, password]
    )

    //throw user[0].Department
    //console.log(user, dsn[0].DSN, username, password);
    if (user[0]) {
      const sessionid = passwordgenerator(20, false)
      const token = jwt.sign(
        {
          sub: user[0].id,
          username: user[0].StaffID,
          role: user[0].Department,
          client: dsn[0].DSN,
          sessionid,
        },
        config.secret,
        {
          expiresIn: '365d', // expires in 365 days
        }
      )

      delete user[0].Password2
      delete user[0].Password
      delete user[0].ModifiedBy
      delete user[0].CreateBy
      const { password, ...userWithoutPassword } = user[0]

      /* if (subscription[0].SingleMultiType == 'su') {
        //id, DSN, User, LoggedDate, SessionID
        var insertpayment = await pool.query(
          `DELETE FROM ` + dsn[0].DSN + `.ao_loggedin WHERE DSN=?`,
          [dsn[0].DSN]
        )
        var insertpayment = await pool.query(
          `INSERT INTO ` +
            dsn[0].DSN +
            `.ao_loggedin SET DSN=?, User=?, LoggedDate=NOW(), SessionID=?`,
          [dsn[0].DSN, user[0].id, sessionid]
        )
      } */
      //console.log('here')
      return {
        ...userWithoutPassword,
        token,
      }
    }

    // if client is logging in
  } catch (err) {
    throw new Error(err)
  }

  //console.log(user);
  //const user = users.find(u => u.username === username && u.password === password);
}
