﻿const config = require("config.json");
const jwt = require("jsonwebtoken");
const pool = require("../_helpers/database");
const fs = require("fs");
const cheerio = require("cheerio");
// users hardcoded for simplicity, store in a db for production applications

module.exports = {
  saveUploadLogo,
  saveUploadTng,
  saveUploadReport,
  saveUpload,
  getImage,
};

async function saveUploadLogo(a, user) {
  try {
    var remove = await pool.query(
      `DELETE FROM ` +
        user.client +
        `.ao_uploadlist WHERE Module='UPLOAD_COMPANYLOGO'`,
      []
    );

    var query = await pool.query(
      `INSERT INTO ` +
        user.client +
        `.ao_uploadlist SET Module='UPLOAD_COMPANYLOGO', CreatedBy=?, CreatedOn=NOW(), FileName=?, FileType=?, FileSize=?`,
      [user.username, a.filename, a.mimetype, a.size]
    );
  } catch (err) {
    throw new Error(err);
  }
  return query;
}

async function saveUploadTng(a, user) {
  try {
    var remove = await pool.query(
      `DELETE FROM ` +
        user.client +
        `.ao_uploadlist WHERE Module='UPLOAD_TOUCHNGO'`,
      []
    );

    var query = await pool.query(
      `INSERT INTO ` +
        user.client +
        `.ao_uploadlist SET Module='UPLOAD_TOUCHNGO', CreatedBy=?, CreatedOn=NOW(), FileName=?, FileType=?, FileSize=?`,
      [user.username, a.filename, a.mimetype, a.size]
    );
  } catch (err) {
    throw new Error(err);
  }
  return query;
}

function cleanString(input) {
  var output = "";
  for (var i = 0; i < input.length; i++) {
    if (input.charCodeAt(i) <= 127) {
      output += input.charAt(i);
    }
  }
  return output;
}

async function saveUploadReport(a, user, id) {
  try {
    const body = fs.readFileSync(
      "/home/uploads/spa1/uploads/healthreport/" + a.filename,
      "utf8"
    );

    let $ = cheerio.load(body);

    const title = $("title").text();
    console.log($("title").text());

    const genderRaw = $(
      "table:nth-child(1) tr:nth-child(3) td:nth-child(2)"
    ).text();
    console.log($("table:nth-child(1) tr:nth-child(3) td:nth-child(2)").text());

    const nameRaw = $(
      "table:nth-child(1) tr:nth-child(3) td:nth-child(1)"
    ).text();
    console.log($("table:nth-child(1) tr:nth-child(3) td:nth-child(1)").text());

    const ageRaw = $(
      "table:nth-child(1) tr:nth-child(3) td:nth-child(3)"
    ).text();
    console.log($("table:nth-child(1) tr:nth-child(3) td:nth-child(3)").text());

    const figureRaw = cleanString(
      $(
        "table:nth-child(1) tr:nth-child(4) td:nth-child(1) table:nth-child(1) tr:nth-child(1) td:nth-child(1)"
      ).text()
    );
    console.log(
      cleanString(
        $(
          "table:nth-child(1) tr:nth-child(4) td:nth-child(1) table:nth-child(1) tr:nth-child(1) td:nth-child(1)"
        ).text()
      )
    );

    const testingTimeRaw = $(
      "table:nth-child(1) tr:nth-child(4) td:nth-child(1) table:nth-child(1) tr:nth-child(1) td:nth-child(2)"
    ).text();
    console.log(
      $(
        "table:nth-child(1) tr:nth-child(4) td:nth-child(1) table:nth-child(1) tr:nth-child(1) td:nth-child(2)"
      ).text()
    );

    const name = nameRaw.split(":")[1].trim();
    const gender = genderRaw.split(":")[1].trim();
    const age = ageRaw.split(":")[1].trim();
    const testingTimeSplit = testingTimeRaw.split(":");
    const testingTimeShift = testingTimeSplit.shift();
    const testingTime = testingTimeSplit.join(":");
    //const name = nameRaw.split(':')[1].trim()
    const height = figureRaw.split("(")[1].split("cm")[0];
    const weight = figureRaw.split("(")[1].split("cm")[1].split("kg)")[0];

    var remove = await pool.query(
      `DELETE FROM spa1.ao_reportheader WHERE UserID = ? AND Title = ? AND TestingTimeRaw = ?`,
      [id, title, testingTimeRaw]
    );

    var query = await pool.query(
      `INSERT 
      INTO spa1.ao_reportheader 
      SET 
        UserID=?, 
        CreatedBy=?, 
        CreatedOn=NOW(),
        Filename=?,
        Title=?,
        GenderRaw=?,
        NameRaw=?,
        AgeRaw=?,
        FigureRaw=?,
        TestingTimeRaw=?,
        Gender=?,
        Name=?,
        Age=?,
        Height=?,
        Weight=?,
        TestingTime=?`,
      [
        id,
        user.username,
        a.filename,
        title,
        genderRaw,
        nameRaw,
        ageRaw,
        figureRaw,
        testingTimeRaw,
        gender,
        name,
        age,
        height,
        weight,
        testingTime,
      ]
    );

    //query.insertId

    const reportData = [];

    $("table:nth-child(4) tr").each(function (i, elem) {
      if (i > 1) {
        reportData.push([
          query.insertId,
          $(`table:nth-child(4) tr:nth-child(${i}) td:nth-child(1)`)
            .text()
            .replace(/(\r\n|\n|\r)/gm, "")
            .trim(),
          $(`table:nth-child(4) tr:nth-child(${i}) td:nth-child(2)`)
            .text()
            .replace(/(\r\n|\n|\r)/gm, "")
            .trim(),
          $(`table:nth-child(4) tr:nth-child(${i}) td:nth-child(3)`)
            .text()
            .replace(/(\r\n|\n|\r)/gm, "")
            .trim(),
          $(`table:nth-child(4) tr:nth-child(${i}) td:nth-child(4)`)
            .text()
            .replace(/(\r\n|\n|\r)/gm, "")
            .trim(),
          user.username,
          new Date().toISOString().slice(0, 10) +
            " " +
            new Date().toISOString().slice(11, 19),
        ]);
      }
    });

    var reportdetail = await pool.query(
      `INSERT INTO spa1.ao_reportdetail (ReportID, TestingItem, NormalRange, ActualMeasurement, TestingResult, CreatedBy, CreatedOn) VALUES ?`,
      [reportData]
    );
  } catch (err) {
    throw new Error(err);
  }
  return query;
}

async function saveUpload(a, id, user, active) {
  try {
    console.log(a);

    let order = 0;

    var remove = await pool.query(
      `DELETE FROM ` +
        user.client +
        `.ao_uploadlist WHERE Module='UPLOAD_PRODUCTIMAGE' AND Active='0'`,
      []
    );

    var ordering = await pool.query(
      `SELECT Ordering FROM ` +
        user.client +
        `.ao_uploadlist WHERE Module='UPLOAD_PRODUCTIMAGE' and ModuleID=? order by Ordering desc`,
      [id]
    );

    if (ordering[0]) {
      order = ordering[0].Ordering + 1;
    }

    var query = await pool.query(
      `INSERT INTO ` +
        user.client +
        `.ao_uploadlist SET Module='UPLOAD_PRODUCTIMAGE', ModuleID=?, CreatedBy=?, CreatedOn=NOW(), FileName=?, FileType=?, FileSize=?, Ordering=?, Active=? `,
      [id, user.username, a.filename, a.mimetype, a.size, order, active]
    );
  } catch (err) {
    throw new Error(err);
  }
  return { filename: a.filename };
}

async function getImage(a, user) {
  try {
    var query = await pool.query(
      `SELECT FileName FROM ` + user.client + `.ao_uploadlist WHERE id = ?`,
      [a]
    );

    res.sendFile("../../../uploads/invoice/" + req.params.id + "/" + file);
  } catch (err) {
    throw new Error(err);
  }
  return query;
}
