const express = require('express')
const router = express.Router()
const _service = require('./upload.service')
const multer = require('multer')
const bodyParser = require('body-parser')
const pool = require('../_helpers/database')
const fs = require('fs')
const iconv = require('iconv-lite')

var storagehealthreport = multer.diskStorage({
  // destino del fichero
  destination: function (req, file, cb) {
    cb(null, '/home/uploads/spa1/uploads/healthreport')
  },
  // renombrar fichero
  filename: function (req, file, cb) {
    let f = file.originalname.split('.')
    cb(null, f[0] + '_' + new Date().getTime() + '.' + f[f.length - 1])
  },
})

var uploadhealthreport = multer({ storage: storagehealthreport })

var storage = multer.diskStorage({
  // destino del fichero
  destination: function (req, file, cb) {
    cb(null, '/home/uploads/' + req.user.client + '/uploads/invoice')
  },
  // renombrar fichero
  filename: function (req, file, cb) {
    let f = file.originalname.split('.')
    cb(null, f[0] + '_' + new Date().getTime() + '.' + f[f.length - 1])
  },
})

var upload = multer({ storage: storage })

// routes
router.put(
  '/send/:id/:active',
  async function (req, file, cb) {
    try {
      var image = await pool.query(
        `SELECT FileName FROM ` +
          req.user.client +
          `.ao_uploadlist WHERE Module='UPLOAD_PRODUCTIMAGE' AND ModuleID = ? AND FileName = ?`,
        [req.params.id, file.originalname]
      )

      if (image[0] && image[0].FileName) {
        if (
          fs.existsSync(
            '/home/uploads/shop/uploads/invoice/' + image[0].FileName
          )
        ) {
          fs.unlinkSync(
            '/home/uploads/shop/uploads/invoice/' + image[0].FileName
          )
        }
      }
      cb()
    } catch (err) {
      console.error(err)
    }
  },
  upload.array('uploads[]', 12),
  function (req, res) {
    _service
      .saveUpload(req.files[0], req.params.id, req.user, req.params.active)
      .then((a) => res.json(a))
      .catch((err) => {
        //console.log(err)
        next(err)
      })

    //res.send(req.files);
  }
)

// routes
router.put(
  '/companylogo',
  async function (req, file, cb) {
    try {
      var image = await pool.query(
        `SELECT FileName FROM ` +
          req.user.client +
          `.ao_uploadlist WHERE Module='UPLOAD_COMPANYLOGO' AND FileName = ?`,
        [req.params.id, file.originalname]
      )

      if (image[0] && image[0].FileName) {
        if (
          fs.existsSync(
            '/home/uploads/' +
              req.user.client +
              ' /uploads/logo/' +
              image[0].FileName
          )
        ) {
          fs.unlinkSync(
            '/home/uploads/' +
              req.user.client +
              ' /uploads/logo/' +
              image[0].FileName
          )
        }
      }
      cb()
    } catch (err) {
      console.error(err)
    }
  },
  upload.array('uploads[]', 12),
  function (req, res) {
    _service
      .saveUploadLogo(req.files[0], req.user)
      .then((a) => res.json(a))
      .catch((err) => {
        //console.log(err)
        next(err)
      })

    //res.send(req.files);
  }
)

// routes
router.put(
  '/touchngo',
  async function (req, file, cb) {
    try {
      var image = await pool.query(
        `SELECT FileName FROM ` +
          req.user.client +
          `.ao_uploadlist WHERE Module='UPLOAD_TOUCHNGO' AND FileName = ?`,
        [req.params.id, file.originalname]
      )

      if (image[0] && image[0].FileName) {
        if (
          fs.existsSync(
            '/home/uploads/' +
              req.user.client +
              ' /uploads/logo/' +
              image[0].FileName
          )
        ) {
          fs.unlinkSync(
            '/home/uploads/' +
              req.user.client +
              ' /uploads/logo/' +
              image[0].FileName
          )
        }
      }
      cb()
    } catch (err) {
      console.error(err)
    }
  },
  upload.array('uploads[]', 12),
  function (req, res) {
    _service
      .saveUploadTng(req.files[0], req.user)
      .then((a) => res.json(a))
      .catch((err) => {
        //console.log(err)
        next(err)
      })

    //res.send(req.files);
  }
)

// routes
router.put(
  '/healthreport/:id',
  async function (req, file, cb) {
    try {
      /* var image = await pool.query(
        `SELECT FileName FROM spa1.ao_uploadlist WHERE Module='UPLOAD_REPORT' AND FileName = ?`,
        [req.params.id, file.originalname]
      )

      if (image[0] && image[0].FileName) {
        if (
          fs.existsSync(
            '/home/uploads/spa1/uploads/healthreport/' + image[0].FileName
          )
        ) {
          fs.unlinkSync(
            '/home/uploads/spa1/uploads/healthreport/' + image[0].FileName
          )
        }
      } */
      cb()
    } catch (err) {
      console.error(err)
    }
  },
  uploadhealthreport.array('uploads[]', 100),
  function (req, res) {
    /* const fileopen = fs.readFileSync(
      '/home/uploads/spa1/uploads/healthreport/' + req.files[0].filename,
      'GB2312'
    )
    fs.writeFileSync(
      '/home/uploads/spa1/uploads/healthreport/' + req.files[0].filename,
      fileopen,
      'UTF-8'
    ) */
    //npm install detect-character-encoding
    /* const buffer = fs.readFileSync(
      '/home/uploads/spa1/uploads/healthreport/' + req.files[0].filename
    )
    const originalEncoding = detectCharacterEncoding(buffer) */
    console.log('hi', req.files)
    req.files.map(async (b, i) => {
      let fileg = fs.readFileSync(
        '/home/uploads/spa1/uploads/healthreport/' + b.filename
      )

      fileg = iconv.decode(fileg, 'GB2312')
      //fileg = iconv.decode(fileg, 'utf8')

      fs.writeFileSync(
        '/home/uploads/spa1/uploads/healthreport/' + b.filename,
        fileg
      )

      _service
        .saveUploadReport(b, req.user, req.params.id)
        .then((a) => res.json(a))
        .catch((err) => {
          //console.log(err)
          next(err)
        })
    })

    //res.send(req.files);
  }
)

router.get('/image/:id', getImage)
router.get('/remove/:id', removeImage)

async function getImage(req, res, next) {
  var query = await pool.query(
    `SELECT FileName FROM ` + req.user.client + `.ao_uploadlist WHERE id = ?`,
    [req.params.id]
  )

  if (query[0]) {
    res.sendFile(
      '/home/uploads/' +
        req.user.client +
        '/uploads/invoice' +
        query[0].FileName
    )
  } else {
    res.status(404).send('Error! File not found')
  }
}

async function removeImage(req, res, next) {
  var query = await pool.query(
    `SELECT FileName FROM ` + req.user.client + `.ao_uploadlist WHERE id = ?`,
    [req.params.id]
  )

  //console.log('inside', query)
  if (!query[0]) return

  if (
    fs.existsSync(
      '/home/uploads/' +
        req.user.client +
        '/uploads/invoice' +
        query[0].FileName
    )
  ) {
    fs.unlinkSync(
      '/home/uploads/' +
        req.user.client +
        '/uploads/invoice' +
        query[0].FileName
    )
  }

  var remove = await pool.query(
    `DELETE FROM ` + req.user.client + `.ao_uploadlist WHERE id = ?`,
    [req.params.id]
  )

  res.json(remove)
}

module.exports = router
